-- Reset all --
DROP TABLE IF EXISTS jyctow_article_have_tag;
DROP TABLE IF EXISTS jyctow_comments;
DROP TABLE IF EXISTS jyctow_articles;
DROP TABLE IF EXISTS jyctow_tags;
DROP TABLE IF EXISTS jyctow_pages;
DROP TABLE IF EXISTS jyctow_categories;
DROP TABLE IF EXISTS jyctow_users;
DROP TABLE IF EXISTS jyctow_user_roles;


-- rôle des utilisateurs --
CREATE TABLE  jyctow_user_roles (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(20) NOT NULL,
    description VARCHAR(100),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- utilisateurs --
CREATE TABLE  jyctow_users (
    id SERIAL PRIMARY KEY NOT NULL,
    username VARCHAR(50) NOT NULL UNIQUE,
    firstname VARCHAR(50),
    lastname VARCHAR(50),
    password CHAR(60) NOT NULL, -- sha512 hash comporte 128 caractères --
    email VARCHAR(50) NOT NULL UNIQUE,
    role_id INT NOT NULL,
    status VARCHAR(10) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    email_verified_at TIMESTAMP DEFAULT NULL,
    email_token CHAR(32) NULL,
    last_connexion TIMESTAMP,
    session_token TEXT,
    CONSTRAINT fk_users_user_roles
        FOREIGN KEY (role_id)
        REFERENCES jyctow_user_roles (id)
);

-- catégorie --
CREATE TABLE  jyctow_categories (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- pages --
CREATE TABLE  jyctow_pages (
    id SERIAL PRIMARY KEY NOT NULL,
    title VARCHAR(50) NOT NULL,
    slug VARCHAR(2048) NOT NULL,
    description  VARCHAR(500),
    body TEXT NOT NULL,
    published BOOLEAN NOT NULL DEFAULT 'false',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- tags --
CREATE TABLE  jyctow_tags (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(100) NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- articles --
CREATE TABLE  jyctow_articles (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL UNIQUE,
    description VARCHAR(50) NOT NULL,
    content TEXT NOT NULL,
    author_id INT NOT NULL,
    published INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    categories_id INT NOT NULL,
    CONSTRAINT fk_articles_categories
        FOREIGN KEY (categories_id)
        REFERENCES jyctow_categories (id),
    CONSTRAINT fk_articles_author
        FOREIGN KEY (author_id)
        REFERENCES jyctow_users (id)
        ON DELETE CASCADE
);

-- commentaires --
CREATE TABLE  jyctow_comments (
    id SERIAL PRIMARY KEY NOT NULL,
    content VARCHAR(800) NOT NULL,
    validation INT NOT NULL DEFAULT 0,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    articles_id INT NOT NULL,
    users_id INT NOT NULL,
    CONSTRAINT fk_comments_articles
        FOREIGN KEY (articles_id)
        REFERENCES jyctow_articles (id)
        ON DELETE CASCADE,
    CONSTRAINT fk_comments_users
        FOREIGN KEY (users_id)
        REFERENCES jyctow_users (id)
        ON DELETE CASCADE
);

-- article_have_tag --
CREATE TABLE  jyctow_article_have_tag (
    id SERIAL PRIMARY KEY NOT NULL,
    articles_id INT NOT NULL,
    tags_id INT NOT NULL,
    CONSTRAINT fk_article_have_tag_articles
        FOREIGN KEY (articles_id)
        REFERENCES jyctow_articles (id)
        ON DELETE CASCADE,
    CONSTRAINT fk_article_have_tag_tags
        FOREIGN KEY (tags_id)
        REFERENCES jyctow_tags (id),
    UNIQUE (articles_id, tags_id)
);

-- INSERT DATA --
-- Roles ---
INSERT INTO jyctow_user_roles (name, description) VALUES
('Administrator', 'Control the entire website'),
('Moderator', 'Moderate comments'),
('Redactor', 'Can write articles'),
('Simple user', 'The simplest type of user');

-- Utilisateur administrateur --
INSERT INTO jyctow_users (username, password, email, role_id, status, email_verified_at) VALUES (
    'bestmaker',
    '$2y$10$iPLNY2Z13/I5x/OOY/4nm.wMkyGnW/XqUn64xDD1lkhTzLTqqmldm',
    'maker@blog.best',
    (SELECT id FROM jyctow_user_roles WHERE name = 'Administrator'),
    'enabled',
    CURRENT_TIMESTAMP
);

INSERT INTO jyctow_tags (name, description) VALUES (
    'basic tag',
    'usefull tag'
);

INSERT INTO jyctow_categories (name) VALUES (
    'No category'
),(
    ' News'
);

-- Home page --
INSERT INTO jyctow_pages (title, slug, description, body, published) VALUES (
    'Accueil',
    '/',
    'Mon accueil',
    '<section class="container">
<div class="row align-center">
<div class="col-lg-7 col-sm-12">
<h1 class="p-10 text-center text-4xl font-bold m-0">Bienvenue sur notre blog !</h1>

<section class="flex items-center justify-evenly mb-0 text-lg">
<p>Acc&eacute;dez d&egrave;s &agrave; pr&eacute;sent &agrave; la liste de nos articles :</p>
<button class="button button-big button--bg-primary"><a href="/articles" class="text-white no-underline">Voir les articles !</a></button></section>

<section class="flex items-center justify-evenly mb-0 mt-5 text-lg">
<p>Nous sommes un collectif de blogueurs discutant de l&#39;actualit&eacute;.<br />
Mais bien plus encore ! Pour en savoir plus :</p>
<button class="button button-big button--bg-primary"><a href="/about" class="text-white no-underline">Voir</a></button></section>
</div>
</div>
</section>
',
    'true'
);


-- About page --
INSERT INTO jyctow_pages (title, slug, description, body, published) VALUES (
    'About us',
    '/about',
    'Ma page A propos',
    '<section class="container text-lg">
<div class="row align-center">
<div class="col-lg-7 col-sm-12">
<h1 class="p-10 text-center text-4xl font-bold m-0">A propos de nous !</h1>

<p>Nous sommes un collectif de blogueurs ind&eacute;pendants, parlant de l&#39;actualit&eacute;.</p>
</div>
</div>
</section>
',
    'true'
);

-- example article --
INSERT INTO jyctow_articles (name, description, content, author_id, published, categories_id) VALUES (
    'Example article',
    'This is a description. Just not write too long !',
    '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent quis lectus dui. Morbi non tortor metus. Morbi consequat suscipit cursus. Maecenas at pharetra risus. Praesent a mollis leo. In sagittis urna nec lorem suscipit, nec eleifend sapien bibendum. Proin dapibus venenatis sapien non mattis. Quisque nibh tortor, sagittis at auctor quis, efficitur ac massa. Nunc laoreet leo eu lorem tempor, ac faucibus orci viverra.</p>

    <p>Nam vehicula risus libero, sed cursus eros consequat id. Vivamus congue pulvinar tellus, non tincidunt odio congue in. Ut turpis leo, luctus vitae imperdiet vitae, hendrerit id leo. Quisque sagittis bibendum elit et tristique. Phasellus nec massa quam. Sed eu erat eros. Sed ut accumsan urna. In erat libero, venenatis ut nisi ac, molestie facilisis felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pharetra tincidunt arcu, porta eleifend dui pulvinar volutpat.</p>

    <p>Suspendisse ac urna pretium, scelerisque eros et, rutrum massa. Mauris ut orci nec leo egestas bibendum ornare non velit. Maecenas hendrerit dolor ut metus pharetra pharetra. Praesent pulvinar euismod dui, eu dapibus urna suscipit id. Quisque malesuada tristique erat et pharetra. Fusce in tempor dui. Nullam luctus fermentum dui at molestie. Aenean ultricies consequat libero ut venenatis. Nullam odio leo, lobortis nec luctus ultrices, commodo nec ligula. Quisque mauris neque, efficitur eu diam et, semper varius orci. Nunc mattis porta consequat. Aenean eu ornare nisl.</p>

    <p>Proin eu placerat dolor. Sed pulvinar luctus nisl sed suscipit. Phasellus sit amet sodales justo, id faucibus elit. Etiam ac dolor leo. Mauris pharetra laoreet imperdiet. Maecenas dignissim dictum nisl. Donec sed ipsum id tortor eleifend porta suscipit nec ligula.</p>

    <p>Ut dictum nisl suscipit, vulputate metus sit amet, porta sem. Nullam vitae scelerisque diam, eu semper est. Etiam erat est, fermentum nec sagittis at, lobortis ut mauris. Nam rutrum pellentesque dui, at blandit ligula euismod et. Vestibulum id condimentum velit. Integer tempor dapibus diam, eget rhoncus turpis tempor ut. Phasellus egestas vehicula velit, a posuere metus sodales sed. Curabitur nulla tellus, sollicitudin nec semper id, maximus at leo. Nulla facilisi. Cras porta risus eu nulla congue fermentum. Proin sit amet sapien ac orci consectetur semper.</p>',
    1,
    1,
    2
);

INSERT INTO jyctow_comments (content, articles_id, users_id, validation) VALUES (
    'Ceci était un superbe article !',
    1,
    1,
    0
);