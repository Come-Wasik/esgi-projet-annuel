<?php

use App\Controller\Dashboard\ArticlesController;
use App\Controller\Dashboard\CategoriesController;
use App\Controller\Dashboard\CommentsController;
use App\Controller\Dashboard\DashboardController;
use App\Controller\Dashboard\PagesController;
use App\Controller\Dashboard\ParameterController;
use App\Controller\Dashboard\TagsController;
use App\Controller\Dashboard\UsersController;

# Définition du path du Dashboard
$dashboard_path = \Config::get('cms.dashboard_path');

# Définition des routes du Dashboard
## Dashboard main page
NewRoute::get('dashboard', $dashboard_path, DashboardController::class . '@index');

## Pages
NewRoute::get('pages.index', $dashboard_path . '/pages', PagesController::class . '@index');
NewRoute::mixed('pages.create', $dashboard_path . '/pages/create', PagesController::class . '@create');
NewRoute::post('pages.delete', $dashboard_path . '/pages/delete', PagesController::class . '@delete');
NewRoute::mixed('pages.edit', $dashboard_path . '/pages/edit/{id_page}', PagesController::class . '@edit');

## Articles
NewRoute::get('articles.index', $dashboard_path . '/articles', ArticlesController::class . '@index');
NewRoute::mixed('articles.create', $dashboard_path . '/articles/create', ArticlesController::class . '@create');
NewRoute::post('articles.delete', $dashboard_path . '/articles/delete', ArticlesController::class . '@delete');
NewRoute::mixed('articles.edit', $dashboard_path . '/articles/edit/{id_article}', ArticlesController::class . '@edit');

## Users
NewRoute::get('users.index', $dashboard_path . '/users', UsersController::class . '@index');
NewRoute::mixed('users.create', $dashboard_path . '/users/create', UsersController::class . '@create');
NewRoute::post('users.delete', $dashboard_path . '/users/delete', UsersController::class . '@delete');
NewRoute::mixed('users.edit', $dashboard_path . '/users/edit/{id_user}', UsersController::class . '@edit');

## Categories
NewRoute::get('categories.index', $dashboard_path . '/categories', CategoriesController::class . '@index');
NewRoute::mixed('categories.create', $dashboard_path . '/categories/create', CategoriesController::class . '@create');
NewRoute::post('categories.delete', $dashboard_path . '/categories/delete', CategoriesController::class . '@delete');
NewRoute::mixed('categories.edit', $dashboard_path . '/categories/edit/{id_category}', CategoriesController::class . '@edit');

## Tags
NewRoute::get('tags.index', $dashboard_path . '/tags', TagsController::class . '@index');
NewRoute::mixed('tags.create', $dashboard_path . '/tags/create', TagsController::class . '@create');
NewRoute::post('tags.delete', $dashboard_path . '/tags/delete', TagsController::class . '@delete');
NewRoute::mixed('tags.edit', $dashboard_path . '/tags/edit/{id_tag}', TagsController::class . '@edit');

## Comments
NewRoute::get('comments.index', $dashboard_path . '/comments', CommentsController::class . '@index');
NewRoute::patch('comments.validate', $dashboard_path . '/comments/validate', CommentsController::class . '@validate');
NewRoute::patch('comments.invalidate', $dashboard_path . '/comments/invalidate', CommentsController::class . '@invalidate');
NewRoute::delete('comments.delete.dashboard', $dashboard_path . '/comments', CommentsController::class . '@delete');

## Configuration
NewRoute::get('config.edit', $dashboard_path . '/config', ParameterController::class . '@edit');
NewRoute::patch('config.update', $dashboard_path . '/config', ParameterController::class . '@update');
