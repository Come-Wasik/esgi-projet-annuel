<?php

use App\Controller\ArticlesController;
use App\Controller\CommentsController;
use App\Controller\UserProfileController;

# Article path
$article_path = \Config::get('cms.article_path');

# Routes qui sont requises pour afficher du contenu classique à l'utilisateur
NewRoute::Auth();

## Articles
NewRoute::get('posts.index', '/articles', ArticlesController::class . '@index');
NewRoute::get('posts.show', $article_path . '/{id_article}', ArticlesController::class . '@show');

## Comments
NewRoute::post('comments.store', $article_path . '/{slug}/comments', CommentsController::class . '@store');
NewRoute::delete('comments.delete', $article_path, CommentsController::class . '@delete');

## User
NewRoute::get('user-param.edit', '/user-param', UserProfileController::class . '@edit');
NewRoute::patch('user-param.update', '/user-param', UserProfileController::class . '@update');
