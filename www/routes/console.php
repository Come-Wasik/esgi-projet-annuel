<?php

use App\Controller\Console\MakerController;
use App\Controller\Console\RunnerController;
use App\Controller\Console\DebugController;
use App\Controller\Console\GenerateController;
use App\Controller\Console\HelpController;

/**
 * Routes list
 */

Command::add('make:controller +', MakerController::class . '@makeController');
Command::add('make:entity + *', MakerController::class . '@makeEntity');
Command::add('make:test +', MakerController::class . '@makeTest');
Command::add('make:validator +', MakerController::class . '@makeValidator');
Command::add('make:class + *', MakerController::class . '@makeClass');

Command::add('run:test + +', RunnerController::class . '@runTest');

Command::add('debug:routes', DebugController::class . '@showRoutes');

Command::add('generate:sitemap', GenerateController::class . '@sitemap');

Command::add('-h', HelpController::class . '@showHelp');
Command::add('--help', HelpController::class . '@showHelp');
