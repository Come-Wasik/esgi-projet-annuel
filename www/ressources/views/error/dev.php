<!DOCTYPE html>
<html>
    <head>
        <title>Personnal error</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </head>
    <body>
        <main class="container">
            <div class="row mt-3">
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <h1 class="text-center">An error <?= $error->getCode(); ?> have occured</h1>
                        <p class="text-center">
                            <span class="lead">In file </span>: <?= $error->getFile(); ?><br />
                            At <span class="lead">line <?= $error->getLine()?> </span> : <?= $error->getmessage(); ?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-12">
                
                <div class="card">
                    <div class="card-header">
                        <h2>Errors listing :</h2>
                    </div>
                    <div class="card-body">
                        <?php foreach ($error->getTrace() as $errorrror) { ?>
                        <article>
                            <div class="">
                                <span class="card-title">In file : </span><span class="card-text"><?= $errorrror['file'] ?></span><br />
                                At <span class="line">line <?= $errorrror['line'] ?> </span> :
                                <?php if (isset($errorrror['function'])) { ?>
                                    <?php if (isset($errorrror['class'])) { ?>
                                        <?= $errorrror['class']; ?>
                                    <?php  } ?>

                                    <?php if (isset($errorrror['type'])) { ?>
                                        <?= $errorrror['type']; ?>
                                    <?php  } ?>

                                    <?= $errorrror['function'] ?>
                                <?php  } ?>
                                <?php if (isset($errorrror['args'])) { ?>
                                    <br /><span class="args">Args </span> : <pre> <?php print_r($errorrror['args']); ?></pre>
                                <?php  } ?>
                                </div>
                        </article>
                    <?php } ?>
                    </div>
                </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h2>Original text :</h2>
                        </div>
                        <div class="card-body">
                            <p>
                            <?php
                            foreach (explode('#', $error) as $errorrror) {
                                echo $errorrror . '<br/>';
                            }?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>