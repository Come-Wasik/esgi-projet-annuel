<?php ob_start() ?>
<script src="https://cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
<script src="/js/editArticle.js"></script>
<?php $_sectionHeader = ob_get_clean(); ?>

<?php ob_start() ?>
<div class="container py-12">
    <div class="border-b border-gray-2">
        <h1 class="text-blue-3 text-2xl font-medium mb-2">Créer un nouvel article</h1>
    </div>

    <div class="my-2">
        <?php if ($errors) : ?>
            <div class="alert alert--danger">
                <?php foreach ($errors as $field) : ?>
                    <?php foreach ($field as $errorMessage) : ?>
                        <div><?= $errorMessage ?></div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <section>
        <form class="w-full pt-6 pb-8" method="post">
            <div class="flex items-center justify-center mb-5">
                <div class="form-group mr-5">
                    <label for="name" class="text-black">Titre</label>
                    <input type="text" class="" name="name" placeholder="Titre de l'article" required>
                </div>
            </div>

            <div class="flex items-center justify-center mb-5">
                <div class="form-group mr-4">
                    <label for="tags_id" class="">Tag(s)</label>
                    <select type="text" class="" name="tags_id[]" multiple>
                        <?php foreach ($tags as $tag) : ?>
                            <option value="<?= $tag->getId() ?>"><?= $tag->getName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="categories_id" class="">Catégorie</label>
                    <select type="text" class="" name="categories_id">
                        <?php foreach ($categories as $category) : ?>
                            <option value="<?= $category->getId() ?>"><?= $category->getName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="text-black">Description</label>
                <input type="text" class="" name="description" placeholder="Description de l'article">
            </div>

            <div class="form-group mb-10">
                <label for="content" class="">Contenu</label>
                <textarea type="text" class="" rows="10" name="content" id="editor" placeholder="Titre de votre article" required></textarea>
            </div>
            <input class="mr-2" type="checkbox" name="published" checked><label>Publier l'article?</label>
            <div class="flex items-center justify-end">
                <button class="button button--bg-success">Créer</button>
            </div>
        </form>
    </section>
</div>
<script>
    initSample();
</script>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
