<?php ob_start() ?>
<div class="container py-12">
    <div class="flex items-center justify-between">
        <h1 class="text-blue-3 text-2xl font-medium"><span class="border-b-2 border-blue-3">Nos</span> articles</h1>
        <a class="" href="<?= getRouteUrl('articles.create') ?>">
            <img class="filter-blue w-10 h-10" src="/images/pictograms/add_button.svg" alt="Plus button" title="Create a new article">
        </a>
    </div>
    <?php if (Flash::has('success')) : ?>
        <div class="alert alert--success my-2">
            <?= Flash::get('success') ?>
        </div>
    <?php endif; ?>
    <section>
        <table class="w-full" style="border-collapse: collapse">
            <thead>
                <tr class="text-left border-b">
                    <th class="pr-4 py-2">ID</th>
                    <th class="px-4 py-2">Titre</th>
                    <th class="px-4 py-2">Auteur</th>
                    <th class="px-4 py-2">Tags</th>
                    <th class="px-4 py-2">Catégorie</th>
                    <th class="px-4 py-2">Date de dernière modification</th>
                    <th class="px-4 py-2">Publié</th>
                    <th class="px-4 py-2">Action</th>
                </tr>
            </thead>
            <tbody class="">
                <?php foreach ($articles as $article) : ?>
                    <tr class="border-b">
                        <td class="pr-4 py-3"><?= $article->getId(); ?></td>
                        <td class="px-4 py-3"><?= $article->getName(); ?></td>
                        <td class="px-4 py-3">
                            <?php
                            foreach ($users as $user) {
                                if ($user->getId() == $article->getAuthor_id()) {
                                    echo $user->getUsername();
                                }
                            } ?>
                        </td>
                        <td class="px-4 py-3">
                            <?php ### Affichage des tags ###
                            ?>
                            <?php $tagsToShow = [];
                            foreach ($relBetwArticleAndTag as $rel) :
                                if ($article->getId() == $rel->getArticles_id()) {
                                    foreach ($tags as $tag) {
                                        if ($tag->getId() == $rel->getTags_id()) {
                                            $tagsToShow[] = $tag->getName();
                                        }
                                    }
                                }
                            endforeach;
                            echo (implode(', ', $tagsToShow)) ?>
                        </td>
                        <td class="px-4 py-3">
                            <?php ### Affichage de la catégorie ###
                            ?>
                            <?php foreach ($categories as $category) :
                                if ($article->getCategories_id() == $category->getId()) {
                                    echo $category->getName();
                                }
                            endforeach; ?>
                        </td>
                        <td class="px-4 py-3"><?= formatDatetime($article->getUpdated_at()) ?></td>
                        <td class="px-6 py-3">
                            <?php if ($article->getPublished()) : ?>
                                <img class="w-6 h-6" src="/images/pictograms/valid.svg" alt="valid button" title="Published">
                            <?php else : ?>
                                <img class="w-6 h-6" src="/images/pictograms/none.svg" alt="cancel button" title="Not published">
                            <?php endif; ?>
                        </td>
                        <td class="px-4 py-3">
                            <div class="flex items-center">
                                <form method="post" action="<?= getRouteUrl('articles.delete') ?>" title="Delete">
                                    <button type="submit" name="id_article" value="<?= $article->getId() ?>" style="background: transparent; border: none !important; cursor: pointer;">
                                        <img class="filter-red w-6 h-6" src="/images/pictograms/Trash.svg" alt="Plus button">
                                    </button>
                                </form>

                                <a href="<?= getRouteUrl('articles.edit') . $article->getId() ?>" class="block" title="Edit">
                                    <img class="filter-blue w-6 h-6" src="/images/pictograms/edit.svg" alt="Plus button">
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if (count($articles) == 0) : ?>
                    <tr class="border-b">
                        <td class="pr-4 py-3">il n'y a aucun article</td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </section>
</div>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
