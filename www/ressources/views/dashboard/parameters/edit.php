<?php ob_start() ?>
<div class="container py-12">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-md font-normal">Paramètres généraux du site</h1>
        </div>
    </div>
    <?php if (Flash::has('success')) : ?>
        <div class="alert alert--success my-2">
            <?= Flash::get('success') ?>
        </div>
    <?php endif; ?>
    <?php if (Flash::has('error')) : ?>
        <div class="alert alert--danger my-2">
            <?= Flash::get('error') ?>
        </div>
    <?php endif; ?>
    <form class="w-full pt-6 pb-8" method="POST" action="<?= getRouteUrl('config.update') ?>">
        <div class="flex items-center justify-center mb-5">
            <div class="form-group mb-5 mr-5">
                <label for="" class="text-black">Nom du blog</label>
                <input type="text" class="" name="blog_name" value="<?= $config['blog_name'] ?>" required>
            </div>
        </div>
        <div class="flex items-center justify-center mb-5">
            <div class="form-group mb-5 mr-5">
                <label for="" class="text-black">Le chemin d'accès au Dashboard</label>
                <input type="text" class="" name="dashboard_path" value="<?= $config['dashboard_path'] ?>" required>
            </div>
        </div>
        <div class="flex items-center justify-center mb-5">
            <div class="form-group mb-5 mr-5">
                <label for="" class="text-black">Le chemin d'accès aux articles</label>
                <input type="text" class="" name="article_path" value="<?= $config['article_path'] ?>" required>
            </div>
        </div>
        <div class="flex items-center justify-center mb-5">
            <div class="form-group mb-5 mr-5">
                <label for="" class="text-black">Template</label>
                <select class="" name="template_front" required>
                    <?php foreach ($templates as $template_name): ?>
                        <option
                                value="<?= $template_name ?>"
                                <?= ($config['template_front'] ==  $template_name ? 'selected' : '') ?>
                        ><?= $template_name ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="flex items-center justify-end my-12">
            <button class="button button-big button--bg-success" type="submit">Sauvegarder</button>
        </div>
        <input type="hidden" name="_method" value="PATCH">
    </form>
</div>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
