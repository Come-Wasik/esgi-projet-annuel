<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>CMS | Création de blog</title>
    <link rel="stylesheet" href="/css/main.css">
    <?= $_sectionHeader ?? null ?>
</head>

<body class="container-full">
    <div class="row">
        <nav class="col-sm-4 col-md-4 col-lg-2 h-screen shadow-r-inset">
            <div class="container h-full">
                <div class="row align-center h-full">
                    <nav class="col-md-10 h-full dashboard">
                        <a href="#" class="title"><?= Config::get('cms.blog_name') ?></a>
                        <a href="<?= getRouteUrl('dashboard') ?>" class="">Dashboard</a>
                        <?php if (in_array(\User::role(), ['Administrator'])) : ?>
                            <a href="<?= getRouteUrl('pages.index') ?>" class="">Pages</a>
                        <?php endif; ?>
                        <?php if (in_array(\User::role(), ['Administrator', 'Redactor'])) : ?>
                            <a href="<?= getRouteUrl('articles.index') ?>" class="">Articles</a>
                            <a href="<?= getRouteUrl('categories.index') ?>" class="">Catégories</a>
                            <a href="<?= getRouteUrl('tags.index') ?>" class="">Tags</a>
                        <?php endif; ?>
                        <?php if (in_array(\User::role(), ['Administrator', 'Moderator'])) : ?>
                            <a href="<?= getRouteUrl('comments.index') ?>" class="">Commentaires</a>
                        <?php endif; ?>
                        <?php if (in_array(\User::role(), ['Administrator'])) : ?>
                            <a href="<?= getRouteUrl('users.index') ?>" class="">Utilisateurs</a>
                            <a href="<?= getRouteUrl('config.edit') ?>" class="">Paramètres</a>
                        <?php endif; ?>
                    </nav>
                </div>
            </div>
        </nav>
        <section class="col-sm-8 col-md-10 col-lg-10 bg-gray-2 h-screen">
            <header class="row bg-white py-3 shadow-b">
                <div class="col-md-12">
                    <div class="flex items-center justify-end px-16 py-2">
                        <span class="text-md font-medium mr-1 fl-upper"><?= \User::name() ?></span>
                        <a href="<?= getRouteUrl('app.process.deconnexion') ?>">
                            <img class="w-6 h-6" src="/images/pictograms/off_button.svg" alt="articles picture">
                        </a>
                    </div>
                </div>
            </header>
            <main class="row">
                <div class="col-md-12">
                    <?= $_sectionContent ?>
                </div>
            </main>
        </section>
    </div>
</body>

</html>