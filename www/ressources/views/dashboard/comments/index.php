<?php ob_start() ?>
<div class="container py-12">
    <div class="flex items-center justify-between">
        <h1 class="text-blue-3 text-2xl font-medium"><span class="border-b-2 border-blue-3">Les</span> commentaires</h1>
    </div>
    <?php if (Flash::has('success')) : ?>
        <div class="alert alert--success my-2">
            <?= Flash::get('success') ?>
        </div>
    <?php endif; ?>
    <section>
        <table class="w-full" style="border-collapse: collapse">
            <thead>
                <tr class="text-left border-b">
                    <th class="pr-4 py-2">ID</th>
                    <th class="pr-4 py-2">Content</th>
                    <th class="px-4 py-2">Article title</th>
                    <th class="px-4 py-2">Auteur</th>
                    <th class="px-4 py-2">Created at</th>
                    <th class="px-4 py-2">Updated at</th>
                    <th class="px-4 py-2">Validated ?</th>
                    <th class="px-4 py-2">Action</th>
                </tr>
            </thead>
            <tbody class="">
                <?php foreach ($comments as $comment) : ?>
                    <tr class="border-b">
                        <td class="pr-4 py-3"><?= $comment->getId(); ?></td>
                        <td class="pr-4 py-3"><?= $comment->getContent(); ?></td>
                        <td class="px-4 py-3"><?= $comment->article->getName(); ?></td>
                        <td class="px-4 py-3">
                            <?php foreach ($users as $user) {
                                if ($user->getId() == $comment->getUsers_id()): ?>
                                     <?= $user->getUsername() ?>
                                <?php endif;
                            }?>
                        </td>
                        <td class="px-4 py-3"><?= formatDatetime($comment->getCreated_at()) ?></td>
                        <td class="px-4 py-3"><?= formatDatetime($comment->getUpdated_at()) ?></td>
                        <td class="px-6 py-3">
                            <?php if ($comment->isValidate()) : ?>
                                <img class=" w-6 h-6" src="/images/pictograms/valid.svg" alt="Edit icon" title="No">
                            <?php else : ?>
                                <img class="w-6 h-6" src="/images/pictograms/none.svg" alt="Plus button" title="Yes">
                            <?php endif; ?>
                        </td>
                        <td class="px-4 py-3">
                            <div class="flex items-center">
                                <?php if ($comment->isValidate()) : ?>
                                    <!-- Bouton d'invalidation -->
                                    <form method="post" action="<?= getRouteUrl('comments.invalidate') ?>">
                                        <input type="hidden" name="_method" value="PATCH">
                                        <button type="submit" name="id_comment" value="<?= $comment->getId() ?>" style="background: transparent; border: none !important; cursor: pointer;">
                                            <img class="w-6 h-6" src="/images/pictograms/none.svg" alt="Plus button" title="Unvalidate">
                                        </button>
                                    </form>
                                <?php else : ?>
                                    <!-- Bouton de validation -->
                                    <form method="post" action="<?= getRouteUrl('comments.validate') ?>" title="Validate">
                                        <input type="hidden" name="_method" value="PATCH">
                                        <button type="submit" name="id_comment" value="<?= $comment->getId() ?>" style="background: transparent; border: none !important; cursor: pointer;">
                                            <img class=" w-6 h-6" src="/images/pictograms/valid.svg" alt="Edit icon">
                                        </button>
                                    </form>

                                <?php endif; ?>
                                <!-- Bouton de suppression -->
                                <form method="post" action="<?= getRouteUrl('comments.delete.dashboard') ?>" title="Delete">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" name="id_comment" value="<?= $comment->getId() ?>" style="background: transparent; border: none !important; cursor: pointer;">
                                        <img class="filter-red w-6 h-6" src="/images/pictograms/Trash.svg" alt="Edit icon">
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if (count($comments) == 0) : ?>
                    <tr class="border-b">
                        <td class="pr-4 py-3">il n'y a aucun article</td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </section>
</div>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
