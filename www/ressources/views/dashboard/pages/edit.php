<?php ob_start() ?>
<script src="https://cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
<script src="/js/editPage.js"></script>
<?php $_sectionHeader = ob_get_clean(); ?>

<?php ob_start() ?>
<div class="container py-12">
    <div class="border-b border-gray-2">
        <h1 class="text-blue-3 text-2xl font-medium mb-2">Editer la page /</h1>
    </div>

    <div class="my-2">
        <?php if ($errors) : ?>
            <div class="alert alert--danger">
                <?php foreach ($errors as $field) : ?>
                    <?php foreach ($field as $errorMessage) : ?>
                        <div><?= $errorMessage ?></div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <section>
        <form class="w-full pt-6 pb-8" method="post">
            <div class="flex items-center justify-center mb-5">
                <div class="form-group mr-5">
                    <label for="title" class="text-black">Titre</label>
                    <input type="text" class="" name="title" value="<?= $page->getTitle() ?>" required>
                </div>
                <div class="form-group mr-5">
                    <label for="name" class="text-black">URL</label>
                    <input type="text" class="" name="slug" value="<?= $page->getSlug() ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="text-black">Description</label>
                <textarea class="" name="description"><?= $page->getDescription() ?></textarea>
            </div>

            <div class="form-group mb-10">
                <label for="body" class="">Contenu</label>
                <textarea class="" rows="10" name="body" id="editor" required>
                        <?= $page->getBody() ?>
                    </textarea>
            </div>
            <input class="mr-2" type="checkbox" name="published" id="published" <?php if ($page->isPublished()) : ?> checked <?php endif; ?>><label for="published">Publier la page ?</label>
            <div class="flex items-center justify-end">
                <button class="button button--bg-success">Sauvegarder</button>
            </div>
        </form>
    </section>
</div>
<script>
    initSample();
</script>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
