<?php ob_start() ?>
<div class="container py-12">
    <div class="flex items-center justify-between">
        <h1 class="text-blue-3 text-2xl font-medium"><span class="border-b-2 border-blue-3">Gestions</span> des pages</h1>
        <a class="" href="<?= getRouteUrl('pages.create') ?>">
            <img class="filter-blue w-10 h-10" src="/images/pictograms/add_button.svg" alt="Create icons" title="Create a new page">
        </a>
    </div>
    <?php if (Flash::has('success')) : ?>
        <div class="alert alert--success my-2">
            <?= Flash::get('success') ?>
        </div>
    <?php endif; ?>
    <?php if (Flash::has('error')) : ?>
        <div class="alert alert--danger my-2">
            <?= Flash::get('error') ?>
        </div>
    <?php endif; ?>
    <section>
        <table class="w-full" style="border-collapse: collapse">
            <thead>
                <tr class="text-left border-b">
                    <th class="pr-4 py-2">Titre</th>
                    <th class="px-4 py-2">URL</th>
                    <th class="px-4 py-2">Publié</th>
                    <th class="px-4 py-2">Date de modification</th>
                    <th class="px-4 py-2 text-center">Action</th>
                </tr>
            </thead>
            <tbody class="">
                <?php foreach ($pages as $page) : ?>
                    <tr class="border-b">
                        <td class="pr-4 py-3"><?= $page->getTitle() ?></td>
                        <td class="px-4 py-3"><?= $page->getSlug() ?></td>
                        <td class="px-4 py-3"><?= ($page->isPublished() ? 'Oui' : 'Non') ?></td>
                        <td class="px-4 py-3"><?= formatDatetime($page->getUpdated_at()) ?></td>
                        <td class="px-4 py-3">
                            <div class="flex items-center justify-evenly">
                                <!-- deletion -->
                                <?php if ($page->getId() != 1) : ?>
                                    <form method="post" action="<?= getRouteUrl('pages.delete') ?>">
                                        <button type="submit" name="id_page" value="<?= $page->getId() ?>" style="background: transparent; border: none !important; cursor: pointer;">
                                            <img class="filter-red w-6 h-6" src="/images/pictograms/Trash.svg" alt="Delete icons" title="Delete">
                                        </button>
                                    </form>
                                <?php endif ?>
                                <!-- Edition -->
                                <a href="<?= getRouteUrl('pages.edit') . $page->getId() ?>" class="block">
                                    <img class="filter-blue w-6 h-6" src="/images/pictograms/edit.svg" alt="Edit icons" title="Edit">
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </section>
</div>

<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
