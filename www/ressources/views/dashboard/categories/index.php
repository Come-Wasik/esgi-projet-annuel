<?php ob_start() ?>
<div class="container py-12">
    <div class="flex items-center justify-between">
        <h1 class="text-blue-3 text-2xl font-medium"><span class="border-b-2 border-blue-3">Gestions</span> des catégories</h1>
        <a href="<?= getRouteUrl('categories.create') ?>">
            <img class="filter-blue w-10 h-10" src="/images/pictograms/add_button.svg" alt="Plus button" title="Create a new category">
        </a>
    </div>
    <?php if (Flash::has('success')) : ?>
        <div class="alert alert--success my-2">
            <?= Flash::get('success') ?>
        </div>
    <?php endif; ?>
    <section>
        <table class="w-full" style="border-collapse: collapse">
            <thead>
                <tr class="text-left border-b">
                    <th class="pr-4 py-2">Id</th>
                    <th class="px-4 py-2">Nom de la catégorie</th>
                    <th class="px-4 py-2">Action</th>
                </tr>
            </thead>
            <tbody class="">
                <?php foreach ($categories as $category) : ?>
                    <tr class="border-b">
                        <td class="pr-4 py-3"><?= $category->getId() ?></td>
                        <td class="px-4 py-3"><?= $category->getName() ?></td>
                        <td class="px-4 py-3">
                            <div class="flex items-center">
                                <form method="POST" action="<?= getRouteUrl('categories.delete') ?>">
                                    <button type="submit" name="id_category" value="<?= $category->getId() ?>" class="no-button" style="width: 40px;">
                                        <img class="filter-red w-6 h-6" src="/images/pictograms/Trash.svg" alt="Plus button" title="Delete">
                                    </button>
                                </form>
                                <a href="<?= getRouteUrl('categories.edit') . $category->getId() ?>" class="block">
                                    <img class="filter-blue w-6 h-6" src="/images/pictograms/edit.svg" alt="Plus button" title="Edit">
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </section>
</div>

<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
