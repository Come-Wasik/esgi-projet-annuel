<?php ob_start() ?>
<div class="container py-20">
    <div class="row wrap-on-phone align-between">
        <div class="col-sm-12 col-md-6 col-md-4">
            <aside class="bg-white shadow-lg mx-4 md-cancel-x-margin py-8 mb-4">
                <div class="flex items-center justify-center mt-3">
                    <img class="w-10 h-10 filter-blue" src="/images/pictograms/article.svg" alt="articles picture">
                </div>
                <div class="text-center">
                    <h5 class="text-2xl font-medium text-black"><?= $articleCount ?></h5>
                    <h5 class="text-xl font-medium text-gray-4">Article(s)</h5>
                </div>
            </aside>
        </div>
        <div class="col-sm-12 col-md-6 col-md-4">
            <aside class="bg-white shadow-lg mx-4 md-cancel-x-margin py-8 mb-4">
                <div class="flex items-center justify-center mt-3">
                    <img class="w-10 h-10 filter-blue" src="/images/pictograms/page.svg" alt="pages picture">
                </div>
                <div class="text-center">
                    <h5 class="text-2xl font-medium text-black"><?= $pageCount ?></h5>
                    <h5 class="text-xl font-medium text-gray-4">Page(s)</h5>
                </div>
            </aside>
        </div>

        <div class="col-sm-12 col-md-6 col-md-4">
            <aside class="bg-white shadow-lg mx-4 md-cancel-x-margin py-8">
                <div class="flex items-center justify-center mt-3">
                    <img class="w-10 h-10 filter-blue" src="/images/pictograms/user.svg" alt="user picture">
                </div>
                <div class="text-center">
                    <h5 class="text-2xl font-medium text-black"><?= $userCount ?></h5>
                    <h5 class="text-xl font-medium text-gray-4">Utilisateur(s)</h5>
                </div>
            </aside>
        </div>
    </div>
</div>

<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
