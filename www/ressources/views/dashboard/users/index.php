<?php ob_start() ?>
<div class="container py-12">
    <div class="flex items-center justify-between">
        <h1 class="text-blue-3 text-2xl font-medium">Utilisateurs</h1>
        <a class="" href="<?= getRouteUrl('users.create') ?>">
            <img class="filter-blue w-10 h-10" src="/images/pictograms/add_button.svg" alt="Plus button" title="Create a new user">
        </a>
    </div>
    <?php if (Flash::has('success')) : ?>
        <div class="alert alert--success my-2">
            <?= Flash::get('success') ?>
        </div>
    <?php endif; ?>
    <section>
        <table class="w-full" style="border-collapse: collapse">
            <thead>
                <tr class="text-left border-b">
                    <th class="pr-4 py-2">ID</th>
                    <th class="px-4 py-2">Username</th>
                    <th class="px-4 py-2">Nom & Prénom</th>
                    <th class="px-4 py-2">Email</th>
                    <th class="px-4 py-2">Role</th>
                    <th class="px-4 py-2">Status</th>
                    <th class="px-4 py-2">Created_at</th>
                    <th class="px-4 py-2">Action</th>
                </tr>
            </thead>
            <tbody class="">
                <?php foreach ($users as $user) : ?>
                    <tr class="border-b">
                        <td class="pr-4 py-3"><?= $user->getId(); ?></td>
                        <td class="px-4 py-3"><?= $user->getUsername(); ?></td>
                        <td class="px-4 py-3"><?= $user->getFirstname(); ?> <?= $user->getLastname(); ?></td>
                        <td class="px-4 py-3"><?= $user->getEmail(); ?></td>
                        <td class="px-4 py-3">
                            <?php
                            foreach ($roles as $role) {
                                if ($user->getRole_id() == $role->getId()) {
                                    echo $role->getName();
                                }
                            }
                            ?>
                        </td>
                        <td class="px-6 py-3">
                            <?php if ($user->getStatus() === 'enabled' && $user->mailIsVerified()) : ?>
                                <img class=" w-6 h-6" src="/images/pictograms/valid.svg" alt="Valid icon" title="Account enabled">
                            <?php elseif (!$user->mailIsVerified()) : ?>
                                <img class=" w-6 h-6" src="/images/pictograms/none.svg" alt="Cancel icon" title="Email not verified">
                            <?php else : ?>
                                <img class=" w-6 h-6" src="/images/pictograms/none.svg" alt="Cancel icon" title="Account disabled">
                            <?php endif; ?>
                        </td>

                        <td class=" px-4 py-3"><?= formatDatetime($user->getCreated_at()) ?></td>
                        <td class="px-4 py-3">
                            <div class="flex items-center">
                                <form method="POST" action="<?= getRouteUrl('users.delete') ?>" title="Delete">
                                    <button type="submit" name="id_user" value="<?= $user->getId() ?>" class="no-button" style="width: 40px;">
                                        <img class="filter-red w-6 h-6" src="/images/pictograms/Trash.svg" alt="Plus button" title="Remove">
                                    </button>
                                </form>

                                <a href="<?= getRouteUrl('users.edit') . $user->getId() ?>" class="block" title="Edit">
                                    <img class="filter-blue w-6 h-6" src="/images/pictograms/edit.svg" alt="Plus button" title="Edit">
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </section>
</div>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
