<?php ob_start() ?>
<div class="container py-12">
    <div class="row">
        <div class="col-md-4">
            <h1 class="text-md font-normal">Créer un nouvel utilisateur</h1>
            
            <?php if ($errors): ?>
                <div class="alert alert--danger">
                <?php foreach ($errors as $field): ?>
                    <?php foreach ($field as $errorMessage): ?>
                        <div><?= $errorMessage ?></div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-md-8">
            <form class="w-full pt-6 pb-8" method="POST">
                <div class="flex items-center justify-center mb-5">
                    <div class="form-group mr-5">
                        <label for="" class="text-black">Username</label>
                        <input type="text" class="" name="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="" class="">Email</label>
                        <input type="email" class="" name="email" placeholder="Email">
                    </div>
                </div>
                <div class="flex items-center justify-center mb-5">
                    <div class="form-group mr-5">
                        <label for="" class="">Prénom</label>
                        <input type="text" class="" name="firstname" placeholder="Prénom">
                    </div>
                    <div class="form-group">
                        <label for="" class="">Nom</label>
                        <input type="text" class="" name="lastname" placeholder="Nom">
                    </div>
                </div>
                <div class="form-group mb-5">
                    <label for="" class="">Mot de passe</label>
                    <input type="password" class="" name="password" placeholder="Mot de passe">
                </div>
                <div class="form-group mb-5">
                    <label for="" class="">Confirmation de mot de passe</label>
                    <input type="password" class="" name="passwordConfirm" placeholder="Confirmation de mot de passe">
                </div>
                <div class="w-full mb-5">
                    <label for="" class="block text-black text-sm mb-2">Rôle</label>
                    <div class="relative">
                        <select class="block w-full text-sm text-gray-3 no-border py-3 px-4 pr-8 rounded" name="role_id" id="role_id">
                            <?php foreach ($roles as $role): ?>
                                <option value="<?= $role->getId() ?>"><?= $role->getName() ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <input class="mr-2" type="checkbox" name="status" checked><label>Activate the account?</label>

                <div class="flex items-center justify-end my-12">
                    <button class="button button-big button--bg-success" type="submit">Ajouter l'utilisateur</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
