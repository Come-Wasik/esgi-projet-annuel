<?php ob_start() ?>
<div class="container py-12">
    <div class="row">
        <div class="col-md-4">
            <h1 class="text-md font-normal">Mettre à jour l'utilisateur <?= $user->getUsername() ?></h1>

            <?php if ($errors) : ?>
                <div class="alert alert--danger">
                    <?php foreach ($errors as $field) : ?>
                        <?php foreach ($field as $errorMessage) : ?>
                            <div><?= $errorMessage ?></div>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-md-8">
            <form class="w-full pt-6 pb-8" method="POST">
                <div class="flex items-center justify-center mb-5">
                    <div class="form-group mb-5 mr-5">
                        <label for="" class="text-black">Username</label>
                        <input type="text" class="" name="username" placeholder="Username" value="<?= $user->getUsername() ?>">
                    </div>
                    <div class="form-group mb-5">
                        <label for="" class="">Email</label>
                        <input type="email" class="" name="email" placeholder="Email" value="<?= $user->getEmail() ?>">
                    </div>
                </div>
                <div class="flex items-center justify-center mb-5">
                    <div class="form-group mr-5">
                        <label for="" class="">Prénom</label>
                        <input type="text" class="" name="firstname" placeholder="Prénom" value="<?= $user->getFirstname() ?>">
                    </div>
                    <div class="form-group">
                        <label for="" class="">Nom</label>
                        <input type="text" class="" name="lastname" placeholder="Nom" value="<?= $user->getLastname() ?>">
                    </div>
                </div>
                <div class="form-group mb-5">
                    <label for="" class="">Nouveau mot de passe</label>
                    <input type="password" class="" name="password" placeholder="Mot de passe">
                </div>
                <div class="form-group mb-5">
                    <label for="" class="">Confirmation nouveau de mot de passe</label>
                    <input type="password" class="" name="passwordConfirm" placeholder="Confirmation de mot de passe">
                </div>
                <div class="w-full mb-5">
                    <label for="" class="block text-black text-sm mb-2">Rôle</label>
                    <div class="relative">
                        <select class="block w-full text-sm text-gray-3 no-border py-3 px-4 pr-8 rounded" name="role_id" id="role_id">
                            <?php foreach ($roles as $role) : ?>
                                <option value="<?= $role->getId() ?>" <?php if ($user->getRole_id() == $role->getId()) : ?> selected <?php endif; ?>><?= $role->getName() ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <input class="mr-2" type="checkbox" name="status" id="status" <?php if ($user->getStatus() == 'enabled') : ?> checked <?php endif; ?>><label for="status">Activate the account?</label>

                <div class="flex items-center justify-end my-12">
                    <button class="button button-big button--bg-success" type="submit">Mettre à jour l'utilisateur</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
