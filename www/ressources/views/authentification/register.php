<?php ob_start() ?>
<div class="bg-gray-3 min-h-screen">
    <div class="container">
        <div class="row align-center">
            <div class="col-lg-6 col-md-8">
                <h2 class="text-center text-4xl text-white font-semibold mb-3">S'inscrire</h2>

                <?php if (Flash::has('success')) : ?>
                    <div class="alert alert--success my-2">
                        <?= Flash::get('success') ?>
                    </div>
                <?php endif; ?>

                <?php if (Flash::has('error')) : ?>
                    <div class="alert alert--danger my-2">
                        <?= Flash::get('error') ?>
                    </div>
                <?php endif; ?>

                <?php if ($errors) : ?>
                    <div class="alert alert--danger">
                        <?php foreach ($errors as $field) : ?>
                            <?php foreach ($field as $errorMessage) : ?>
                                <div><?= $errorMessage ?></div>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <form class="w-full pt-6 pb-8" method="post">
                    <div class="flex items-center justify-center mb-5">
                        <div class="form-group mr-5">
                            <label for="" class="text-white">Prénom</label>
                            <input type="text" class="" name="firstname" placeholder="Prénom">
                        </div>
                        <div class="form-group">
                            <label for="" class="text-white">Nom</label>
                            <input type="text" class="" name="lastname" placeholder="Nom">
                        </div>
                    </div>
                    <div class="form-group mb-5">
                        <label for="" class="text-white">Username</label>
                        <input type="text" class="" name="username" placeholder="Username">
                    </div>
                    <div class="form-group mb-5">
                        <label for="" class="text-white">Email</label>
                        <input type="email" class="" name="email" placeholder="Email">
                    </div>
                    <div class="form-group mb-5">
                        <label for="" class="text-white">Mot de passe</label>
                        <input type="password" class="" name="password" placeholder="Mot de passe">
                    </div>
                    <div class="form-group">
                        <label for="" class="text-white">Confirmation de mot de passe</label>
                        <input type="password" class="" name="passwordConfirm" placeholder="Confirmation de mot de passe">
                    </div>
                    <div class="flex items-center justify-center mt-12">
                        <a href="<?= getRouteUrl('app.reset-password.send-email.form') ?>" class="text-white no-underline button button-big button--bg-primary mr-6">Reset your password</a>
                        <button class="button button-big button--bg-success" type="submit">S'inscrire</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $_section_main = ob_get_clean();

require view_path() . '/authentification/template.php';
