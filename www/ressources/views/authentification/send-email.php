<?php ob_start() ?>
<div class="bg-gray-3 min-h-screen">
    <div class="container">
        <div class="row align-center">
            <div class="col-lg-6 col-md-8">
                <h2 class="text-center text-4xl text-white font-semibold">Reset password</h2>
                <form class="w-full pt-6 pb-8" method="post">
                    <div class="form-group mb-10">
                        <label for="" class="text-white">Email</label>
                        <input type="text" class="" name="email" placeholder="Email">
                    </div>
                    <div class="flex items-center justify-center my-12">
                        <button class="button button-big button--bg-success" type="submit">Send email</button>
                    </div>
                </form>
            </div>
        </div>

        <?php if (Flash::has('success')) : ?>
            <div class="alert alert--success my-2">
                <?= Flash::get('success') ?>
            </div>
        <?php endif; ?>

        <?php if (Flash::has('error')) : ?>
            <div class="alert alert--danger my-2">
                <?= Flash::get('error') ?>
            </div>
        <?php endif; ?>

        <?php if ($errors) : ?>
            <div class="alert alert--danger">
                <?php foreach ($errors as $field) : ?>
                    <?php foreach ($field as $errorMessage) : ?>
                        <div><?= $errorMessage ?></div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php $_section_main = ob_get_clean();

require view_path() . '/authentification/template.php';
