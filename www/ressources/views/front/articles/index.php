<?php $_title = 'Mes articles' ?>
<?php ob_start() ?>
<section class="container-full bg-blue-3 py-3">
    <div class="row">
        <div class="col-lg-3 col-sm-3"></div>
        <div class="col-lg-3">
            <h1 class="text-white text-4xl font-semibold mt-0">
                <span class="border-b-2">Our</span> articles <span class="text-normal">(<?= count($articles) ?>)</span>
            </h1>
        </div>
        <div class="col-lg-3 col-sm-1"></div>
        <div class="col-lg-3 col-sm-1"></div>
    </div>
</section>
</section>
<section class="container">
    <div class="row wrap-on-phone">
        <?php foreach ($articles as $article) : ?>
            <article class="col-sm-12 col-md-12 col-lg-4 mx-auto grid-wrap p-12 mt-4 shadow-on-hover" style="border-color: #10275D;">
                <a href="<?= getRouteUrl('posts.show') . $article->getId() ?>" class="no-underline text-black">
                    <main class=" border-b border-gray-1">
                        <h3 class="text-black text-xl text-normal font-medium mb-1 fl-upper">
                            <span class="text-2xl">
                                <?php foreach ($users as $user) {
                                    echo ($user->getId() === $article->getAuthor_id()) ? $user->getUsername() : '';
                                } ?>
                            </span>
                            wrote
                        </h3>
                        <span class="block text-gray-3 text-md">
                            <?php
                            $now = new DateTime('now');
                            $createdAt = new DateTime($article->getCreated_at());
                            $interval = $now->diff($createdAt);
                            echo $interval->format('%a day(s), %h hour(s), %i minute(s)   ');
                            ?>
                        </span>
                        <h2 class="text-3xl font-medium m-0 pt-2">
                            <?= $article->getName() ?>
                        </h2>
                        <p class="text-xl font-200 my-2">
                            <?= $article->getDescription() ?>
                        </p>
                        <div class="text-center">&rarr;</div>
                    </main>
                </a>
            </article>
        <?php endforeach; ?>
        <?php if (empty($articles)) : ?>
            Il n'y a aucun article pour le moment
        <?php endif; ?>
    </div>
</section>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/front/templates/' . Config::get('cms.template_front');
