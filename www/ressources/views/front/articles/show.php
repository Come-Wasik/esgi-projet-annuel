<?php $_title = $article->getName() ?>

<?php ob_start() ?>
<div class="bg-blue-3 py-12"></div>
<main class="container" style="transform: translateY(-60px);">
    <section class="row align-center">
        <aside class="col-sm-12 col-lg-8 rounded-sm shadow-lg bg-white">
            <div class="px-6 py-6">
                <h1 class="text-black text-4xl font-medium mb-0 mt-4"><?= $article->getName() ?></h1>
                <h2 class="text-black text-2xl font-normal my-0">By
                    <?= $user->getUsername() ?>
                </h2>
                <div class="text-xl fl-lg">
                    <?= $article->getContent() ?>
                </div>
            </div>
            <div class="px-6 py-6 text-lg">
                <h3 class="mb-0 text-2xl">Liste des commentaires :</h3>
                <?php if (!empty($comments)) : ?>
                    <?php foreach ($comments as $comment) : ?>
                        <div class="mb-6">
                            <div class="flex items-center justify-between mt-1">
                                <h4 class="font-100 mt-0 mb-0"><span class="font-semibold"><?= $comment->getAuthor()->getUsername() ?></span> à écrit :</h4>
                                <?php if ($comment->getAuthor()->getId() == User::id()) : ?>
                                    <div class="mt-2">
                                        <form method="post" action="<?= getRouteUrl('comments.delete') ?>">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" name="id_comment" value="<?= $comment->getId() ?>" style="background: transparent; border: none !important; cursor: pointer;">
                                                <img class="filter-red w-6 h-6" src="/images/pictograms/Trash.svg" alt="Plus button" title="Remove your comment">
                                            </button>
                                        </form>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="mt-O"><?= $comment->getContent() ?></div>
                            <div class="mt-1">Le <?= formatDatetime($comment->getCreated_at()) ?></div>
                        </div>
                    <?php endforeach; ?>
                <?php else : ?>
                    <div class="mt-4">Il n'y a aucun commentaire pour cet article</div>
                <?php endif; ?>

                <?php if (User::isConnected()) : ?>
                    <!-- Ecrire un commentaire -->
                    <?php if (Flash::has('success')) : ?>
                        <div class="alert alert--success my-2">
                            <?= Flash::get('success') ?>
                        </div>
                    <?php endif; ?>

                    <?php if (Flash::has('error')) : ?>
                        <div class="alert alert--danger my-2">
                            <?= Flash::get('error') ?>
                        </div>
                    <?php endif; ?>

                    <form class="w-full pb-8 pt-1" method="POST" action="<?= getRouteUrl('comments.store') . $article->getId() . '/comments' ?>">
                        <div class="form-group mb-10">
                            <label for="comment" class="mt-6">Poster un commentaire</label>
                            <textarea type="text" class="" rows="5" name="content" id="content" placeholder="Le contenu de votre commentaire" required></textarea>
                        </div>
                        <div class="flex items-center justify-end">
                            <button class="button button--bg-success text-xl">Ajouter</button>
                        </div>
                    </form>
                <?php else : ?>
                    <p class="text-center">Vous devez être connecté pour poster des commentaires. <a href="<?= getRouteUrl('app.form.connexion') ?>" class="text-blue-4">Connectez vous</a></p>
                <?php endif; ?>
            </div>
        </aside>
    </section>
</main>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/front/templates/' . Config::get('cms.template_front');
