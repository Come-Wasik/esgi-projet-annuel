<?php $_title = 'Profile configuration' ?>
<?php ob_start() ?>
<section class="container-full bg-blue-3 py-3">
    <div class="row">
        <div class="col-lg-3 col-sm-3"></div>
        <div class="col-lg-5">
            <h1 class="text-white text-4xl font-semibold mt-0">
                <span class="border-b-2">Profile</span> configuration <span class="text-normal"></span>
            </h1>
        </div>
        <div class="col-lg-4 col-sm-1"></div>
    </div>
</section>
</section>
<section class="container pt-3">
    <div class="row align-center">
        <div class="col-lg-8">
            <?php if ($errors) : ?>
                <div class="alert alert--danger">
                    <?php foreach ($errors as $field) : ?>
                        <?php foreach ($field as $errorMessage) : ?>
                            <div><?= $errorMessage ?></div>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <form class="w-full pt-6 pb-8" method="POST">
                <div class="flex items-center justify-center mb-5">
                    <div class="form-group mb-5 mr-5 bg-white">
                        <label for="" class="text-black">Username</label>
                        <input type="text" class="" name="username" placeholder="Username" value="<?= $user->getUsername() ?>">
                    </div>
                    <div class="form-group mb-5">
                        <label for="" class="">Email</label>
                        <input type="email" class="" name="email" placeholder="Email" value="<?= $user->getEmail() ?>">
                    </div>
                </div>
                <div class="flex items-center justify-center mb-5">
                    <div class="form-group mr-5">
                        <label for="" class="">Prénom</label>
                        <input type="text" class="" name="firstname" placeholder="Prénom" value="<?= $user->getFirstname() ?>">
                    </div>
                    <div class="form-group">
                        <label for="" class="">Nom</label>
                        <input type="text" class="" name="lastname" placeholder="Nom" value="<?= $user->getLastname() ?>">
                    </div>
                </div>
                <div class="form-group mb-5">
                    <label for="" class="">Nouveau mot de passe</label>
                    <input type="password" class="" name="password" placeholder="Mot de passe">
                </div>
                <div class="form-group mb-5">
                    <label for="" class="">Confirmation nouveau de mot de passe</label>
                    <input type="password" class="" name="passwordConfirm" placeholder="Confirmation de mot de passe">
                </div>
                <div class="flex items-center justify-end my-12">
                    <button class="button button-big button--bg-success" type="submit">Update my profile</button>
                </div>

                <input type="hidden" name="_method" value="PATCH">
            </form>
        </div>

    </div>
</section>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/front/templates/' . Config::get('cms.template_front');
