<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title><?= $_title ?? 'Blogify' ?></title>
    <link rel="stylesheet" href="/css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.hamburger-button').click(function() {
                $('.nav-content').toggleClass("show-on-computer");
            });
        })
    </script>
</head>

<body class="container-full flex flex-col bg-personal-color-1">
    <header class="col-sm-12 col-md-12 col-lg-12 bg-blue-3" style="flex-basis: initial;">
        <nav class="container-full py-6">
            <div class="row items-center justify-between wrap-on-tablet text-center">
                <div class="col-sm-12 col-lg-3 row">
                    <div class="show-max-tablet col-sm-2 items-center">
                        <img class="hamburger-button w-10" src="/images/pictograms/hamburger-button.svg" alt="hamburger menu" style="cursor: pointer;">
                    </div>
                    <div class="show-max-tablet col-sm-8">
                        <a href="/" class="text-4xl font-semibold text-white no-underline ">
                            <?= Config::get('cms.blog_name') ?>
                        </a>
                    </div>
                    <div class="show-on-computer col-md-12">
                        <a href="/" class="text-4xl font-semibold text-white no-underline ml-4 align-center"><?= Config::get('cms.blog_name') ?></a>
                    </div>

                </div>
                <div class="col-md-5 row wrap-on-tablet align-center show-on-computer nav-content">
                    <a href="/" class="text-xl text-white no-underline mr-4 px-2 py-2 inline-block">Accueil</a>
                    <a href="<?= getRouteUrl('posts.index') ?>" class="text-xl text-white no-underline mr-4 px-2 py-2">Articles</a>
                    <?php foreach ($pagesInNavbar as $navElement) : ?>
                        <a href="<?= $navElement->getSlug() ?>" class="text-xl text-white no-underline mr-4 px-2 py-2"><?= $navElement->getTitle() ?></a>
                    <?php endforeach; ?>
                </div>
                <div class="col-md-4 row wrap-on-tablet align-center show-on-computer nav-content">
                    <?php if (User::isConnected()) : ?>
                        <?php if (in_array(\User::role(), ['Administrator', 'Moderator', 'Redactor'])) : ?>
                            <a href="<?= getRouteUrl('dashboard') ?>" class="text-lg text-white no-underline mr-4 px-2 py-2">Dashboard</a>
                        <?php endif; ?>
                        <a href="<?= getRouteUrl('app.process.deconnexion') ?>" class="text-lg text-white no-underline mr-4 px-2 py-2">Logout</a>
                        <span class="text-white no-underline mr-4 px-2 py-2 show-min-tablet">
                            <span class="text-md">
                                <a href="<?= getRouteUrl('user-param.edit') ?>" class="fl-upper no-underline text-white">
                                    <?= User::name() ?>'s profile
                                </a>
                            </span>
                        </span>
                    <?php else : ?>
                        <a href="<?= getRouteUrl('app.form.inscription') ?>" class="text-lg text-white no-underline mr-4 px-2 py-2">Register</a>
                        <a href="<?= getRouteUrl('app.form.connexion') ?>" class="text-lg text-white no-underline mr-4 px-2 py-2">Login</a>
                    <?php endif; ?>
                </div>
            </div>
        </nav>
    </header>
    <main class="flex-1">
        <?= $_sectionContent ?>
    </main>
</body>

</html>