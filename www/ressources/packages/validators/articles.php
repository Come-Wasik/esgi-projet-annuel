<?php

return [
    'name' => 'required|min:1|max:50|type:string',
    'categories_id' => 'required|type:int',
    'content' => 'required|min:1|type:string',
    'description' => 'required|min:1|max:50|type:string',
    'tags_id' => 'type:array',
    // 'published' => '',
];
