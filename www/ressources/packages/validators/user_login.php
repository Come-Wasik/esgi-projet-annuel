<?php

return [
    'email' => 'required|min:1|max:50|type:string',
    'password' => 'required|min:8|max:40|type:string'
];
