<?php

return [
	'email' => 'required|min:1|type:string|format:email'
];
