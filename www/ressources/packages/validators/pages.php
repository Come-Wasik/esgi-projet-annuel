<?php

return [
	'title' => 'required|min:1|max:50|type:string',
	'slug' => 'required|min:1|max:2048|type:string',
	'description' => 'required|min:1|max:500|type:string',
	'body' => 'required|min:1|type:string'
];
