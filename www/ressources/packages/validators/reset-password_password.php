<?php

return [
	'password' => 'min:8|max:40|type:string',
	'passwordConfirm' => 'min:8|max:40|type:string',
];
