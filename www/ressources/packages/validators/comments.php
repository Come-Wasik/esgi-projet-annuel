<?php

return [
    'content' => 'required|min:1|max:800|type:string'
];
