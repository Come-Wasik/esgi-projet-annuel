<?php

return [
    'email' => 'required|min:1|max:50|type:string|format:email',
    'password' => 'required|min:8|max:40|type:string',
    'passwordConfirm' => 'required|min:8|max:40|type:string',
    'username' => 'required|min:1|max:50|type:string',
    'firstname' => 'min:1|max:50|type:string',
    'lastname' => 'min:1|max:50|type:string',
];
