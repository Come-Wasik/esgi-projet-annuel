<?php

return [
    'name' => 'required|min:1|max:50|type:string',
    'description' => 'min:1|max:100|type:string',
];
