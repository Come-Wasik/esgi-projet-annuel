<?php

return [
    'username' => 'required|min:1|max:50|type:string',
    'password' => 'min:8|max:40|type:string',
    'passwordConfirm' => 'min:8|max:40|type:string',
    'email' => 'required|min:1|max:50|type:string|format:email',
    'role_id' => 'required|type:int',
    'firstname' => 'min:1|max:50|type:string',
    'lastname' => 'min:1|max:50|type:string',
    // 'status' => ''
];
