<?php

return [
	'blog_name' => 'required|min:1|max:17|type:string',
	'dashboard_path' => 'required|min:1|type:string',
	'article_path' => 'required|min:1|type:string',
	'template_front' => 'required|type:string',
];
