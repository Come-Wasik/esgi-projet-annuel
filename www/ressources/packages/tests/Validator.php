<?php

namespace Framework\Test;

use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use Config;

class Validator
{
    public function tryValidate()
    {
        \NewRoute::get('test.validator', '/test-validator', function (Request $request) {
            $errors = validate($request->getRequest(), 'test');
            if ($errors->any()) {
                // echo 'Il y a des erreurs';
            }
            vdump($errors);
        });

        Config::set('security.moduleEnabled', false);

        web_request('/test', 'GET', [
            'request' => [
                'field1' => '123',
                'field2' => '12345678901',
                'field3' => 'string@elc'
            ]
        ]);
        # Write yours tests here and launch the command run:test Validator tryMe
    }

    public function test()
    {
        echo "\n";
        $errors = validate([
            'myField' => '0'
        ], [
            'myField' => 'min:-1|max:12|type:int'
        ]);

        dd($errors->all());
    }
}
