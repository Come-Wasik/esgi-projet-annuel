<?php

namespace Framework\Test;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MailerTest
{
	public function sendMail()
	{
		// Instantiation and passing `true` enables exceptions
		$mail = new PHPMailer(true);

		try {
			//Server settings
			$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = MAIL_SMTP_SERVER;                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = MAIL_USERNAME;                     // SMTP username
			$mail->Password   = MAIL_PASSWORD;                             // SMTP password
			$mail->Port       = MAIL_PORT;                                    // TCP port to connect to, use 465

			//Recipients
			$mail->setFrom('from@example.com', 'Mailer');
			$mail->addAddress(MAIL_USERNAME, MAIL_USERNAME);     // Add a recipient

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Message test';
			$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();
			echo 'Message has been sent';
		} catch (Exception $e) {
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	}

	public function useFacade()
	{
		\Mailer::sendMail(function ($mail) {
			//Recipients
			$mail->setFrom('itworks@cms-blog.com', 'Mailer');
			$mail->addAddress('abdoulrahimbah6@gmail.com', 'Monsieur Bah');     // Add a recipient

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Le CMS peut maintenant envoyer des mails !';
			$mail->Body    = 'Avec une Facade <b>Mailer::sendmail</b> et les constantes d\'environnement, on pourra désormais envoyer facilement des mails !';
			$mail->AltBody = 'Avec une Facade Mailer::sendmail et les constantes d\'environnement, on pourra désormais envoyer facilement des mails !';

			return $mail;
		});
	}
}
