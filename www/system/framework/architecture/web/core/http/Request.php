<?php

namespace Framework\Architecture\Web\CoreMaker\Component\Http;

class Request
{
    private $query;
    private $request;
    private $cookie;
    private $session;
    private $server;
    private $header;
    private $files;

    public function __construct(array $data = null)
    {
        ### Hydratation ###
        if ($data) {
            $this->hydrate($data);
        }
    }

    private function hydrate(array $data): void
    {
        foreach ($data as $attName => $attValue) {
            $settername = 'set' . ucfirst($attName);
            $this->$settername($attValue);
        }
    }

    public function fillFromGlobals()
    {
        $this->setQuery($_GET);
        $this->setRequest($_POST);
        $this->setCookie($_COOKIE);
        $this->setSession($_SESSION);
        $this->setServer($_SERVER);
        $this->setHeader($_SERVER);
        $this->setFiles($_FILES);

        $this->setHttpRequest($_REQUEST);
    }

    /**
     * Getters et setters
     */
    public function getQuery(): ?array
    {
        return $this->query;
    }

    public function getRequest(): ?array
    {
        return $this->request;
    }

    public function getCookie(): ?array
    {
        return $this->cookie;
    }

    public function getSession(): ?array
    {
        return $this->session;
    }

    public function getServer(): ?array
    {
        return $this->server;
    }

    public function getHeader(): ?array
    {
        return $this->header;
    }

    public function getFiles(): ?array
    {
        return $this->files;
    }

    public function setQuery(array $query): void
    {
        $this->query = $query;
    }

    public function setRequest(array $request): void
    {
        $this->request = $request;
    }

    public function setCookie(array $cookie): void
    {
        $this->cookie = $cookie;
    }

    public function setSession(array $session): void
    {
        $this->session = $session;
    }

    public function setServer(array $server): void
    {
        $this->server = $server;
    }

    public function setHeader(array $server): void
    {
        $this->header = [];
        foreach ($server as $dataName => $dataContent) {
            if (strpos($dataName, 'HTTP_') !== false) {
                $this->header[] = $dataContent;
            }
        }
    }

    public function setFiles(array $files): void
    {
        $this->files = $files;
    }

    public function setHttpRequest($request): void
    {
        if (key_exists('_method', $request)) {
            $this->server['REQUEST_METHOD'] = $request['_method'];
        }
    }

    /**
     * Appendix method for getters
     */
    public function getHttpMethod()
    {
        return $this->server['REQUEST_METHOD'];
    }

    public function getUri()
    {
        $uri = strtok($this->server['REQUEST_URI'], '?');
        return $uri;
    }
}
