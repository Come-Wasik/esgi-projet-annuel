<?php

namespace Framework\Architecture\Web\CoreMaker\Component\Http;

class Response
{
    // https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
    # 1xx: Informational - Request received, continuing process
    const HTTP_CONTINUE = 100;
    const HTTP_SWITCHING_PROTOCOLS = 101;
    const HTTP_PROCESSING = 102;
    const HTTP_EARLY_HINTS = 103;

    # 2xx: Success - The action was successfully received, understood, and accepted
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NON_AUTHORITATIVE_INFORMATION = 203;
    const HTTP_NO_CONTENT = 204;
    const HTTP_RESET_CONTENT = 205;
    const HTTP_PARTIAL_CONTENT = 206;
    const HTTP_MULTI_STATUS = 207;
    const HTTP_ALREADY_REPORTED = 208;

    const HTTP_CONTENT_DIFFERENT = 210;

    const HTTP_IM_USED = 226;

    # 3xx: Redirection - Further action must be taken in order to complete the request
    const HTTP_MULTIPLE_CHOICES = 300;
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_FOUND = 302;
    const HTTP_SEE_OTHER = 303;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_USE_PROXY = 305;
    const HTTP_SWITCH_PROXY = 30;
    const HTTP_TEMPORARY_REDIRECT = 307;
    const HTTP_PERMANENT_REDIRECT = 308;

    const HTTP_TOO_MANY_REDIRECTS = 310;

    # 4xx: Client Error - The request contains bad syntax or cannot be fulfilled
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_PAYMENT_REQUIRED = 402;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_FOUND = 405;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;
    const HTTP_REQUEST_TIMEOUT = 408;
    const HTTP_CONFLICT = 409;
    const HTTP_GONE = 410;
    const HTTP_LENGHT_REQUIRED = 411;
    const HTTP_PRECONDITION_FAILED = 412;
    const HTTP_PAYLOAD_TOO_LONG = 413;
    const HTTP_URI_TOO_LONG = 414;
    const HTTP_UNSUPPORTED_TYPE = 415;
    const HTTP_RANGE_NOT_ATIFIABLE = 416;
    const HTTP_EXPECTATION_FAILED = 417;
    const HTTP_I_AM_A_TEAPOT = 418;
    const HTTP_MISDIRECTED = 421;
    const HTTP_UNPROCESSABLE_ENTITY = 422;
    const HTTP_LOCKED = 423;
    const HTTP_FAILED_DEPENDENCY = 424;
    const HTTP_TOO_EARLY = 425;
    const HTTP_UPGRADE_REQUIRED = 426;

    const HTTP_PRECONDITION_REQUIRED = 428;
    const HTTP_TOO_MANY_REQUESTS = 429;

    const HTTP_REQUEST_DEADER_FIELDS_TOO_LARGE = 431;

    const HTTP_NO_RESPONSE = 444;                   # Étendu de Nginx
    const HTTP_RETRY_WITH = 449;
    const HTTP_BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS = 450;

    const HTTP_UNAVAIBLE_FOR_LEGAL_REASONS = 451;

    const HTTP_UNRECOVERABLE_ERROR = 456;

    const HTTP_SSL_CERTIFICATE_ERROR = 495;         # Étendu de Nginx
    const HTTP_SSL_CERTIFICATE_REQUIRED = 496;      # Étendu de Nginx
    const HTTP_REQUEST_SENT_TO_HTTPS_PORT = 497;    # Étendu de Nginx
    const HTTP_TOKEN_EXPIRED_INVALID = 498;         # Étendu de Nginx
    const HTTP_CLIENT_CLOSED_REQUEST = 499;         # Étendu de Nginx

    # 5xx: Server Error - The server failed to fulfill an apparently valid request
    const HTTP_INTERNAL_SERVER_ERROR = 500;
    const HTTP_NOT_IMPLEMENTED = 501;
    const HTTP_BAD_GATEWAY = 502;
    const HTTP_SERVICE_UNAVAIBLE = 503;
    const HTTP_GATEWAY_TIMEOUT = 504;
    const HTTP_VERSION_NOT_SUPPORTED = 505;
    const HTTP_VARIANT_ALSO_NEGOTIATES = 506;
    const HTTP_INSUFFICIENT_STORAGE = 507;
    const HTTP_LOOP_DETECTED = 508;
    const HTTP_BANDWIDTH_LIMIT_EXCEEDED = 509;
    const HTTP_NOT_EXTENDED = 510;
    const HTTP_NETWORD_AUTHENTIFICATION_REQUIRED = 511;

    const HTTP_UNKNOW_ERROR = 520;              # Étendu de Cloudflare
    const HTTP_WEB_SERVER_IS_DOWN = 521;        # Étendu de Cloudflare
    const HTTP_CONNECTION_TIMED_OUT = 522;      # Étendu de Cloudflare
    const HTTP_ORIGIN_IS_UNREACHABLE = 523;     # Étendu de Cloudflare
    const HTTP_A_TIMEOUT_OCCURED = 524;         # Étendu de Cloudflare
    const HTTP_SSL_HANDSHAKE_FAILED = 525;      # Étendu de Cloudflare
    const HTTP_INVALID_SSL_CERTIFICATE = 526;   # Étendu de Cloudflare
    const HTTP_RAILGUN_ERROR = 527;             # Étendu de Cloudflare

    private $content;
    private $httpCode;
    private $contentType;

    public function __construct(string $content = '', int $httpCode = 200, string $contentType = 'text/html')
    {
        $this->content = $content;
        $this->httpCode = $httpCode;
        $this->contentType = $contentType;
    }

    public function print(): void
    {
        header('Content-Type: '.$this->contentType);
        http_response_code($this->httpCode);
        print($this->content);
    }

    public function setCode(int $httpCode): void
    {
        $this->httpCode = $httpCode;
    }

    public function setContentType(string $contentType): void
    {
        $this->contentType = $contentType;
    }
}
