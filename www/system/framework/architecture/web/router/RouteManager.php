<?php

/**
 * This file manage route objects for routing system
 *
 * @method addRoute => Add a route in the backpack with no doublon
 * @method getRouteList => Get the actual route list
 * @method searchRoute => Get the route which match with the request
 */

namespace Framework\Architecture\Web\Routage;

use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use Framework\Architecture\Web\Component\Backpack\Route as RouteBp;
use Exception;

class RouteManager
{
    /**
     * @method This method add a route in the RouteBackpack but it take care to do not append a doublon
     * @param Route $newRoute
     * @throws Exception
     */
    public function addRoute(Route $newRoute)
    {
        # Clean Uri
        $newRoute->setUrl(trim($newRoute->getUrl(), '/'));

        # Duplicity test
        $routes = $this->getRouteList();
        if (!empty($routes)) {
            foreach ($routes as $currentRoute) {
                # Test by name
                if ($currentRoute->getName() == $newRoute->getName()) {
                    throw new Exception('The route name ' . $newRoute->getName() . ' already exists.', 500);
                }

                # Test by http method
                if (
                    $currentRoute->getUrl() == $newRoute->getUrl()
                    && (
                        $currentRoute->getMethod() == $newRoute->getMethod()
                        || (
                            strtoupper($currentRoute->getMethod()) == 'MIXED'
                            || strtoupper($newRoute->getMethod()) == 'MIXED'
                        )
                    )
                ) {
                    throw new Exception('The route ' . $newRoute->getName() . ' already exists with the ' . $currentRoute->getMethod() . ' http method.', 500);
                }
            }
        }
        RouteBp::append($newRoute);
    }

    /**
     * This method return the route list actually contained in the backpack
     */
    public function getRouteList()
    {
        return RouteBp::getAll();
    }

    /**
     * @method This method take a request and return a route which match with it
     */
    public function searchRoute(Request $request): ?Route
    {
        # Obtention des données primaires + clean uri
        $url = trim($request->getUri(), '/');
        $method = $request->getHttpMethod();

        # On prévoit au cas où la route comporte une variable, l'initialisation d'un tableau qui contient chaque parties de l'url
        $trimedUrl = trim($url, '/');
        $PartsOfUrl = explode('/', $trimedUrl);

        foreach ($this->getRouteList() as $route) {
            ### Test d'égalité simple ###
            if (
                $route->getUrl() == $url
                && ($route->getMethod() == $method || $route->getMethod() == 'MIXED')
            ) {
                return $route;
            }

            ### test d'égalité avec variable(s) ###
            # On regarde s'il y a des variables dans la route
            $routeVars = $route->getVariableListInUrl();

            if ($routeVars != null) { # null signifie qu'il n'y a pas de variables
                # On initialise un tableau qui contient chaque parties de l'url de la route
                $trimedRouteUrl = trim($route->getUrl(), '/');
                $PartsOfRouteUrl = explode('/', $trimedRouteUrl);

                # On teste la taille de chaque url
                if (
                    \count($PartsOfUrl) == \count($PartsOfRouteUrl)
                ) {
                    # On boucle jusqu'à la fin ou, ce que l'url soit fausse
                    $thisIsThisRoute = true;

                    # On regarde si l'url du navigateur et l'url de la route sont les même, et
                    for ($i = 0; $i < count($PartsOfUrl) && $thisIsThisRoute == true; $i++) {
                        if (
                            $PartsOfUrl[$i] == $PartsOfRouteUrl[$i]
                            || isset($routeVars[$i])
                        ) {
                            if (isset($routeVars[$i])) {
                                $route->addVariable($routeVars[$i], $PartsOfUrl[$i]);
                            }
                        } else {
                            $thisIsThisRoute = false;
                        }
                    }

                    if ($thisIsThisRoute) {
                        return $route;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Fetch a route url with a route name
     *
     * @param string $name A route name
     * @return string
     **/
    public function getUrl(string $name)
    {
        $routes = $this->getRouteList();
        if ($routes) {
            foreach ($routes as $route) {
                if ($route->getName() == $name) {
                    if (($endpos = strpos($route->getUrl(), '{')) !== false) {
                        return '/' . substr($route->getUrl(), 0, $endpos);
                    } else {
                        return '/' . $route->getUrl();
                    }
                }
            }
        }
        return '';
    }
}
