<?php

namespace Framework\Architecture\Web\Component;

use Exception;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class MiddlewareManager
{
    public function runAll(Request $request)
    {
        $listOfMidlewaresPath = \Config::get('middleware_list_path');

        if (!file_exists($listOfMidlewaresPath)) {
            throw new Exception('Listing file for middlewares don\'t exist at ' . $listOfMidlewaresPath, 500);
        }
        $listOfMidlewares = include $listOfMidlewaresPath;

        foreach ($listOfMidlewares as $middleware) {
            $middleware($request);
        }
    }

    public function run(string $middlewareName, Request $request)
    {
        $listOfMidlewaresPath = \Config::get('middleware_list_path');

        if (!file_exists($listOfMidlewaresPath)) {
            throw new Exception('Listing file for middlewares don\'t exist at ' . $listOfMidlewaresPath, 500);
        }
        $listOfMidlewares = include $listOfMidlewaresPath;

        if (!key_exists($middlewareName, $listOfMidlewares)) {
            throw new Exception('Middleware ' . $middlewareName . ' don\'t exist at ' . $listOfMidlewaresPath, 500);
        }

        $listOfMidlewares[$middlewareName]($request);
    }
}
