<?php

namespace Framework\Architecture\Web\Component;

use Exception;

class Session
{
    public function session_start()
    {
        $status = \session_status();
        if ($status == PHP_SESSION_DISABLED || $status == PHP_SESSION_NONE) {
            \session_start();
        } else {
            throw new Exception('La session est déjà démarrée', 500);
        }
    }

    public function session_stop()
    {
        $status = \session_status();
        if ($status == PHP_SESSION_ACTIVE) {
            \session_destroy();
        } else {
            throw new Exception('La session n\'est pas démarrée', 500);
        }
    }

    /**
     * Ajoute une information en session
     * @param $dataPath
     * @param $dataContent
     * @throws Exception
     */
    public function set($dataPath, $dataContent)
    {
        $status = \session_status();
        if ($status == PHP_SESSION_ACTIVE) {
            array_change($_SESSION, $dataPath, $dataContent);
        } else {
            throw new Exception('La session n\'est pas démarrée', 500);
        }
    }

    /**
     * Récupère une information en session
     *
     * @param $dataName
     * @return mixed|null
     * @throws Exception
     */
    public function get($dataPath)
    {
        $status = \session_status();
        if ($status == PHP_SESSION_ACTIVE) {
            return array_find($_SESSION, $dataPath);
        } else {
            throw new Exception('La session n\'est pas démarrée', 500);
        }
    }

    /**
     * Test if the path has value
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function has(string $dataPath)
    {
        $status = \session_status();
        if ($status == PHP_SESSION_ACTIVE) {
            return ((array_find($_SESSION, $dataPath) ?? false) ? true : false);
        } else {
            throw new Exception('La session n\'est pas démarrée', 500);
        }
    }

    /**
     * Supprime une clef en session
     *
     * @param $dataName
     * @throws Exception
     */
    public function delete($dataName): void
    {
        $status = \session_status();
        if ($status == PHP_SESSION_ACTIVE) {
            unset($_SESSION[$dataName]);
        } else {
            throw new Exception('La session n\'est pas démarrée', 500);
        }
    }
}
