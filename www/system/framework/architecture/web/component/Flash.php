<?php

namespace Framework\Architecture\Web\Component;

use Exception;
use Session;

class Flash
{
    const FLASH_SLUG = '_flash';

    public function assign(string $type, string $message)
    {
        if ($this->typeExist($type)) {
            Session::set(self::FLASH_SLUG . '.' . $type, $message);
        }
    }

    public function has(string $type)
    {
        if ($this->typeExist($type)) {
            if (!empty(\Session::get(self::FLASH_SLUG . '.' . $type))) {
                return true;
            }
            return false;
        }
    }

    public function get(string $type)
    {
        if ($this->typeExist($type)) {
            if ($this->has($type)) {
                $data = \Session::get(self::FLASH_SLUG . '.' . $type);
                unset($_SESSION[self::FLASH_SLUG][$type]);
                return $data;
            }
        }
    }

    private function typeExist(string $type)
    {
        $acceptedType = [
            'success',
            'error',
            'waitForProcess',
        ];

        if (!in_array($type, $acceptedType)) {
            throw new Exception('The ' . $type . ' flash type is not accepted');
        }
        return true;
    }
}
