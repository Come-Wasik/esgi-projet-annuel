<?php

namespace Framework\Architecture\Web\View;

use Exception;

class ViewManager
{
    public function show($viewName, array $variables = [])
    {
        # On importe les variables dans le scope global
        extract($variables);

        # On importe la vue
        $viewName = str_replace('.php', '', $viewName);
        $viewPath = view_path() . '/' . $viewName . '.php';
        if (!file_exists($viewPath)) {
            throw new Exception('View at path ' . $viewPath . ' not exists', 404);
        }
        \ob_start();
        include $viewPath;
        $viewContent = ob_get_clean();
        ## On remplit le flux par au minimum le caractère de fin de chaîne (\0) ##
        return (empty($viewContent) ? "\0" : $viewContent);
    }
}
