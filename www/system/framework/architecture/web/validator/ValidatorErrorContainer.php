<?php

namespace Framework\Architecture\Web\Validator;

use stdClass;

class ValidatorErrorContainer
{
    private $errorBackpack = [];

    /**
     * Affectation
     */
    public function addError(string $field, string $path, array $dataToReplace = [])
    {
        $validatorDataContainer = \Config::get('validator');
        $message = $this->array_find($validatorDataContainer, 'messages.' . $path);
        if ($dataToReplace) {
            foreach ($dataToReplace as $keyword => $value) {
                $message = str_replace('{' . $keyword . '}', $value, $message);
            }
        }
        $this->errorBackpack[$field][] = $message;
    }

    public function array_find(array $dataContainer, string $path, string $delimiter = '.')
    {
        $decomposedPath = explode($delimiter, $path);
        $key = array_shift($decomposedPath);
        $newPath = implode($delimiter, $decomposedPath);
        $newDimension = $dataContainer[$key] ?? null;

        if ($newDimension === null) {
            return null;
        }

        if (count($decomposedPath) >= 1) {
            return array_find($newDimension, $newPath);
        } else {
            return $newDimension;
        }
    }

    /**
     * Get or test data
     */
    public function any(): bool
    {
        if (!empty($this->errorBackpack)) {
            return true;
        }
        return false;
    }

    public function get(string $name)
    {
        return $this->errorBackpack[$name] ?? null;
    }

    public function all()
    {
        return $this->errorBackpack;
    }

    public function has(string $name)
    {
        if (key_exists($name, $this->errorBackpack)) {
            return true;
        }
        return false;
    }

    public function first()
    {
        $key = array_key_first($this->errorBackpack);
        return (object) [
            'title' => $key,
            'reason' => $this->errorBackpack[$key][0]
        ];
    }

    public function last()
    {
        $key = array_key_last($this->errorBackpack);
        return (object) [
            'title' => $key,
            'reason' => $this->errorBackpack[$key][0]
        ];
    }
}
