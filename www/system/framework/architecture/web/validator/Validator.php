<?php

namespace Framework\Architecture\Web\Validator;

use Exception;

class Validator
{
    public function check(array $requestData, $meanToValidate)
    {
        # Verify that validator is either an array or a string
        if (!is_string($meanToValidate) && !is_array($meanToValidate)) {
            throw new Exception('Validator is neither a string or an array', 500);
        }

        ### Init the error container
        $errorContainer = new ValidatorErrorContainer();

        ### Getting the validator tree
        if (is_string($meanToValidate)) {
            # Nothing for the moment, you must read a validator file
            $validatorFilePath = validator_path() . '/' . $meanToValidate . '.php';

            if (!file_exists($validatorFilePath)) {
                throw new Exception('The validator file "' . $meanToValidate . '" doesn\'t exist', 500);
            }
            $formToValidate = include $validatorFilePath;
        } else {
            # Read the array of data
            $formToValidate = $meanToValidate;
        }

        if (empty($formToValidate)) {
            throw new Exception('The form which need validation is void', 500);
        }

        if (!is_array($formToValidate)) {
            throw new Exception('The form which need validation is not an array', 500);
        }

        ### Testing all validation as possible
        foreach ($formToValidate as $field => $fDataContainer) {
            ## On vérifie que les données du champ n'est pas vide
            if (!empty($fDataContainer)) {
                if (is_string($fDataContainer)) {
                    $fDataContainer = explode('|', $fDataContainer);
                }

                foreach ($fDataContainer as $fCondition) {
                    ## Required
                    if ($fCondition == 'required') {
                        if (
                            !key_exists($field, $requestData)
                            || (
                                empty($requestData[$field])
                                || (
                                    (($type = $this->detectType($requestData[$field])) !== 'string')
                                    && $requestData[$field] == 0
                                )
                            )
                        ) {
                            $errorContainer->addError($field, 'required', ['field' => $field]);
                        }
                    }
                    if (
                        key_exists($field, $requestData)
                        && (
                            !empty($requestData[$field])
                            || (
                                (($type = $this->detectType($requestData[$field])) !== 'string')
                                && $requestData[$field] == 0
                            )
                        )
                    ) {
                        ## Min
                        if (strpos($fCondition, 'min:') === 0) {
                            if (substr_count($fCondition, ':') !== 1) {
                                throw new Exception('Formulation error in validator for ' . $field . ': there is more than one colon in min condition', 500);
                            }

                            $len = (float) substr(strstr($fCondition, ':'), 1);
                            $type = null;

                            ## There is a type element for this field ?
                            # Manuel type detection
                            foreach ($fDataContainer as $weSearchType) {
                                if (strpos($weSearchType, 'type:') === 0) {
                                    if (substr_count($weSearchType, ':') !== 1) {
                                        throw new Exception('Formulation error in validator for ' . $field . ': there is more than one colon in max condition', 500);
                                    }
                                    $type = (string) substr(strstr($weSearchType, ':'), 1);
                                }
                            }

                            if ($type === null) {
                                ## It is a number or a string ?
                                # Automatic type detection
                                $type = $this->detectType($requestData[$field]);
                            }

                            switch ($type) {
                                case 'int':
                                case 'float':
                                    if ((float)$requestData[$field] < $len) {
                                        $errorContainer->addError($field, 'numberCount.min', ['field' => $field, 'min' => $len]);
                                    }
                                    break;
                                case 'string':
                                default:
                                    if (strlen($requestData[$field]) < $len) {
                                        $errorContainer->addError($field, 'characterCount.min', ['field' => $field, 'min' => $len]);
                                    }
                                    break;
                            }

                            ## Max
                        } elseif (strpos($fCondition, 'max:') === 0) {
                            if (substr_count($fCondition, ':') !== 1) {
                                throw new Exception('Formulation error in validator for ' . $field . ': there is more than one colon in max condition', 500);
                            }

                            $len = (float) substr(strstr($fCondition, ':'), 1);
                            $type = null;

                            ## There is a type element for this field ?
                            # Manuel type detection
                            foreach ($fDataContainer as $weSearchType) {
                                if (strpos($weSearchType, 'type:') === 0) {
                                    if (substr_count($weSearchType, ':') !== 1) {
                                        throw new Exception('Formulation error in validator for ' . $field . ': there is more than one colon in max condition', 500);
                                    }
                                    $type = (string) substr(strstr($weSearchType, ':'), 1);
                                }
                            }

                            if ($type === null) {
                                ## It is a number or a string ?
                                # Automatic type detection
                                $type = $this->detectType($requestData[$field]);
                            }

                            switch ($type) {
                                case 'int':
                                case 'float':
                                    if ((float)$requestData[$field] > $len) {
                                        $errorContainer->addError($field, 'numberCount.max', ['field' => $field, 'max' => $len]);
                                    }
                                    break;
                                case 'string':
                                default:
                                    if (strlen($requestData[$field]) > $len) {
                                        $errorContainer->addError($field, 'characterCount.max', ['field' => $field, 'max' => $len]);
                                    }
                                    break;
                            }
                            ## Type
                        } elseif (strpos($fCondition, 'type:') === 0) {
                            if (substr_count($fCondition, ':') !== 1) {
                                throw new Exception('Formulation error in validator for ' . $field . ': there is more than one colon in type condition', 500);
                            }

                            $type = (string) substr(strstr($fCondition, ':'), 1);

                            if (
                                (
                                    $type === 'string'
                                    && gettype($requestData[$field]) !== 'string'
                                ) || (
                                    ($type === 'int' || $type === 'integer')
                                    && !is_int(filter_var($requestData[$field], FILTER_VALIDATE_INT))
                                ) || (
                                    $type === 'float'
                                    && !is_float(filter_var($requestData[$field], FILTER_VALIDATE_FLOAT))
                                ) || (
                                    $type === 'array'
                                    && gettype($requestData[$field]) !== 'array'
                                )
                            ) {
                                $errorContainer->addError($field, 'type', ['field' => $field, 'type' => $type]);
                            }
                            # Format
                        } elseif (strpos($fCondition, 'format:') === 0) {
                            if (substr_count($fCondition, ':') !== 1) {
                                throw new Exception('Formulation error in validator for ' . $field . ': there is more than one colon in type condition', 500);
                            }

                            $format = (string) substr(strstr($fCondition, ':'), 1);
                            switch ($format) {
                            case 'email':
                                if (filter_var($requestData[$field], FILTER_VALIDATE_EMAIL) === false) {
                                    $errorContainer->addError($field, 'format.email', ['field' => $field]);
                                }
                                break;
                            case 'date':
                                # La date doit être sous le format : 2019-05-25
                                $date = explode('-', $requestData[$field]);
                                if (
                                    count($date) !== 3
                                    || checkdate($date[1], $date[2], $date[0]) === false
                                ) {
                                    $errorContainer->addError($field, 'format.date', ['field' => $field]);
                                }
                                break;
                            default:
                                throw new Exception('Format ' . $format . ' invalide in validator for ' . $field . ' field', 500);
                            }
                        }
                    }
                }
            }
        }

        return $errorContainer;
    }

    public function detectType($variable)
    {
        $type = '';
        if (is_int(filter_var($variable, FILTER_VALIDATE_INT))) {
            $type = 'int';
        } elseif (is_float(filter_var($variable, FILTER_VALIDATE_FLOAT))) {
            $type = 'float';
        } else {
            $type = 'string';
        }
        return $type;
    }
}
