<?php

/**
 * @method addCommand => Add a route ins the RouteBackpack
 * @method getCommandList => Get all routes
 */

namespace Framework\Architecture\Console\Router;

use Framework\Architecture\Console\CoreMaker\Stream\Command as Request;
use Framework\Architecture\Console\Router\Route;
use Framework\Architecture\Console\Component\Backpack\Route as RouteBp;
use Exception;

class RouteManager
{
    static public function addCommand(Route $command)
    {
        RouteBp::append($command);
    }

    static public function getCommandList()
    {
        return RouteBp::getAll();
    }

    public function searchRoute(Request $request): Route
    {
        $inputArgList = explode(' ', $request->getCommand());
        $inputArgCount = $request->getSize();

        ## La requête ne comporte aucune demande
        if($inputArgCount == 0) {
            throw new Exception('No argument received', 404);
        }

        ## Aucune route n'a été défini
        if(empty(self::getCommandList())) {
            throw new Exception('No command found', 404);
        }

        foreach(self::getCommandList() as $command) {
            ### Début de l'analyse ###
            $commandArgList = explode(' ', $command->getRegex());
            $commandArgCount = $command->getSize();

            ## On teste l'égalité : tout mais obligatoire (+), tout mais optionnel (*) ##
            $corresponds = true;

            for($i=0; $i < $commandArgCount && $corresponds == true; $i++) {
                if(
                    /** 
                     * L'objectif est de tester chaque commande en gagnant un maximum de temps si la commande en cours d'analyse ne correspond pas
                     * 
                     * Voici un exemple de commande :
                     * 1 : make:controller My
                     * 2 : make:controller +
                     * 
                     * Voici l'équation qui dit que la commande en cours correspond à une des routes :
                     * True if => 
                     * True if =>  1.a == isset && 1.a == 2.a || (2.a == '+' && 1.a != empty) || 2.b == *
                     * False if => 2.a == !isset || 1.a != 2.a  && (2.a != '+' || 1.a == empty) && 2.b != *      -- Technique utilisé ci-dessous
                     * 
                     */
                    (
                        !isset($inputArgList[$i])
                        || $inputArgList[$i] != $commandArgList[$i]
                    )
                    && (
                        $commandArgList[$i] != '+' || empty($inputArgList[$i])
                    )
                    && $commandArgList[$i] != '*'
                ) {
                    $corresponds = false;
                }
            }

            if($corresponds) {
                $routeArgs = [];
                for($i = 0; $i < $inputArgCount; $i++) {
                    if(
                        $commandArgList[$i] == '+'
                        || $commandArgList[$i] == '*'
                    ) {
                        $command->addArgument($inputArgList[$i]);
                    }
                }
                return $command;
            }
        }
        throw new Exception('Command not found', 404);
    }
}
