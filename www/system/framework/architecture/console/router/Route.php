<?php

/**
 * This object is a route for a console
 * 
 * @attribute regex => Regex which can be matched by a requested command
 * @attribute action => Action to execute if the route match with a request command
 * @attribute arguments => Arguments of the route (filled if the route match bu a requested command)
 */

namespace Framework\Architecture\Console\Router;

use Exception;

class Route
{
	private $regex;
	private $action;
	private $arguments;
    
    public function __construct(array $data = null)
    {
		### Initialisation ###
		$this->arguments = [];

		### Hydratation ###
		if($data) {
			$this->hydrate($data);
		}
    }

    private function hydrate(array $data): void
    {
        foreach($data as $attName => $attValue) {
            $settername = 'set'. ucfirst($attName);
            $this->$settername($attValue);
        }
	}
	
	/**
	 * Setters and getters
	 */
	public function getRegex(): ?string
	{
		return $this->regex;
	}
	public function getAction()
	{
		return $this->action;
	}
	public function getArguments(): ?array
	{
		return $this->arguments;
	}

	public function setRegex(string $regex): void
	{
		$this->regex = $regex;
	}
	public function setAction($action): void
	{
		$this->action = $action;
	}
	public function setArguments(array $arguments): void
	{
		foreach($arguments as $arg) {
			if(!is_string($arg)) {
				throw new Exception('Each arguments of the command need to be a string. '.$arg.' is not a string.', 500);
			}
		}
		$this->arguments = $arguments;
	}

	/**
	 * Appendix setters
	 */
	public function addArgument(string $arguments): void
	{
		$this->arguments[] = $arguments;
	}

	/**
	 * Appendix methods
	 */
	public function getSize(): int
	{
		return (empty($this->regex) ? 0 : 1 + substr_count($this->regex, ' '));
	}
}
