<?php

namespace Framework\Architecture\Console\CoreMaker;

use Framework\Architecture\Console\Router\RouteManager;
use Framework\Architecture\Console\CoreMaker\Stream\Command as Request;
use Exception;

class ConsoleMaker
{
    public function __construct()
    {
        $this->getRouteFiles();
    }


    public function getRouteFiles()
    {
        ### Récupération de la liste des fichiers de route par architecture (web, console) ###
        $webRouteFilenames = \Config::get('router')['filesFor']['console'];
        $webRoutesPath = array_map(function($filename) {
            if(!is_string($filename)) {
                throw new Exception('One route filename in the data storage for router isn\'t a string');
            }
            return \Config::get('route_path').'/'.$filename.'.php';
        }, $webRouteFilenames);

        ## Récupération de toutes les routes
        foreach($webRoutesPath as $routeFile) {
            if(!file_exists($routeFile)) {
                throw new Exception('The filename : '.$routeFile.' doens\'t exist', 500);
            }
            include $routeFile;
        }
    }

    public function launchCommand(Request $request)
    {
        # On recherche une condordance entre la route demandé et celles contenus dans le backpack
        $manager = new RouteManager();
        $command = $manager->searchRoute($request);

        $action = $command->getAction();

        if(!is_callable($action) && !is_string($action)) {
            throw new Exception('The action from the route '.$command->getRegex().' cannot be process.', 500);
        }

        ### Execution de l'action ###
        if(is_callable($action)) {
            $action(...$command->getArguments());
            
        } else if (is_string($action)) {
            # L'action ici demande un contrôleur et sa méthode
            if(
                empty($action)
                || \substr_count($action, '@') != 1
                || \strpos($action, '@', 1) === false
                || \strlen($action) < 3
            ) {
                throw new Exception('Route action '.$action.' cannot be read', 500);
            }

            # Récupération du controller et de la méthode
            $actionContent = explode('@', $action);

            if(
                count($actionContent) != 2
                || empty($actionContent[1])
            ) {
                throw new Exception('Action is not correct. please use notation \'myController@myMethod\'', 500);
            }

            ### Récupération du contrôleur et de sa méthode à lancer ###
            $controllerFullname = $actionContent[0];

            # Récupération de la méthode du controller à lancer
            $method = $actionContent[1];

            # Lancement du controller
            if(!class_exists($controllerFullname)) {
                throw new Exception('The controller '.$controllerFullname.' not exists', 500);
            }

            if(!method_exists($controllerFullname, $method)) {
                throw new Exception('The method '.$method.' not exists', 500);
            }

            # Lancement du contrôleur à exécuter
            (new $controllerFullname())->$method(...$command->getArguments());
        }
    }
}
