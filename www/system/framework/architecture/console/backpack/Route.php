<?php

namespace Framework\Architecture\Console\Component\Backpack;

use Framework\Architecture\All\Component\Interfaces\IndicedBackpackInterface;

class Route implements IndicedBackpackInterface
{
    private static $backpack = [];

    public static function append($content)
    {
        self::$backpack[] = $content;
    }

    public static function get(int $indice)
    {
        return self::$backpack[$indice] ?? null;
    }

    public static function getAll()
    {
        return self::$backpack;
    }
}
