<?php

namespace Framework\Architecture\All\Component\Interfaces;

interface IndicedBackpackInterface
{
    public static function append($content);

    public static function get(int $indice);

    public static function getAll();
}
