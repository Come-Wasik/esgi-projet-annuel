<?php

namespace Framework\Architecture\All\Component\Interfaces;

interface NamedBackpackInterface
{
    public static function set(string $name, $content);

    public static function get(string $name);

    public static function all();
}
