<?php

namespace Framework\Architecture\All\Component\Backpack;

use Framework\Architecture\All\Component\Interfaces\NamedBackpackInterface;

class Config implements NamedBackpackInterface
{
    private static $backpack = [];

    public static function set(string $path, $content, $delimiter = '.')
    {
        $newBackpack = self::$backpack;
        array_change($newBackpack, $path, $content, $delimiter);
        self::$backpack = $newBackpack;
    }

    public static function get(string $path, $delimiter = '.')
    {
        $backpack = self::$backpack;
        return array_find($backpack, $path, $delimiter);
    }

    public static function all()
    {
        return self::$backpack;
    }
}
