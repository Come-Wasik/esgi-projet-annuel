<?php

namespace Framework\Architecture\All\Component\Backpack;

use Framework\Architecture\All\Component\Interfaces\NamedBackpackInterface;

class Package implements NamedBackpackInterface
{
    private static $backpack = [];

    public static function set(string $name, $content)
    {
        self::$backpack[$name] = $content;
    }

    public static function get(string $name)
    {
        return self::$backpack[$name] ?? null;
    }

    public static function all()
    {
        return self::$backpack;
    }

    public static function has(string $name)
    {
        if (key_exists($name, self::$backpack)) {
            return true;
        }
        return false;
    }
}
