<?php

namespace Framework\Architecture\All\Component\Database;

use Framework\Architecture\All\Component\Database\Ressource\HelperMethods;
use Framework\Architecture\All\Component\Database\Ressource\ErrorMethod;
use PDO;
use Exception;

class  DatabaseManager
{
    /**
     * Set traits :
     *      - Helpers methods for Database Manager
     *      - Error methods for Database Manager
     */
    use HelperMethods;
    use ErrorMethod;

    /**
     * @attribute $hdb = connexion PDO vers une base de données
     */
    private $dbh;

    /**
     * @function constructor = Créer une connexion à une base de données utilisable
     */
    public function __construct(
        string $dbDriver,
        string $dbAddress,
        string $dbName,
        string $dbUser,
        string $dbPass,
        string $port = ''
    ) {
        $this->dbh = new PDO($dbDriver . ':dbname=' . $dbName . ';host=' . $dbAddress, $dbUser, $dbPass);
    }

    /**
     * @function request = Permet d'effectuer une requête PDO vers la base de données
     */
    public function request(string $request, array $pdoArgs = [], string $fetchStyle = 'assoc', array $fetchOptions = [])
    {
        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute($pdoArgs)) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }

        # Initialisation d'un potentiel argument à envoyer à fetch. Peut changer selon sa méthode de filtre.
        $fetchArg = [];

        # Filtrage de la méthode fetch à employer
        switch ($fetchStyle) {
            case 'unique':
            case PDO::FETCH_UNIQUE:
                $fetchStyle = PDO::FETCH_UNIQUE;
                break;

            case 'group':
            case PDO::FETCH_GROUP:
                $fetchStyle = PDO::FETCH_GROUP;
                break;

            case 'both':
            case PDO::FETCH_BOTH:
                $fetchStyle = PDO::FETCH_BOTH;
                break;

            case 'column':
            case PDO::FETCH_COLUMN:
                /** @example :
                 *  $column = $manager->request('SELECT * FROM ' . DB_PREFIX . 'Epee', [], 'column', [
                 *      'column' => 3
                 *  ]);
                 */
                $fetchStyle = PDO::FETCH_COLUMN;
                $fetchArg[] = $fetchOptions['column'];
                break;

            case 'func':
            case PDO::FETCH_FUNC:
                /** @example :
                 *  $columns = $manager->request('SELECT name, power FROM ' . DB_PREFIX . 'Epee', [], 'func', ['function' => function($name, $power) {
                 *      return 'Nous avons une '.$name.' avec '.$power.' de puissance !';
                 *  }]);
                 */
                $fetchStyle = PDO::FETCH_FUNC;
                $fetchArg[] = $fetchOptions['function'];
                break;

            case 'class':
            case PDO::FETCH_CLASS:
                /** @example :
                 *  $object = $manager->request('SELECT * FROM ' . DB_PREFIX . 'Epee', [], 'class', [
                 *      'classname' => \App\Entity\Epee::class
                 *  ]);
                 */

                $fetchStyle = PDO::FETCH_CLASS;
                $fetchArg[] = $fetchOptions['classname'];

                $fetchArg[] = ($fetchOptions['constructorArgs'] ?? null);
                break;

            case 'assoc':
            case PDO::FETCH_ASSOC:
            default:
                $fetchStyle = PDO::FETCH_ASSOC;
                break;
        }

        return $pdoSt->fetchAll($fetchStyle, ...$fetchArg);
    }

    /**
     * @function getAll = Permet d'obtenir sous forme d'objet toutes les lignes d'un tableau
     */
    public function getAll(string $entityName)
    {
        if (!class_exists($entityName)) {
            throw new Exception('Class ' . $entityName . ' doesn\'t exist', 500);
        }
        # On récupère le nom simple de la classe (sans le namespace)
        $tableName = $this->getOnlyClassname($entityName);

        $request = 'SELECT * FROM ' . DB_PREFIX . $tableName . ' ORDER BY id';

        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute([])) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }

        return $pdoSt->fetchAll(PDO::FETCH_CLASS, $entityName);
    }

    /**
     * @function get = Permet d'obtenir sous forme d'objet une ligne d'une table en lui donnant son id
     */
    public function get(string $entityName, int $id)
    {
        if (!class_exists($entityName)) {
            throw new Exception('Class ' . $entityName . ' doesn\'t exist', 500);
        }
        # On récupère le nom simple de la classe (sans le namespace)
        $tableName = $this->getOnlyClassname($entityName);

        $request = 'SELECT * FROM ' . DB_PREFIX . $tableName . ' WHERE id = :id';

        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute(['id' => $id])) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }
        /**
         * Si array YES
         * Si null Nop
         */

        $response = $pdoSt->fetchAll(PDO::FETCH_CLASS, $entityName);
        if (is_array($response) && !empty($response)) {
            return $response[array_key_first($response)];
        }
        return $response;
    }

    /**
     * @function add = ajoute une entité dans la base de données
     */
    public function add(object $entity)
    {
        # On récupère le nom de la classe pour l'entité passé en argument
        $tableName = $this->getOnlyClassname(\get_class($entity));

        # On récupère les attributs et leur valeur contenus dans l'entité
        $varFromEntity = $this->getObjectAttr($entity);

        # On ne veut pas ajouter l'id dans la requête donc on fait le tri
        $columnData = $varFromEntity;
        unset($columnData['id']);
        $columnNames = array_keys($columnData);

        $request = 'INSERT INTO ' . DB_PREFIX . $tableName . ' (' . implode(',', $columnNames) . ') VALUES (:' . implode(', :', $columnNames) . ')';

        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute($columnData)) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }

        return 'Demande d\'ajout réussi avec succès. ' . $pdoSt->rowCount() . ' lignes ont été ajoutés.';
    }

    /**
     * @function update = Met à jour une entité dans la base de données
     */
    public function update(object $entity)
    {
        # On récupère le nom de la classe pour l'entité passé en argument
        $tableName = lcfirst($this->getOnlyClassname(\get_class($entity)));

        # On récupère les attributs et leur valeur contenus dans l'entité
        $varFromEntity = $this->getObjectAttr($entity);

        # On ne veut pas ajouter l'id dans la liste des données à mettre à jour donc on fait le tri
        $columnData = $varFromEntity;
        unset($columnData['id']);
        $columnNames = array_keys($columnData);

        $request = 'UPDATE ' . DB_PREFIX . $tableName . ' SET '
            . implode(',', array_map(function ($attrName) {
                return $attrName . ' = :' . $attrName;
            }, $columnNames))
            . ' WHERE id = :id';

        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute($varFromEntity)) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }

        return 'Demande de mise à jour réussi avec succès. ' . $pdoSt->rowCount() . ' lignes ont été ajoutés.';
    }

    /**
     * @function save = Fusion de add et update
     */
    public function save(object $entity)
    {
        # Add
        if ($entity->getId() == null) {
            return $this->add($entity);
            # update
        } else {
            return $this->update($entity);
        }
    }

    /**
     * @function update = Supprime une entité dans la base de données
     *
     * @example = delete(object $myObject)
     * @example = delete(string $className, int $id)
     */
    public function delete($entity, int $id = null)
    {
        # Nous sommes face à un nom de classe
        if (is_string($entity)) {
            $entityName = $entity;

            if (!class_exists($entityName)) {
                throw new Exception('Class ' . $entityName . ' doesn\'t exist', 500);
            }

            # Obtention du nom de la table
            $tableName = $this->getOnlyClassname($entityName);

            $pdoArgs = [
                'id' => $id
            ];

            # Nous sommes face à une entité
        } elseif (is_object($entity)) {
            if ($id != null) {
                throw new Exception('Dans les faits, l\'argument qui sert à définir un id ne sert à rien. Donc autant ne pas en mettre.');
            }

            $tableName = $this->getOnlyClassname(\get_class($entity));

            $pdoArgs = [
                'id' => $this->getObjectAttr($entity)['id']
            ];
        } else {
            throw new Exception('La méthode' . __METHOD__ . ' n\'accepte en premier argument que des éléments de type string ou objet.');
        }

        # Définition de la requête
        $request = 'DELETE FROM ' . DB_PREFIX . $tableName . ' WHERE id = :id';

        # Execution de la requête
        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute($pdoArgs)) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }

        # Renvoi du résultat
        return 'Demande de suppression réussi avec succès. ' . $pdoSt->rowCount() . ' lignes ont été supprimés.';
    }
}
