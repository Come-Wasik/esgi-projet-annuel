<?php

namespace Framework\Architecture\All\Component\Database\Ressource;

use Exception;

trait ErrorMethod
{
    /**
     * Errors methods for Database Manager
     */
    private function throwPdoError(string $request, array $errors)
    {
        $errors[0] = 'SQL error code : '.$errors[0];
        $errors[1] = 'Driver Error code : '.$errors[1];
        $errors[2] = 'Error message : '.$errors[2];
        throw new Exception('La requête '.$request.' n\'a pas pu aboutir. Raison : '.implode('; ', $errors), 500);
    }
}
