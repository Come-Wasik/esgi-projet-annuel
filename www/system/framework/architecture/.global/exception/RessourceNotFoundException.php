<?php

namespace Framework\Architecture\All\Component\Exception;

use Exception;
use Throwable;

class RessourceNotFoundException extends Exception
{
    public function __construct(string $message = '', int $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
