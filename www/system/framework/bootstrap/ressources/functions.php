<?php

/**
 * Allow to debug variable
 */
if(!function_exists('vprint')) {
    function vprint($data)
    {
        if(ENV == 'dev') {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
        }
    }
}

if(!function_exists('vdump')) {
    function vdump($data)
    {
        if(ENV == 'dev') {
            echo '<pre>';
            var_dump($data);
            echo '</pre>';
        }
    }
}

if(!function_exists('prd')) {
    function prd($data)
    {
        vprint($data);
        die();
    }
}

if(!function_exists('dd')) {
    function dd($data)
    {
        vdump($data);
        die();
    }
}

/**
 * Get root project path
 */
if(!function_exists('base_path')) {
    function base_path()
    {
        return (!empty($_SERVER['DOCUMENT_ROOT']) ? dirname($_SERVER['DOCUMENT_ROOT']) : dirname(dirname(dirname(dirname(__DIR__)))));
    }
}

    /**
      * Get a data in array with path
      *
      * @param array $dataContainer the array which contain the data that we search
      * @param string $path The path of the needed data in the array
      * @param string $delimiter Set the delimiter use in path
      * @return mixed
      **/
if (!function_exists('array_find')) {
    function array_find($dataContainer, string $path, string $delimiter = '.')
    {
        if($dataContainer === null) {
            $dataContainer = [];
        }

        if(!is_array($dataContainer)) {
            throw new Exception('The '.__FUNCTION__.' function has its first argument with not the array type', 500);
        }

        $decomposedPath = explode($delimiter, $path);
        $key = array_shift($decomposedPath);
        $newPath = implode($delimiter, $decomposedPath);
        $newDimension = $dataContainer[$key] ?? null;

        if($newDimension === null) {
            return null;
        }

        if (count($decomposedPath) >= 1) {
            return array_find($newDimension, $newPath);
        } else {
            return $newDimension;
        }
    }
}

    /**
      * Set a data in array with path
      *
      * @param array $dataContainer the array which contain the data that we would to change
      * @param string $path The path of the needed data in the array
      * @param mixed $newValue The value to affect at the endpath
      * @param string $delimiter Set the delimiter use in path
      * @return mixed
      **/
if (!function_exists('array_change')) {
    function array_change(&$dataContainer, string $path, $newValue, string $delimiter = '.')
    {
        # Solving the "null enter" problem #
        if($dataContainer === null) {
            $dataContainer = [];
        }

        $decomposedPath = explode($delimiter, $path);
        $key = array_shift($decomposedPath);
        $newPath = implode($delimiter, $decomposedPath);
        
        if (count($decomposedPath) >= 1) {
            # Solving the "key doesn't exist" problem #
            if(!key_exists($key, $dataContainer)) {
                $dataContainer[$key] = null;
            }

            # Solving the "data exist" problem #
            if(!is_array($dataContainer[$key])) {
                $dataContainer[$key] = [];
            }

            array_change($dataContainer[$key], $newPath, $newValue);
        } else {
            $dataContainer[$key] = $newValue;
        }
    }
}