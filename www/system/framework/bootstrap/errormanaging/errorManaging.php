<?php

/**
 * This file allow to catch any error and exception
 * ------------------------------------------------
 *
 * @function set_exception_handler => Catch an Error or an Exception
 */

### Set the error/exception handler ###
set_exception_handler(function (Throwable $exception) {
    function get_error_view(string $viewName, Throwable $error)
    {
        if (viewExists('error/' . $viewName)) {
            return view('error/' . $viewName, [
                'error' => $error
            ]);
        } else {
            ob_start();
            require __DIR__ . '/views/' . $viewName . '.php';
            return ob_get_clean();
        }
    }

    function generateResponse(string $viewName, Throwable $exception)
    {
        # Génération de la vue à afficher à l'utilisateur #
        $view = get_error_view($viewName, $exception);
        # Création d'une réponse à envoyer à l'utilisateur et envoi de la réponse #
        $response = response($view, $exception->getCode());
        $response->print();
    }

    ### On cherche à afficher correctement l'erreur ###
    switch (ARCH) {
        case 'web':
            if (defined('ENV')) {
                switch (ENV) {
                    case 'prod':
                        generateResponse('prod', $exception);
                        break;
                    case 'dev':
                    default:
                        generateResponse('dev', $exception);
                        break;
                }
            } else {
                generateResponse('dev', $exception);
            }
            break;
        case 'console':
        default:
            if (defined('ENV')) {
                switch (ENV) {
                    case 'prod':
                        echo 'An error has occured. Please contact an administrator.';
                        break;
                    case 'dev':
                    default:
                        echo 'An error ' . $exception->getCode() . ' has occured : ' . $exception->getMessage() . ' at line ' . $exception->getLine() . ' in file ' . $exception->getFile() . '.';
                        break;
                }
            } else {
                echo 'Ya une erreur';
            }
            break;
    }
});
