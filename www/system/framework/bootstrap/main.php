<?php

use Framework\Bootstrap\FileLoader\Config;
use Framework\Bootstrap\FileLoader\Helper;
use Framework\Bootstrap\FileLoader\Env;
use Framework\Architecture\Web\Component\Session;

/**
 * Bootstrap functions
 */
require __DIR__ . '/ressources/functions.php';

/**
 * Components loading...
 */

## Loading env data ##
$envFileLoader = new Env();
$envFileLoader->init();

## Loading config data ##
$configFileLoader = new Config();
$configFileLoader->init();

## Loading helpers ##
$helperFileLoader = new Helper();
$helperFileLoader->init();

$sessionManager = new Session();
$sessionManager->session_start();

## Register error managment ##
require __DIR__ . '/errormanaging/errorManaging.php';

/**
 * Php reconfiguration
 */
foreach (glob(__DIR__ . '/phpConfiguration/*.php') as $configFile) {
    include $configFile;
}
