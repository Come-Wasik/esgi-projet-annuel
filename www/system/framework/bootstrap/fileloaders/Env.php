<?php

/**
 * @File = Ce fichier a pour obtectif de charger les données d'environnement en constantes dans tout le programme
 */

namespace Framework\Bootstrap\FileLoader;

class Env implements FileLoaderInterface
{
    private $tempEnvData = [];

    public function init(): void
    {
        # Enregistrement des constantes d'environnement
        # D'abord avec le fichier .env, puis avec le fichier .env.VOTRE_ENVIRONNEMENT
        $envDirPath = base_path() . '/.env';
        $this->readEnvFile($envDirPath);

        if (isset($this->tempEnvData['ENV'])) {
            $environment = $this->tempEnvData['ENV'];
            switch ($environment) {
                case 'dev':
                    $envDirPath = base_path() . '/.env.dev';
                    $this->readEnvFile($envDirPath);
                    break;
                case 'prod':
                    $envDirPath = base_path() . '/.env.prod';
                    $this->readEnvFile($envDirPath);
                    break;
            }
        }

        $envDirPath = base_path() . '/.local.env';
        if (file_exists($envDirPath)) {
            $this->readEnvFile($envDirPath);
        }

        # Affectation des données temporaire d'environnement en tant que constantes
        foreach ($this->tempEnvData as $dataName => $dataContent) {
            if (!\defined($dataName)) {
                \define($dataName, $dataContent);
            }
        }
    }

    private function readEnvFile($envDirPath): void
    {
        if (file_exists($envDirPath)) {
            $stream = file_get_contents($envDirPath);
            $lines = explode("\n", $stream);
            // On parcourt chaque ligne du fichier
            foreach ($lines as $line) {
                if (
                    !empty($line)
                    && \substr_count($line, '=') == 1
                    && \strpos($line, '=', 1) !== false
                    && strlen($line) >= 2
                ) {
                    # Ici, cela veut dire que la donnée est valide à enregistrement
                    $data = explode('=', $line);
                    # On enregistre temporairement la donnée d'environnement
                    $this->tempEnvData[trim($data[0])] = trim($data[1]);
                }
            }
        }
    }
}
