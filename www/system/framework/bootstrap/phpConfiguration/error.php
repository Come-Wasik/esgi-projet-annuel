<?php

### Set the report level ###
if(defined('ENV')) {
    switch (ENV) {
        case 'prod':
            error_reporting(\Config::get('errorManaging')['errorVisibility']['prod'] ?? E_ERROR | E_PARSE);
            break;
        case 'dev':
        default:
            error_reporting(\Config::get('errorManaging')['errorVisibility']['dev'] ?? E_ALL);
            break;
    }
}