<?php

$default_timezone = \Config::get('app')['default_timezone'] ?? 'Europe/London';
date_default_timezone_set($default_timezone);