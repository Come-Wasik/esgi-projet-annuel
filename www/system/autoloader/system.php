<?php

spl_autoload_register(function ($className) {
    ### Define the system directory path ###
    ## Including makerIni ##
    $makeIniPath = dirname(dirname(__DIR__)) . '/maker.ini';
    if (!file_exists($makeIniPath)) {
        die('Fatal error : makerIni file should exists');
    }
    $makerData = parse_ini_file($makeIniPath);

    if (!isset($makerData['system_path'])) {
        die('Fatal error : makerIni file should comport a system_path data');
    }

    ## Define system directory ##
    $systemPath = dirname(dirname(__DIR__)) . '/' . $makerData['system_path'];

    $autoloadContent = [
        # Global
        'Framework\Bootstrap\FileLoader' => $systemPath . '/framework/bootstrap/fileloaders/',
        'Framework\Architecture\All\Component\Backpack' => $systemPath . '/framework/architecture/.global/backpack/',
        'Framework\Architecture\All\Component\Interfaces' => $systemPath . '/framework/architecture/.global/interfaces/',
        'Framework\Architecture\All\Component\Dependency' => $systemPath . '/framework/architecture/.global/dependency/',
        'Framework\Architecture\All\Component\Database' => $systemPath . '/framework/architecture/.global/model/',
        'Framework\Architecture\All\Component\Database\Ressource' => $systemPath . '/framework/architecture/.global/model/ressources/',
        'Framework\Architecture\All\Component\Exception' => $systemPath . '/framework/architecture/.global/exception/',
        # Console
        'Framework\Architecture\Console\Router' => $systemPath . '/framework/architecture/console/router/',
        'Framework\Architecture\Console\CoreMaker' => $systemPath . '/framework/architecture/console/core/',
        'Framework\Architecture\Console\CoreMaker\Stream' => $systemPath . '/framework/architecture/console/core/stream/',
        'Framework\Architecture\Console\Component\Backpack' => $systemPath . '/framework/architecture/console/backpack/',
        'Framework\Architecture\Console\Component' => $systemPath . '/framework/architecture/console/backpack/',
        # Web
        'Framework\Architecture\Web\View' => $systemPath . '/framework/architecture/web/view/',
        'Framework\Architecture\Web\Routage' => $systemPath . '/framework/architecture/web/router/',
        'Framework\Architecture\Web\CoreMaker' => $systemPath . '/framework/architecture/web/core/',
        'Framework\Architecture\Web\CoreMaker\Component\Http' => $systemPath . '/framework/architecture/web/core/http/',
        'Framework\Architecture\Web\Component' => $systemPath . '/framework/architecture/web/component/',
        'Framework\Architecture\Web\Component\Backpack' => $systemPath . '/framework/architecture/web/backpack/',
        'Framework\Architecture\Web\Security' => $systemPath . '/framework/architecture/web/security/',
        'Framework\Architecture\Web\Validator' => $systemPath . '/framework/architecture/web/validator/'
    ];

    ### Extract namespace and classe name ###
    $namespaceParts = explode('\\', $className);
    $justClassName = array_pop($namespaceParts);
    $justNamespace = implode('\\', $namespaceParts);

    if (key_exists($justNamespace, $autoloadContent)) {
        $classPath = $autoloadContent[$justNamespace] . $justClassName . '.php';
        include $classPath;
    }
});
