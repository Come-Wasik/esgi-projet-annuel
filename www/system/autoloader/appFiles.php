<?php

spl_autoload_register(function($className) {
    ### Extract namespace and classe name ###
    $namespaceParts = explode('\\', $className);
    $justClassName = array_pop($namespaceParts);
    $justNamespace = implode('\\', $namespaceParts);

    if(!empty($justNamespace)) {
        ## Has namespace ##
        if(
            count($namespaceParts) >= 1
            && $namespaceParts[0] == 'App'
        ) {
            # Cut the array namespace in an array
            if(count($namespaceParts) > 1) {
                # Downcase all directories in the array
                $namespaceParts = array_map(function($directory) {
                    return lcfirst($directory);
                }, $namespaceParts);

                # Transform the directories array to a string like a path
                $allDirectories = implode('/', array_slice($namespaceParts, 1)).'/';
            } else {
                $allDirectories = '';
            }

            include app_path().'/'.$allDirectories.$justClassName.'.php';
        }
    }
});
