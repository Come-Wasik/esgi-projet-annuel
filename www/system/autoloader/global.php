<?php

spl_autoload_register(function ($className) {
    ### Define the system directory path ###
    ## Including makerIni ##
    $makeIniPath = dirname(dirname(__DIR__)) . '/maker.ini';
    if (!file_exists($makeIniPath)) {
        die('Fatal error : makerIni file should exists');
    }
    $makerData = parse_ini_file($makeIniPath);

    if (!isset($makerData['system_path'])) {
        die('Fatal error : makerIni file should comport a system_path data');
    }

    ## Define system directory ##
    $systemPath = dirname(dirname(__DIR__)) . '/' . $makerData['system_path'];

    $autoloadContent = [
        # Logger
        'Packages\Logger' => $systemPath . '/logger/',

        # FormMaker
        'Packages\FormMaker\Builder' => $systemPath . '/formMaker/builder/',
        'Packages\FormMaker\Builder\Component\Field' => $systemPath . '/formMaker/builder/fields/',
        'Packages\FormMaker\Validator' => $systemPath . '/formMaker/validator/',
        'Packages\FormMaker\Validator\Exception' => $systemPath . '/formMaker/validator/exception/',

        # Php mailer
        'PHPMailer\PHPMailer' => $systemPath . '/phpmailer-6.1.6/src/',
    ];

    ### Extract namespace and classe name ###
    $namespaceParts = explode('\\', $className);
    $justClassName = array_pop($namespaceParts);
    $justNamespace = implode('\\', $namespaceParts);

    if (key_exists($justNamespace, $autoloadContent)) {
        $classPath = $autoloadContent[$justNamespace] . $justClassName . '.php';
        include $classPath;
    }
});
