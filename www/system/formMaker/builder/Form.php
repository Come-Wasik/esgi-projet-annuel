<?php

namespace Packages\FormMaker\Builder;

use Packages\FormMaker\Builder\Component\Field\{
	FormTag,
	Button
};
use Exception;

class Form
{
    /**
     * @attribute (name, class...) = form attribute
     * @attribute (fieldBackpack) = form field elements
     */
    private $name;
	private $class;
	private $id;
	private $theme = 'standard';
	private $method = 'POST';
	private $action = '';
	private $enctype = 'application/x-www-form-urlencoded';
	private $accept_charset = 'UTF-8';
	private $autocomplete = true;
	private $novalidate = false;
	private $target = '_self';
	private $auto_submit_button = false;
    
    private $fieldBackpack;

    /**
     * Creation of the object
     */
    public function __construct(array $formData = null, array $fields = null) {
		## Ajout d'un bouton de sousmission si l'action automatique a été activé ##
		if(key_exists('auto_submit_button', $formData) && $formData['auto_submit_button']) {
			$fields[] = new Button(['type' => 'submit']);
		}

		## Insertion de chaque champs dans le formulaire
		$this->fieldBackpack = [];
		if($fields) {
			foreach($fields as $field) {
				## On ajoute une instance de Field ##
				$this->addField($field);
			}
		}

        ## Hydratation des métadonnées du formulaire ##
        if($formData) {
            $this->hydrate($formData);
        }
    }

    private function hydrate(array $data): void
    {
        foreach($data as $attName => $attValue) {
            $settername = 'set'. ucfirst($attName);
            $this->$settername($attValue);
        }
	}
	
	/**
	 * Rendering form
	 */
	public function render()
	{
		$themePath = __DIR__.'/themes/'.$this->theme.'.php';
		ob_start();
		include $themePath;
		return ob_get_clean();
	}

    /**
     * Field backpack methods
     */
    public function addField(FormTag $field): void
    {
        $this->fieldBackpack[] = $field;
    }

    public function getFieldList(): array
    {
        return $this->fieldBackpack;
    }

    /**
     * Getters and Setters
     */
    public function getName(): ?string
	{
		return $this->name;
	}
	public function getClass(): ?string
	{
		return $this->class;
	}
	public function getId(): ?int
	{
		return $this->id;
	}
	public function getTheme(): ?string
	{
		return $this->theme;
	}
	public function getMethod(): ?string
	{
		return $this->method;
	}
	public function getAction(): ?string
	{
		return $this->action;
	}
	public function getEnctype(): ?string
	{
		return $this->enctype;
	}
	public function getAccept_charset(): ?string
	{
		return $this->accept_charset;
	}
	public function getAutocomplete(): ?bool
	{
		return $this->autocomplete;
	}
	public function getNovalidate(): ?bool
	{
		return $this->novalidate;
	}
	public function getTarget(): ?string
	{
		return $this->target;
	}
	public function getAuto_submit_button(): ?bool
	{
		return $this->auto_submit_button;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}
	public function setClass(string $class): void
	{
		$this->class = $class;
	}
	public function setId(int $id): void
	{
		$this->id = $id;
	}
	public function setTheme(string $theme): void
	{
		if(!file_exists(__DIR__.'/themes/'.$theme.'.php')) {
			throw new Exception('The theme '.$theme.' doesn\'t exists.');
		}
		$this->theme = $theme;
	}
	public function setMethod(string $method): void
	{
		$this->method = $method;
	}
	public function setAction(string $action): void
	{
		$this->action = $action;
	}
	public function setEnctype(string $enctype): void
	{
		$this->enctype = $enctype;
	}
	public function setAccept_charset(string $accept_charset): void
	{
		$this->accept_charset = $accept_charset;
	}
	public function setAutocomplete(bool $autocomplete): void
	{
		$this->autocomplete = $autocomplete;
	}
	public function setNovalidate(bool $novalidate): void
	{
		$this->novalidate = $novalidate;
	}
	public function setTarget(string $target): void
	{
		$this->target = $target;
	}
	public function setAuto_submit_button(bool $auto_submit_button): void
	{
		$this->auto_submit_button = $auto_submit_button;
	}
}
