<?php

namespace Packages\FormMaker\Builder\Component\Field;

class Input extends FormTag
{
	private $type = 'text';
	private $accept;
	private $alt;
	private $autocomplete;
	private $autofocus;
	private $checked;
	private $dirname;
	private $formaction;
	private $formenctype;
	private $formmethod;
	private $formnovalidate;
	private $formtarget;
	private $min;
	private $max;
	private $step;
	private $int;
	private $minlenght;
	private $maxlenght;
	private $multiple;
	private $pattern;
	private $placeholder;
	private $readonly;
	private $required;
	private $src;

	public function getType(): ?string
	{
		return $this->type;
	}
	public function getAccept(): ?string
	{
		return $this->accept;
	}
	public function getAlt(): ?string
	{
		return $this->alt;
	}
	public function getAutocomplete(): ?bool
	{
		return $this->autocomplete;
	}
	public function getAutofocus(): ?bool
	{
		return $this->autofocus;
	}
	public function getChecked(): ?bool
	{
		return $this->checked;
	}
	public function getDirname(): ?string
	{
		return $this->dirname;
	}
	public function getFormaction(): ?string
	{
		return $this->formaction;
	}
	public function getFormenctype(): ?string
	{
		return $this->formenctype;
	}
	public function getFormmethod(): ?string
	{
		return $this->formmethod;
	}
	public function getFormnovalidate(): ?string
	{
		return $this->formnovalidate;
	}
	public function getFormtarget(): ?string
	{
		return $this->formtarget;
	}
	public function getMin(): ?int
	{
		return $this->min;
	}
	public function getMax(): ?int
	{
		return $this->max;
	}
	public function getStep(): ?int
	{
		return $this->step;
	}
	public function getMinlenght(): ?int
	{
		return $this->minlenght;
	}
	public function getMaxlenght(): ?int
	{
		return $this->maxlenght;
	}
	public function getMultiple(): ?bool
	{
		return $this->multiple;
	}
	public function getPattern(): ?string
	{
		return $this->pattern;
	}
	public function getPlaceholder()
	{
		return $this->placeholder;
	}
	public function getReadonly(): ?bool
	{
		return $this->readonly;
	}
	public function getRequired(): ?bool
	{
		return $this->required;
	}
	public function getSrc(): ?string
	{
		return $this->src;
	}

	public function setType(string $type): void
	{
		$this->type = $type;
	}
	public function setAccept(string $accept): void
	{
		$this->accept = $accept;
	}
	public function setAlt(string $alt): void
	{
		$this->alt = $alt;
	}
	public function setAutocomplete(bool $autocomplete): void
	{
		$this->autocomplete = $autocomplete;
	}
	public function setAutofocus(bool $autofocus): void
	{
		$this->autofocus = $autofocus;
	}
	public function setChecked(bool $checked): void
	{
		$this->checked = $checked;
	}
	public function setDirname(string $dirname): void
	{
		$this->dirname = $dirname;
	}
	public function setFormaction(string $formaction): void
	{
		$this->formaction = $formaction;
	}
	public function setFormenctype(string $formenctype): void
	{
		$this->formenctype = $formenctype;
	}
	public function setFormmethod(string $formmethod): void
	{
		$this->formmethod = $formmethod;
	}
	public function setFormnovalidate(string $formnovalidate): void
	{
		$this->formnovalidate = $formnovalidate;
	}
	public function setFormtarget(string $formtarget): void
	{
		$this->formtarget = $formtarget;
	}
	public function setMin(int $min): void
	{
		$this->min = $min;
	}
	public function setMax(int $max): void
	{
		$this->max = $max;
	}
	public function setStep(int $step): void
	{
		$this->step = $step;
	}
	public function setMinlenght(int $minlenght): void
	{
		$this->minlenght = $minlenght;
	}
	public function setMaxlenght(int $maxlenght): void
	{
		$this->maxlenght = $maxlenght;
	}
	public function setMultiple(bool $multiple): void
	{
		$this->multiple = $multiple;
	}
	public function setPattern(string $pattern): void
	{
		$this->pattern = $pattern;
	}
	public function setPlaceholder($placeholder): void
	{
		$this->placeholder = $placeholder;
	}
	public function setReadonly(bool $readonly): void
	{
		$this->readonly = $readonly;
	}
	public function setRequired(bool $required): void
	{
		$this->required = $required;
	}
	public function setSrc(string $src): void
	{
		$this->src = $src;
	}
}
