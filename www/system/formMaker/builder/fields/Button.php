<?php

namespace Packages\FormMaker\Builder\Component\Field;

class Button extends FormTag
{
    private $autofocus;
	private $formaction;
	private $formenctype;
	private $formmethod;
	private $formnovalidate;
	private $formtarget;
    private $type;
    
    public function __construct(array $data = null)
	{
        $this->value = ($this->value == null ? 'Send !' : $this->value);
        parent::__construct($data);
	}

	public function getAutofocus(): ?bool
	{
		return $this->autofocus;
	}
	public function getFormaction(): ?string
	{
		return $this->formaction;
	}
	public function getFormenctype(): ?string
	{
		return $this->formenctype;
	}
	public function getFormmethod(): ?string
	{
		return $this->formmethod;
	}
	public function getFormnovalidate(): ?bool
	{
		return $this->formnovalidate;
	}
	public function getFormtarget(): ?string
	{
		return $this->formtarget;
	}
	public function getType(): ?string
	{
		return $this->type;
	}

	public function setAutofocus(bool $autofocus): void
	{
		$this->autofocus = $autofocus;
	}
	public function setFormaction(string $formaction): void
	{
		$this->formaction = $formaction;
	}
	public function setFormenctype(string $formenctype): void
	{
		$this->formenctype = $formenctype;
	}
	public function setFormmethod(string $formmethod): void
	{
		$this->formmethod = $formmethod;
	}
	public function setFormnovalidate(bool $formnovalidate): void
	{
		$this->formnovalidate = $formnovalidate;
	}
	public function setFormtarget(string $formtarget): void
	{
		$this->formtarget = $formtarget;
	}
	public function setType(string $type): void
	{
		$this->type = $type;
	}
}
