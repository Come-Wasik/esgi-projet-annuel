<?php

namespace Packages\FormMaker\Builder\Component\Field;

class OptionElement
{
	private $name;
	private $class;
	private $id;
	private $value;
	private $label;

	public function __construct(array $data = null)
	{
		if($data) {
			$this->hydrate($data);
		}
	}

	private function hydrate(array $data): void
    {
        foreach($data as $attName => $attValue) {
            $settername = 'set'. ucfirst($attName);
            $this->$settername($attValue);
        }
    }

	public function getName(): ?string
	{
		return $this->name;
	}
	public function getClass(): ?string
	{
		return $this->class;
	}
	public function getId(): ?string
	{
		return $this->id;
	}
	public function getValue(): ?string
	{
		return $this->value;
	}
	public function getLabel(): ?string
	{
		return $this->label;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}
	public function setClass(string $class): void
	{
		$this->class = $class;
	}
	public function setId(string $id): void
	{
		$this->id = $id;
	}
	public function setValue(string $value): void
	{
		$this->value = $value;
	}
	public function setLabel(string $label): void
	{
		$this->label = $label;
	}
}
