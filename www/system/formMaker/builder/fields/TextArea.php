<?php

namespace Packages\FormMaker\Builder\Component\Field;

class TextArea extends FormTag
{
	private $autofocus;
	private $cols;
	private $dirname;
	private $maxlenght;
	private $placeholder;
	private $readonly;
	private $required;
	private $rows;
	private $wrap;

	public function getAutofocus(): ?bool
	{
		return $this->autofocus;
	}
	public function getCols(): ?int
	{
		return $this->cols;
	}
	public function getDirname(): ?string
	{
		return $this->dirname;
	}
	public function getMaxlenght(): ?string
	{
		return $this->maxlenght;
	}
	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}
	public function getReadonly(): ?bool
	{
		return $this->readonly;
	}
	public function getRequired(): ?bool
	{
		return $this->required;
	}
	public function getRows(): ?int
	{
		return $this->rows;
	}
	public function getWrap(): ?string
	{
		return $this->wrap;
	}

	public function setAutofocus(bool $autofocus): void
	{
		$this->autofocus = $autofocus;
	}
	public function setCols(int $cols): void
	{
		$this->cols = $cols;
	}
	public function setDirname(string $dirname): void
	{
		$this->dirname = $dirname;
	}
	public function setMaxlenght(string $maxlenght): void
	{
		$this->maxlenght = $maxlenght;
	}
	public function setPlaceholder(string $placeholder): void
	{
		$this->placeholder = $placeholder;
	}
	public function setReadonly(bool $readonly): void
	{
		$this->readonly = $readonly;
	}
	public function setRequired(bool $required): void
	{
		$this->required = $required;
	}
	public function setRows(int $rows): void
	{
		$this->rows = $rows;
	}
	public function setWrap(string $wrap): void
	{
		$this->wrap = $wrap;
	}
}
