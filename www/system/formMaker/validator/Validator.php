<?php

namespace Packages\FormMaker\Validator;

use Exception;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use Packages\FormMaker\Validator\Exception\ValidatorException;

class Validator
{
    public function searchError(string $validatorName, Request $request): ?string
    {
        try {
            ### On contrôle le chemin du validator pour obtenir ses données ###
            $validatorPath = validator_path() . '/' . $validatorName . '.php';

            if (!file_exists($validatorPath)) {
                throw new Exception('Le validateur de formulaire "' . $validatorName . '" situé à l\'adresse ' . $validatorPath . ' n\'existe pas.');
            }

            ### On récupère et stocke des informations utiles pour la suite de la méthode
            $validatorData = include $validatorPath;
            $validatedFields = $validatorData['fields'];

            ### Récupération des éléments de la requête ###
            $requestedArgs = [];
            $requestedArgs += $request->getQuery();
            $requestedArgs += $request->getRequest();

            ### On vérifie la conformité de la quantité d'information envoyé par le formulaire ###
            if (count($requestedArgs) != count($validatedFields)) {
                throw new ValidatorException('La quantité d\'information entre celle envoyé et celle demandé est différente. Pour info : ' . count($requestedArgs) . ' au lieu de ' . count($validatedFields) . '.');
            }

            ### On vérifie la conformité du nom de chaque champs du formulaire envoyé ###
            foreach ($validatedFields as $fieldName => $fieldData) {
                if (!isset($requestedArgs[$fieldName])) {
                    throw new ValidatorException('Le champ "' . $fieldName . '" du formulaire n\'est pas exigé.');
                }
            }

            ### On va vérifier le type de donnée (text, number) de chaque champs ###
            foreach ($validatedFields as $fieldName => $fieldData) {
                ## On cherche ici à faire de la recherche, donc pas d'affectation par référence ##
                $currentValueArgument = $requestedArgs[$fieldName];

                switch ($fieldData['fieldType'] ?? 'text') {
                    ## La colorisation ##
                    case 'color':
                        if (
                            # On teste que la valeur est hexadécimale, mais on omet le "#" du début dans le test
                            ctype_xdigit(substr($currentValueArgument, 1)) === false
                        ) {
                            throw new ValidatorException('La couleur du champ "' . $currentValueArgument . '" est invalide !');
                        }
                        break;

                    ## Les dates et le temps ##
                    case 'date':
                        # La date doit être sous le format : 2019-05-25
                        $date = explode('-', $currentValueArgument);
                        if (
                            count($date) !== 3
                            || checkdate($date[1], $date[2], $date[0]) === false
                        ) {
                            throw new ValidatorException('La date du champ "' . $currentValueArgument . '" est invalide !');
                        }
                        break;
                    case 'time':
                        # Code inspiré du travail à l'adresse : https://www.php.net/manual/en/function.checkdate.php#113205
                        $dateFormat = 'H:i';
                        $time = \DateTime::createFromFormat($dateFormat, $currentValueArgument);

                        if (($time && $time->format($dateFormat) == $currentValueArgument) == false) {
                            throw new ValidatorException('L\'horaire du champ "' . $currentValueArgument . '" est invalide !');
                        }
                        break;
                    # Note : Les type suivants ne sont pas supportés par Firefox
                    case 'datetime-local':
                        $dateFormat = 'Y-m-d\TH:i';
                        $time = \DateTime::createFromFormat($dateFormat, $currentValueArgument);

                        if (($time && $time->format($dateFormat) == $currentValueArgument) == false) {
                            throw new ValidatorException('La date ou l\'horaire du champ "' . $currentValueArgument . '" est invalide !');
                        }
                        break;
                    case 'month':
                        $dateFormat = 'Y-m';
                        $time = \DateTime::createFromFormat($currentValueArgument, $myDate);

                        if (($time && $time->format($dateFormat) == $currentValueArgument) == false) {
                            throw new ValidatorException('La date du champ "' . $currentValueArgument . '" est invalide !');
                        }
                        break;
                    case 'week':
                        $dateFormat = 'Y-\WW';
                        # Le week end n'étant pas supporté par createFromFormat, j'utilise setISODate en suivant le conseil à l'adresse :
                        # https://www.php.net/manual/fr/datetime.createfromformat.php#119225
                        $date = new \DateTime();

                        # On récupère l'année et le mois pour l'utiliser dans la méthode setISODate
                        \sscanf($currentValueArgument, '%d-W%s', $year, $week);
                        $date->setISODate($year, $week);

                        if (($date && $date->format($dateFormat) == $currentValueArgument) == false) {
                            throw new ValidatorException('La date du champ "' . $currentValueArgument . '" est invalide !');
                        }
                        break;
                    ## Email ##
                    case 'email':
                        if (filter_var($currentValueArgument, FILTER_VALIDATE_EMAIL) === false) {
                            throw new ValidatorException('L\'email du champ "' . $currentValueArgument . '" est invalide !');
                        }
                        break;

                    ## Les fichiers ##
                    case 'file':
                        if (substr($currentValueArgument, 0, 8) != 'file:///') {
                            $currentValueArgument = 'file:///' . $currentValueArgument;
                        }
                        if (
                            filter_var($currentValueArgument, FILTER_VALIDATE_URL, [
                                'flags' => FILTER_FLAG_PATH_REQUIRED
                            ]) === false
                        ) {
                            throw new ValidatorException('Le lien du champ "' . $currentValueArgument . '" du champ ' . $fieldName . ' est invalide !');
                        }
                        break;

                    ## Les éléments à liste ##
                    case 'checkbox':
                        if (!isset($fieldData['acceptedValues'])) {
                            throw new ValidatorException('Erreur du fichier de metadata : La checkbox ' . $fieldName . ' n\'a pas de valeur à vérifier.');
                        } else {
                            if (!in_array($currentValueArgument, $fieldData['acceptedValues'])) {
                                throw new ValidatorException('La valeur ' . $currentValueArgument . ' de la case à cocher "' . $fieldName . '" ne peut être attribué !');
                            }
                        }
                        break;
                    case 'radio':
                        if (!isset($fieldData['acceptedValues'])) {
                            throw new ValidatorException('Erreur du fichier de metadata : Le bouton radio ' . $fieldName . ' n\'a pas de valeur à vérifier.');
                        } else {
                            if (!in_array($currentValueArgument, $fieldData['acceptedValues'])) {
                                throw new ValidatorException('La valeur ' . $currentValueArgument . ' du bouton radio "' . $fieldName . '" ne peut être attribué !');
                            }
                        }
                        break;
                    case 'datalist':
                        if (!isset($fieldData['acceptedValues'])) {
                            throw new ValidatorException('Erreur du fichier de métadonnées : le champ "' . $fieldName . '" du formulaire n\'a pas de valeur à vérifier.');
                        } else {
                            if (!in_array($currentValueArgument, $fieldData['acceptedValues'])) {
                                throw new ValidatorException('La valeur ' . $currentValueArgument . ' de la liste d\'options "' . $fieldName . '" ne peut être attribué !');
                            }
                        }
                        break;
                    ## Les éléments simples (ou à trier) ##
                    case 'number':
                        # Tester les valeurs min et max
                        if ($currentValueArgument < $fieldData['min'] || $currentValueArgument > $fieldData['max']) {
                            throw new ValidatorException('Le nombre ' . $currentValueArgument . ' du champ "' . $fieldName . '" n\'est pas accepté ! Il doit se trouver entre ' . $fieldData['min'] . ' et ' . $fieldData['max'] . '.');
                        }
                        break;
                    case 'range':
                        if ($currentValueArgument < $fieldData['min'] || $currentValueArgument > $fieldData['max']) {
                            throw new ValidatorException('Le nombre ' . $currentValueArgument . ' du champ "' . $fieldName . '" n\'est pas accepté ! Il doit se trouver entre ' . $fieldData['min'] . ' et ' . $fieldData['max'] . '.');
                        }
                        break;
                    case 'tel':
                        $str_contain_only_number = function ($numberList) {
                            for ($i = 0; $i < strlen($numberList); $i++) {
                                if (!\is_numeric($numberList[$i])) {
                                    return false;
                                }
                            }
                            return true;
                        };

                        if (
                            strlen($currentValueArgument) < 8
                            || ($str_contain_only_number($currentValueArgument)) === false
                            && ($currentValueArgument[0] != '+')
                            || ($str_contain_only_number(substr($currentValueArgument, 1))) === false
                        ) {
                            throw new ValidatorException('Le numéro de téléphone ' . $currentValueArgument . ' du champ "' . $fieldName . '" est invalide !');
                        }
                        break;
                    case 'url':
                        if (filter_var($currentValueArgument, FILTER_VALIDATE_URL) === false) {
                            throw new ValidatorException('L\'url ' . $currentValueArgument . ' du champ ' . $fieldName . ' est invalide !');
                        }
                        break;
                    case 'text':
                    default:
                        break;
                }
            }

            return null; # Pas d'erreur
        } catch (ValidatorException $error) {
            return $error->getMessage();
        }
    }
}
