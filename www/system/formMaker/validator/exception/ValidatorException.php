<?php

namespace Packages\FormMaker\Validator\Exception;

use Exception;
use Throwable;

class ValidatorException extends Exception
{
    public function __construct(string $message = '', int $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
