<?php

namespace App\Entity;

use DateTime;

class Articles
{
	private $id;
	private $name;
	private $description;
	private $content;
	private $author_id;
	private $published;
	private $created_at;
	private $updated_at;
	private $categories_id;

	public function getId(): ?int
	{
		return $this->id;
	}
	public function getName(): ?string
	{
		return $this->name;
	}
	public function getDescription(): ?string
	{
		return $this->description;
	}
	public function getContent(): ?string
	{
		return $this->content;
	}
	public function getAuthor_id(): ?int
	{
		return $this->author_id;
	}
	public function getPublished(): ?int
	{
		return $this->published;
	}
	public function getCreated_at(): ?string
	{
		return $this->created_at;
	}
	public function getUpdated_at(): ?string
	{
		return $this->updated_at;
	}
	public function getCategories_id(): ?int
	{
		return $this->categories_id;
	}

	public function setId(int $id): void
	{
		$this->id = $id;
	}
	public function setName(string $name): void
	{
		$this->name = $name;
	}
	public function setDescription(string $description): void
	{
		$this->description = $description;
	}
	public function setContent(string $content): void
	{
		$this->content = $content;
	}
	public function setAuthor_id(int $author_id): void
	{
		$this->author_id = $author_id;
	}
	public function setPublished(int $published): void
	{
		$this->published = $published;
	}
	public function setCreated_at(string $created_at): void
	{
		$this->created_at = $created_at;
	}
	public function setUpdated_at(string $updated_at): void
	{
		$this->updated_at = $updated_at;
	}
	public function setCategories_id(int $categories_id): void
	{
		$this->categories_id = $categories_id;
	}

	public function forCreation()
	{
		$this->created_at = getFormattedTimestamp();
		$this->updated_at = getFormattedTimestamp();
	}
	public function forUpdate()
	{
		$this->updated_at = getFormattedTimestamp();
	}

	public function isPublished()
	{
		return $this->published;
	}
}
