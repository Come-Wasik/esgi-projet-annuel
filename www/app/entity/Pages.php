<?php

namespace App\Entity;

class Pages
{
	private $id;
	private $title;
	private $slug;
	private $description;
	private $body;
	private $published;
	private $created_at;
	private $updated_at;

	public function getId(): ?int
	{
		return $this->id;
	}
	public function getTitle(): ?string
	{
		return $this->title;
	}
	public function getSlug(): ?string
	{
		return $this->slug;
	}
	public function getDescription(): ?string
	{
		return $this->description;
	}
	public function getBody(): ?string
	{
		return $this->body;
	}
	public function getPublished(): ?int
	{
		return $this->published;
	}
	public function getCreated_at(): ?string
	{
		return $this->created_at;
	}
	public function getUpdated_at(): ?string
	{
		return $this->updated_at;
	}

	public function setId(int $id): void
	{
		$this->id = $id;
	}
	public function setTitle(string $title): void
	{
		$this->title = $title;
	}
	public function setSlug(string $slug): void
	{
		$this->slug = $slug;
	}
	public function setDescription(string $description): void
	{
		$this->description = $description;
	}
	public function setBody(string $body): void
	{
		$this->body = $body;
	}
	public function setPublished(int $published): void
	{
		$this->published = $published;
	}
	public function setCreated_at(string $created_at): void
	{
		$this->created_at = $created_at;
	}
	public function setUpdated_at(string $updated_at): void
	{
		$this->updated_at = $updated_at;
	}

	public function isPublished(): int
	{
		return $this->published;
	}

	public function forCreation()
	{
		$this->created_at = getFormattedTimestamp();
		$this->updated_at = getFormattedTimestamp();
	}
	public function forUpdate()
	{
		$this->updated_at = getFormattedTimestamp();
	}
}
