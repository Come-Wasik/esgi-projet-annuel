<?php

namespace App\Entity;

class Article_have_tag
{
    private $id;
    private $articles_id;
    private $tags_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticles_id(): ?int
    {
        return $this->articles_id;
    }

    public function getTags_id(): ?int
    {
        return $this->tags_id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setArticles_id(int $articles_id): void
    {
        $this->articles_id = $articles_id;
    }

    public function setTags_id(int $tags_id): void
    {
        $this->tags_id = $tags_id;
    }
}
