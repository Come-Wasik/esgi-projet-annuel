<?php

namespace App\Entity;

class Tags
{
	private $id;
	private $name;
	private $description;
	private $created_at;
	private $updated_at;

	public function getId(): ?int
	{
		return $this->id;
	}
	public function getName(): ?string
	{
		return $this->name;
	}
	public function getDescription(): ?string
	{
		return $this->description;
	}
	public function getCreated_at(): ?string
	{
		return $this->created_at;
	}
	public function getUpdated_at(): ?string
	{
		return $this->updated_at;
	}

	public function setId(int $id): void
	{
		$this->id = $id;
	}
	public function setName(string $name): void
	{
		$this->name = $name;
	}
	public function setDescription(string $description): void
	{
		$this->description = $description;
	}
	public function setCreated_at(string $created_at): void
	{
		$this->created_at = $created_at;
	}
	public function setUpdated_at(string $updated_at): void
	{
		$this->updated_at = $updated_at;
	}

	public function forCreation()
	{
		$this->created_at = getFormattedTimestamp();
		$this->updated_at = getFormattedTimestamp();
	}
	public function forUpdate()
	{
		$this->updated_at = getFormattedTimestamp();
	}
}
