<?php

namespace App\Entity;

class Comments
{
	private $id;
	private $content;
	private $validation;
	private $created_at;
	private $updated_at;
	private $articles_id;
	private $users_id;

	public function getId(): ?int
	{
		return $this->id;
	}
	public function getContent(): ?string
	{
		return $this->content;
	}
	public function getValidation(): ?int
	{
		return $this->validation;
	}
	public function getCreated_at(): ?string
	{
		return $this->created_at;
	}
	public function getUpdated_at(): ?string
	{
		return $this->updated_at;
	}
	public function getArticles_id(): ?int
	{
		return $this->articles_id;
	}
	public function getUsers_id(): ?int
	{
		return $this->users_id;
	}

	public function setId(int $id): void
	{
		$this->id = $id;
	}
	public function setContent(string $content): void
	{
		$this->content = $content;
	}
	public function setValidation(int $validation): void
	{
		$this->validation = $validation;
	}
	public function setCreated_at(string $created_at): void
	{
		$this->created_at = $created_at;
	}
	public function setUpdated_at(string $updated_at): void
	{
		$this->updated_at = $updated_at;
	}
	public function setArticles_id(int $articles_id): void
	{
		$this->articles_id = $articles_id;
	}
	public function setUsers_id(int $users_id): void
	{
		$this->users_id = $users_id;
	}

	# Adding object after requested in database
	public function setAuthor(Users $user)
	{
		$this->author = $user;
	}
	public function getAuthor()
	{
		return $this->author;
	}

	public function setArticle(Articles $article)
	{
		$this->article = $article;
	}
	public function getArticle()
	{
		return $this->article;
	}

	public function validate()
	{
		$this->validation = true;
	}

	public function invalidate()
	{
		$this->validation = false;
	}

	public function isValidate()
	{
		return ($this->validation ? true : false);
	}

	public function forCreation()
	{
		$this->created_at = getFormattedTimestamp();
		$this->updated_at = getFormattedTimestamp();
	}
	public function forUpdate()
	{
		$this->updated_at = getFormattedTimestamp();
	}
}
