<?php

namespace App\Entity;

class Users
{
	private $id;
	private $username;
	private $firstname;
	private $lastname;
	private $password;
	private $email;
	private $role_id;
	private $status;
	private $created_at;
	private $updated_at;
	private $email_verified_at;
	private $email_token;
	private $last_connexion;
	private $session_token;

	public function getId(): ?int
	{
		return $this->id;
	}
	public function getUsername(): ?string
	{
		return $this->username;
	}
	public function getFirstname(): ?string
	{
		return $this->firstname;
	}
	public function getLastname(): ?string
	{
		return $this->lastname;
	}
	public function getPassword(): ?string
	{
		return $this->password;
	}
	public function getEmail(): ?string
	{
		return $this->email;
	}
	public function getRole_id(): ?int
	{
		return $this->role_id;
	}
	public function getStatus(): ?string
	{
		return $this->status;
	}
	public function getCreated_at(): ?string
	{
		return $this->created_at;
	}
	public function getUpdated_at(): ?string
	{
		return $this->updated_at;
	}
	public function getEmail_verified_at(): ?string
	{
		return $this->email_verified_at;
	}
	public function getEmail_token(): ?string
	{
		return $this->email_token;
	}
	public function getLast_connexion(): ?string
	{
		return $this->last_connexion;
	}
	public function getSession_token(): ?string
	{
		return $this->session_token;
	}

	public function setId(int $id): void
	{
		$this->id = $id;
	}
	public function setUsername(string $username): void
	{
		$this->username = $username;
	}
	public function setFirstname(string $firstname): void
	{
		$this->firstname = $firstname;
	}
	public function setLastname(string $lastname): void
	{
		$this->lastname = $lastname;
	}
	public function setPassword(string $password): void
	{
		$this->password = $password;
	}
	public function setEmail(string $email): void
	{
		$this->email = $email;
	}
	public function setRole_id(int $role_id): void
	{
		$this->role_id = $role_id;
	}
	public function setStatus(string $status): void
	{
		$this->status = $status;
	}
	public function setCreated_at(string $created_at): void
	{
		$this->created_at = $created_at;
	}
	public function setUpdated_at(string $updated_at): void
	{
		$this->updated_at = $updated_at;
	}
	public function setEmail_verified_at(string $email_verified_at): void
	{
		$this->email_verified_at = $email_verified_at;
	}
	public function setEmail_token($email_token): void
	{
		$this->email_token = $email_token;
	}
	public function setLast_connexion(string $last_connexion): void
	{
		$this->last_connexion = $last_connexion;
	}
	public function setSession_token(string $session_token): void
	{
		$this->session_token = $session_token;
	}

	public function forCreation()
	{
		$this->created_at = getFormattedTimestamp();
		$this->updated_at = getFormattedTimestamp();
	}
	public function forUpdate()
	{
		$this->updated_at = getFormattedTimestamp();
	}

	public function mailIsVerified(): bool
	{
		if ($this->email_verified_at) {
			return true;
		}
		return false;
	}
}
