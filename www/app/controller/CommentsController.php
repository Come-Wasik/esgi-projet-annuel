<?php

namespace App\Controller;

use App\Entity\Comments;
use Flash;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use Session;

class CommentsController
{
    /**
     * Ajouter / Supprimer
     */
    public function store(Request $request, string $slug)
    {
        $postData = $request->getRequest();
        $dbConnexion = \DB::manager();

        ### Création du nouveau commenraire
        $errors = validate($postData, 'comments');

        $articleIdToComment = $slug;

        if (!$errors->any()) {
            $newComment = new Comments();
            foreach ($postData as $key => $data) {
                $setter = 'set' . ucfirst($key);
                $newComment->$setter($data);
            }

            $newComment->setUsers_id(\User::id());
            $newComment->setArticles_id($articleIdToComment);
            $newComment->invalidate();
            $newComment->forCreation();

            $dbConnexion->add($newComment);

            Flash::set('success', 'Your comment will be verified by admin before acceptation');
        } else {
            Flash::set('error', 'The comment cannot be posted');
        }
        redirection(getRouteUrl('posts.show') . $articleIdToComment);
    }

    public function delete(Request $request)
    {
        ### Si l'utilisateur est connecté et que c'est son commentaire, alors le supprimer
        $postData = $request->getRequest();
        $commentId = $postData['id_comment'];
        $dbConnexion = \DB::manager();

        $comment = $dbConnexion->get(Comments::class, $commentId);
        if (
            \User::isConnected()
            && $comment->getUsers_id() === \User::id()
        ) {
            $dbConnexion->delete($comment);
            \Flash::set('success', 'Your comment has been deleted');
        } else {
            \Flash::set('error', 'You are not allowed to do this action');
        }
        redirection(getRouteUrl('posts.show') . $comment->getArticles_id());
    }
}
