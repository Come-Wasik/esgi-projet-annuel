<?php

namespace App\Controller;

use App\Entity\Pages;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class PagesController
{
    public function searchAndRender(Request $request)
    {
        $uri = trim($request->getUri(), '/');
        $uri = '/' . $uri;

        $dbConnexion = \DB::manager();
        $pages = $dbConnexion->request('SELECT * FROM ' . DB_PREFIX . 'pages WHERE slug= :slug', ['slug' => $uri], 'class', [
            'classname' => Pages::class
        ]);

        if (empty($pages)) {
            return null;
        }

        $page = $pages[0];
        // Get slug in database
        if ($uri !== $page->getSlug()) {
            return null;
        }

        if (!$page->isPublished()) {
            return null;
        }

        return view('front/templates/' . \Config::get('cms.template_front'), [
            'title' => $page->getTitle(),
            '_sectionContent' => $page->getBody(),
            'pagesInNavbar' => getNavBarContent()
        ]);
    }
}
