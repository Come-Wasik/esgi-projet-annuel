<?php

namespace App\Controller;

use App\Entity\Article_have_tag;
use App\Entity\Articles;
use App\Entity\Categories;
use App\Entity\Comments;
use App\Entity\Pages;
use App\Entity\Tags;
use App\Entity\Users;

class ArticlesController
{
    /**
     *
     * List les article
     *
     * @return false|string
     * @throws \Exception
     * @method GET
     */
    public function index()
    {
        # Get all articles
        $dbConnexion = \DB::manager();
        $articles = $dbConnexion->getAll(Articles::class);
        $categories = $dbConnexion->getAll(Categories::class);
        $tags = $dbConnexion->getAll(Tags::class);
        $relBetwArticleAndTag = $dbConnexion->getAll(Article_have_tag::class);
        $users = $dbConnexion->getAll(Users::class);

        # get page list to the navbar
        $pagesInNavbar = [];
        foreach ($dbConnexion->getAll(Pages::class) as $page) {
            if (
                $page->getSlug() != '/'
                && $page->isPublished()
            ) {
                $pagesInNavbar[] = $page;
            }
        }

        return view('front/articles/index', [
            'articles' => $articles,
            'categories' => $categories,
            'tags' => $tags,
            'relBetwArticleAndTag' => $relBetwArticleAndTag,
            'users' => $users,
            'pagesInNavbar' => $pagesInNavbar,
        ]);
    }

    public function show(int $id_article)
    {
        # Get one article
        $dbConnexion = \DB::manager();
        $article = $dbConnexion->get(Articles::class, $id_article);

        # Error entry
        if (empty($article)) {
            redirection(getRouteUrl('posts.index'));
        }
        $user = $dbConnexion->get(Users::class, $article->getAuthor_id());

        # Get the comment list
        $comments = $dbConnexion->request('SELECT * FROM ' . DB_PREFIX . 'Comments WHERE articles_id = :id_article AND validation = 1', [
            'id_article' => $id_article
        ], 'class', [
            'classname' => Comments::class
        ]);
        # Remplissage de l'auteur
        foreach ($comments as &$comment) {
            $author = $dbConnexion->get(Users::class, $comment->getUsers_id());
            $comment->setAuthor($author);
        }

        return view('front/articles/show', [
            'article' => $article,
            'user' => $user,
            'pagesInNavbar' => getNavBarContent(),
            'comments' => $comments,
        ]);
    }
}
