<?php

namespace App\Controller;

use App\Entity\Users;
use User;
use DB;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use Session;

class UserProfileController
{
    public function edit()
    {
        if (User::isConnected()) {
            $errors = null;
            $me = DB::manager()->get(Users::class, User::id());

            if (Session::has('form_error')) {
                $errors = Session::get('form_error');
                Session::delete('form_error');
            }

            return view('front/user-profile/edit', [
                'pagesInNavbar' => getNavBarContent(),
                'user' => $me,
                'errors' => $errors
            ]);
        }
    }

    public function update(Request $request)
    {
        if (User::isConnected()) {
            $user = DB::manager()->get(Users::class, User::id());
            $postData = $request->getRequest();
            unset($postData['_method']);

            ### Vérification de l'existance des champs + protection XSS avec htmlentities + rejet d'un champ inconnu
            $errors = validate($postData, 'user-profile_update');

            ## Vérification des champs
            # On choisit ou non d'avoir à modifier le mot de passe
            if (empty($postData['password']) && empty($postData['passwordConfirm'])) {
                unset($postData['password'], $postData['passwordConfirm']);
                # Vérification du mot de passe
            } elseif ($postData['password'] != $postData['passwordConfirm']) {
                $errors->addError('passwordConfirm', 'passwordDoNotMatch');
                unset($postData['passwordConfirm']);
            } else {
                # Même si les mots de passe sont pareils, on a plus besoin de passwordConfirm
                $postData['password'] = password_hash($postData['password'], PASSWORD_BCRYPT);
                unset($postData['passwordConfirm']);
            }

            ## Enregistrement des données
            if ($errors->any()) {
                Session::set('form_error', $errors->all());
                redirection(getRouteUrl('user-param.edit'));
            }

            # Appel automatique des setters
            foreach ($postData as $key => $data) {
                $setter = 'set' . ucfirst($key);
                $user->$setter($data);
            }
            $user->forUpdate();

            DB::manager()->update($user);
            \Flash::set('success', 'Your user profile has been updated');
            redirection(getRouteUrl('user-param.edit'));
        }
    }
}
