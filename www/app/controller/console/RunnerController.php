<?php

namespace App\Controller\Console;

use Command;

class RunnerController
{
    public function runTest(string $TestGroupName, string $testName)
    {

        # The group is in reality the class name
        $TestClassName = ucfirst($TestGroupName);
        $TestFullClassName = 'Framework\\Test\\' . $TestClassName;
        $testPath = base_path() . '/ressources/packages/tests';

        # Tests to know if the test can be launch
        require $testPath . '/' . $TestClassName . '.php';
        if (!class_exists($TestFullClassName)) {
            Command::write('Class ' . $TestFullClassName . ' don\'t exists', 'error');
            die();
        }
        if (!method_exists($TestFullClassName, $testName)) {
            Command::write('Method/Test ' . $testName . ' don\'t exists', 'error');
            die();
        }

        # Launch the test
        Command::write('Test starting');
        (new $TestFullClassName())->$testName();

        Command::write('The test has been correctly launched');
    }
}
