<?php

namespace App\Controller\Console;

use Command;

class MakerController
{
    public function makeController(string $controllerFullname)
    {
        Command::write('The controller ' . $controllerFullname . ' will be create');
        # Allow to control namespace with array
        $explodedController = explode('/', $controllerFullname);

        $controllerNamespace = array_merge( ## Adding App/Controller in the namespace
            ['App', 'Controller'],
            array_slice( ## Deleting the controller name
                array_map(function ($entry) { # Change all name in PascalCase
                    return ucfirst($entry);
                }, $explodedController),
                0,
                count($explodedController) - 1
            )
        );
        # Comment : The class name and namespace should be in PascalCase
        $controllerOnlyName = ucfirst(array_slice($explodedController, -1, 1)[0]);

        # Write the file in a stream
        $fileContent = "<?php\n"
            . "\n"
            . 'namespace ' . implode('\\', $controllerNamespace) . ";\n"
            . "\n"
            . 'class ' . $controllerOnlyName . "Controller\n"
            . "{\n"
            . "\n"
            . "}\n"
            . "\n";

        # generate the controller path with the namespace #
        $controllerDir = controller_path() . '/' . implode( # Get the namespace with / separator
            '/',
            array_map(
                function ($entry) {
                    return lcfirst($entry);
                },
                array_slice( ## Select only controller namespace
                    $controllerNamespace,
                    2 ## Deleting the App/Controller part of the namespace
                )
            )
        );

        # Generate the directory if not exists #
        if (!file_exists($controllerDir)) {
            mkdir($controllerDir);
        }

        # Generate the file #
        file_put_contents($controllerDir . '/' . $controllerOnlyName . 'Controller.php', $fileContent);
        Command::write('The controller ' . $controllerFullname . ' has been created.', 'success');
    }

    public function makeEntity(string $entityName, string $entityAttributes = 'id:int')
    {
        Command::write('The ' . $entityName . ' will be create.');
        # Change here the entityDirectory
        $entityDirectory = entity_path();

        # The class name should be in PascalCase
        $entityName = ucfirst($entityName);

        # Filtering of ; at the beginning and end so as not to write an empty value in the file later on
        $entityAttributes = trim($entityAttributes, ';"');

        # Forced adding for id
        $idIsPresent = false;
        $allAttrs = explode(';', $entityAttributes);
        for ($i = 0; $i < count($allAttrs) && $idIsPresent == false; $i++) {
            $attrData = explode(':', $allAttrs[$i]);
            if ($attrData[0] == 'id') {
                $idIsPresent = true;
            }
        }
        if ($idIsPresent === false) {
            $entityAttributes = 'id:int,' . $entityAttributes;
        }

        $getAttrType = function ($attribute) {
            $attribute = trim($attribute, ',;:');
            if (count($temp = explode(':', $attribute)) == 2) {
                return [
                    $temp[0],
                    $temp[1]
                ];
            }
            return [
                $temp[0],
                null
            ];
        };

        $fileContent = "<?php\n"
            . "\n"
            . "namespace App\\Entity;\n"
            . "\n"
            . 'class ' . $entityName . "\n"
            . "{\n";

        # Properties definition
        foreach (explode(',', $entityAttributes) as $attribute) {
            $attribute = trim($attribute);
            list($attribute, $attrType) = $getAttrType($attribute);

            $fileContent .= "\tprivate $" . $attribute . ";\n";
        }

        # getters definition
        $fileContent .= "\n";
        foreach (explode(',', $entityAttributes) as $attribute) {
            $attribute = trim($attribute);
            list($attribute, $attrType) = $getAttrType($attribute);

            $fileContent .= "\tpublic function get" . ucfirst($attribute) . '()' . ($attrType !== null ? ': ?' . $attrType : '') . "\n"
                . "\t{\n"
                . "\t\treturn \$this->" . $attribute . ";\n"
                . "\t}\n";
        }

        # Setters definition
        $fileContent .= "\n";
        foreach (explode(',', $entityAttributes) as $attribute) {
            $attribute = trim($attribute);
            list($attribute, $attrType) = $getAttrType($attribute);

            $fileContent .= "\tpublic function set" . ucfirst($attribute) . '(' . ($attrType !== null ? $attrType . ' ' : '') . '$' . $attribute . "): void\n"
                . "\t{\n"
                . "\t\t\$this->" . $attribute . ' = $' . $attribute . ";\n"
                . "\t}\n";
        }
        $fileContent .= "}\n"
            . "\n";

        # Generate the directory if not exists #
        if (!file_exists($entityDirectory)) {
            mkdir($entityDirectory);
        }

        # Generate the file #
        file_put_contents($entityDirectory . '/' . $entityName . '.php', $fileContent);
        Command::write('The entity ' . $entityName . ' has been created.', 'success');
    }

    public function makeValidator(string $validatorName)
    {
        $validatorPath = validator_path();

        $fileContent = "<?php\n"
            . "\n"
            . "return [\n"
            . "\t'field1' => 'required',\n"
            . "\t'field2' => ['required', 'min:5', 'max: 20'],\n"
            . "\t'field3' => 'required|min:1|max:20|type:string|format:email'\n"
            . "];\n";

        # Generate the directory if not exists #
        if (!file_exists($validatorPath) || !is_dir($validatorPath)) {
            mkdir($validatorPath);
        }

        # Generate the file #
        file_put_contents($validatorPath . '/' . $validatorName . '.php', $fileContent);
        Command::write('The validator ' . $validatorName . ' has been created.', 'success');
    }

    public function makeTest(string $testName)
    {
        $testName = ucfirst($testName);
        $testDirectory = test_path();

        $fileContent = "<?php\n"
            . "\n"
            . "namespace Framework\\Test;\n"
            . "\n"
            . "use Exception;\n"
            . "\n"
            . 'class ' . $testName . "\n"
            . "{\n"
            . "\tpublic function tryMe()\n"
            . "\t{\n"
            . "\t\t# Write yours tests here and launch the command run:test " . $testName . " tryMe\n"
            . "\t}\n"
            . "}\n";

        # Generate the directory if not exists #
        if (!file_exists($testDirectory) || !is_dir($testDirectory)) {
            mkdir($testDirectory);
        }

        # Generate the file #
        file_put_contents($testDirectory . '/' . $testName . '.php', $fileContent);
        Command::write('The test ' . $testName . ' has been created.');
    }

    public function makeClass(string $className, string $classAttributes = null)
    {
        # Class directories #
        $classDir = app_path() . '/component';

        # The class name should be in PascalCase
        $className = ucfirst($className);

        # Filtering of ; at the beginning and end so as not to write an empty value in the file later on
        $classAttributes = trim($classAttributes, ';"');

        $getAttrType = function ($attribute) {
            $attribute = trim($attribute, ',;:');
            if (count($temp = explode(':', $attribute)) == 2) {
                return [
                    $temp[0],
                    $temp[1]
                ];
            }
            return [
                $temp[0],
                null
            ];
        };

        $fileContent = "<?php\n"
            . "\n"
            . "namespace App\\Component;\n"
            . "\n"
            . 'class ' . $className . "\n"
            . "{\n";

        # Properties definition
        if ($classAttributes) {
            foreach (explode(';', $classAttributes) as $attribute) {
                $attribute = trim($attribute);
                list($attribute, $attrType) = $getAttrType($attribute);

                $fileContent .= "\tprivate $" . $attribute . ";\n";
            }

            # getters definition
            $fileContent .= "\n";
            foreach (explode(';', $classAttributes) as $attribute) {
                $attribute = trim($attribute);
                list($attribute, $attrType) = $getAttrType($attribute);

                $fileContent .= "\tpublic function get" . ucfirst($attribute) . '()' . ($attrType !== null ? ': ?' . $attrType : '') . "\n"
                    . "\t{\n"
                    . "\t\treturn \$this->" . $attribute . ";\n"
                    . "\t}\n";
            }

            # Setters definition
            $fileContent .= "\n";
            foreach (explode(';', $classAttributes) as $attribute) {
                $attribute = trim($attribute);
                list($attribute, $attrType) = $getAttrType($attribute);

                $fileContent .= "\tpublic function set" . ucfirst($attribute) . '(' . ($attrType !== null ? $attrType . ' ' : '') . '$' . $attribute . "): void\n"
                    . "\t{\n"
                    . "\t\t\$this->" . $attribute . ' = $' . $attribute . ";\n"
                    . "\t}\n";
            }
        } else {
            $fileContent .= "\t\n";
        }

        # End of file
        $fileContent .= "}\n";

        # Generate the directory if not exists #
        if (!file_exists($classDir)) {
            mkdir($classDir);
        }

        # Generate the file #
        file_put_contents($classDir . '/' . $className . '.php', $fileContent);
        Command::write('The class ' . $className . ' has been created.', 'success');
    }
}
