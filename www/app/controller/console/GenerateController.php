<?php

/**
 * DO NOT DELETE THIS FILE
 */

namespace App\Controller\Console;

use App\Entity\Articles;
use App\Entity\Pages;
use Command;
use DateTime;
use DB;
use SimpleXMLElement;
use Str;

class GenerateController
{
    public function sitemap()
    {
        $websiteUrl = 'https://cms-blog.online/';

        Command::write('Démarrage', 'info');
        $fileContent = file_get_contents(storage_path('sitemap_template.xml'));
        $xmlObject = new SimpleXMLElement($fileContent);

        Command::write('Recherche des articles disponibles en bdd', 'info');
        $articles = DB::manager()->getAll(Articles::class);
        foreach ($articles as $article) {
            if ($article->isPublished()) {
                $articleNode = $xmlObject->addChild('url');
                $articleNode->addChild('loc', $websiteUrl . 'articles/' . $article->getId());
                $updatedAt = new DateTime($article->getUpdated_at());
                $articleNode->addChild('lastmod', $updatedAt->format('Y-m-d'));
            }
        }

        Command::write('Recherche des pages disponibles en bdd', 'info');
        $pages = DB::manager()->getAll(Pages::class);
        foreach ($pages as $page) {
            if ($page->getId() != 1) {
                $pageNode = $xmlObject->addChild('url');
                $pageNode->addChild('loc', $websiteUrl . ltrim($page->getSlug(), '/'));
                $updatedAt = new DateTime($page->getUpdated_at());
                $pageNode->addChild('lastmod', $updatedAt->format('Y-m-d'));
            }
        }

        Command::write('Enregistrement des nouvelles données dans le sitemap.xml', 'info');
        file_put_contents(base_path() . '/public/sitemap.xml', $xmlObject->asXML());
        Command::write('Enregistrement réussi !', 'success');
    }
}
