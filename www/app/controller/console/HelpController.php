<?php

namespace App\Controller\Console;

use Framework\Architecture\Console\Router\RouteManager;

class HelpController
{
    public function showHelp()
    {
        echo 'Command list :';
        foreach (RouteManager::getCommandList() as $command) {
            if ($command->getRegex() != '--help' && $command->getRegex() != '-h') {
                echo "\r\n" . "\t\e[93m" . $command->getRegex() . "\e" . '[39m';
            }
        }
        echo "\n";
    }
}
