<?php

/**
 * DO NOT DELETE THIS FILE
 */

namespace App\Controller\Console;

use Framework\Architecture\Web\Routage\RouteManager;

class DebugController
{
    public function showRoutes()
    {
        $webRouteFilePath = \route_path() . '/web.php';

        include $webRouteFilePath;

        $routeManager = new RouteManager();
        $routes = $routeManager->getRouteList();

        if (!empty($routes)) {
            echo 'Route list :';
            echo "\r\nROUTE NAME\t\t\tROUTE URL\t\tROUTE METHOD";
            foreach ($routes as $route) {
                echo "\r\n" . $route->getName() . "\t\t\t" . $route->getUrl() . "\t\t\t" . $route->getMethod();
            }
            echo "\n";
        }
    }
}
