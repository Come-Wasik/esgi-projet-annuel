<?php

namespace App\Controller\Dashboard;

use App\Entity\Pages;
use Exception;
use Flash;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class PagesController
{
    /**
     * Affiches le pannel d'administration des pages
     *
     * Liste l'ensemble des pages et la possibilité de les éditer, supprimer ou d'en ajouter
     *
     * @return false|string
     * @method GET
     */
    public function index()
    {
        $dbConnexion = \DB::manager();
        $pages = $dbConnexion->getAll(Pages::class);

        return view('dashboard/pages/index', [
            'pages' => $pages
        ]);
    }

    /**
     * Permet de créer une nouvelle page
     *
     * Affiche le formulaire et enregistre la nouvelle page en base de données.
     *
     * @param Request $request
     * @return false|string la page qui a été créée
     * @method GET / POST
     **/
    public function create(Request $request)
    {
        $errors = null;
        $dbConnexion = \DB::manager();

        if ($request->getHttpMethod() === 'POST') {
            $pageData = $request->getRequest();
            $errors = validate($pageData, 'pages');

            ## Filtering url which not begin by / and end by a /
            $pageData['slug'] = trim($pageData['slug'], '/');
            $pageData['slug'] = '/' . $pageData['slug'];

            ## Saisie du status de publication de la page en fonction de la valeur de la checkbox
            if (key_exists('published', $pageData)) {
                $pageData['published'] = true;
            } else {
                $pageData['published'] = false;
            }

            if (!$errors->any()) {
                $newPage = new Pages();

                # Appel automatique des setters
                foreach ($pageData as $key => $data) {
                    $setter = 'set' . ucfirst($key);
                    $newPage->$setter($data);
                }
                $newPage->forCreation();

                # Enregistrement de la page
                $dbConnexion->add($newPage);
                \Flash::set('success', 'The page has been created');
                redirection(getRouteUrl('pages.index'));
            } else {
                $errors = $errors->all();
            }
        }

        return view('dashboard/pages/create', [
            'errors' => $errors
        ]);
    }

    /**
     * Permet d'éditer une page
     *
     * Fournit un formulaire de modification de la page et traite la modification
     *
     * @param Request $request
     * @param int $id_page
     * @return false|string la vue permettant d'éditer un nouvel.
     * @method GET / POST
     */
    public function edit(Request $request, int $id_page)
    {
        $errors = null;
        $dbConnexion = \DB::manager();
        $page = $dbConnexion->get(Pages::class, $id_page);

        if ($request->getHttpMethod() === 'POST') {
            $pageData = $request->getRequest();
            $errors = validate($pageData, 'pages');

            ## Filtering url which not begin by / and end by a /
            $pageData['slug'] = trim($pageData['slug'], '/');
            $pageData['slug'] = '/' . $pageData['slug'];

            ## Saisie du status de publication de la page en fonction de la valeur de la checkbox
            if (key_exists('published', $pageData)) {
                $pageData['published'] = true;
            } else {
                $pageData['published'] = false;
            }

            $pageData['updated_at'] = getFormattedTimestamp();

            if (!$errors->any()) {
                # Appel automatique des setters
                foreach ($pageData as $key => $data) {
                    $setter = 'set' . ucfirst($key);
                    $page->$setter($data);
                }
                $page->forUpdate();

                $dbConnexion->update($page);
                \Flash::set('success', 'The page with id ' . $page->getId() . ' has been updated');
                redirection(getRouteUrl('pages.index'));
            } else {
                $errors = $errors->all();
            }
        }

        return view('dashboard/pages/edit', [
            'errors' => $errors,
            'page' => $page
        ]);
    }

    /**
     * Permet de supprimer une page dans le dashboard
     *
     * @param Request $request Requête Http
     * @method POST
     **/
    public function delete(Request $request)
    {
        $pageData = $request->getRequest();

        if (key_exists('id_page', $pageData)) {
            $id = (int) $pageData['id_page'];

            # We verify that the id of this page is not the "home page"
            if ($id === 1) {
                Flash::set('error', 'This page cannot be deleted');
            }

            if ($id > 1) {
                $dbConnexion = \DB::manager();

                if ($dbConnexion->get(Pages::class, $id) !== null) {
                    $dbConnexion->delete(Pages::class, $id);
                    \Flash::set('success', 'The page with id ' . $pageData['id_page'] . ' has been deleted');
                } else {
                    \Flash::set('error', 'The page with id ' . $pageData['id_page'] . ' do not exists');
                }
            }
        }
        redirection(getRouteUrl('pages.index'));
    }
}
