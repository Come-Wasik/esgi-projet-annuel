<?php

namespace App\Controller\Dashboard;

use App\Entity\Categories;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class CategoriesController
{
    /**
     * @Method = GET
     */
    public function index()
    {
        $dbConnexion = \DB::manager();
        $categories = $dbConnexion->getAll(Categories::class);

        return view('dashboard/categories/index', [
            'categories' => $categories
        ]);
    }

    /**
     * @Method = GET / POST
     * @param Request $request
     * @return false|string
     */
    public function create(Request $request)
    {
        $errors = null;

        if ($request->getHttpMethod() == 'POST') {
            $postData = $request->getRequest();
            $dbConnexion = \DB::manager();

            $errors = validate($postData, 'categories');

            ## Enregistrement des données
            if (!$errors->any()) {
                $newCategorie = new Categories();
                # Appel automatique des setterse
                foreach ($postData as $key => $data) {
                    $setter = 'set' . ucfirst($key);
                    $newCategorie->$setter($data);
                }
                $newCategorie->forCreation();

                $dbConnexion->add($newCategorie);
                \Flash::set('success', 'The category has been created');
                redirection(getRouteUrl('categories.index'));
            } else {
                $errors = $errors->all();
            }
        }

        return view('dashboard/categories/create', [
            'errors' => $errors
        ]);
    }

    /**
     * @Method = POST
     * @param Request $request
     */
    public function delete(Request $request)
    {
        $postData = $request->getRequest();
        if (key_exists('id_category', $postData)) {
            $idCategory = (int) $postData['id_category'];
            if ($idCategory > 0) { # L'ID d'un categorie commence à 1. La valeur 0 Signifierait que $postData['id_user'] était du texte avant transtypage et non un nombre
                $dbConnexion = \DB::manager();
                if ($dbConnexion->get(Categories::class, $idCategory) !== null) { # La catégorie existe
                    $dbConnexion->delete(Categories::class, $idCategory);
                }
            }
        }
        \Flash::set('success', 'The category with id ' . $idCategory . ' has been deleted');
        redirection(getRouteUrl('categories.index'));
    }

    /**
     * @Method = GET / POST
     * @param int $id_category
     * @param Request $request
     * @return false|string
     */
    public function edit(int $id_category, Request $request)
    {
        $errors = null;
        $dbConnexion = \DB::manager();
        $category = $dbConnexion->get(Categories::class, $id_category);

        if ($request->getHttpMethod() == 'POST') {
            $categoryData = $request->getRequest();

            $errors = validate($categoryData, 'categories');

            ## Enregistrement des données
            if (!$errors->any()) {
                # Appel automatique des setters
                foreach ($categoryData as $key => $data) {
                    $setter = 'set' . ucfirst($key);
                    $category->$setter($data);
                }
                $category->forUpdate();

                $dbConnexion->update($category);
                \Flash::set('success', 'The category with id ' . $category->getId() . ' has been deleted');
                redirection(getRouteUrl('categories.index'));
            } else {
                $errors = $errors->all();
            }
        }
        return view('dashboard/categories/edit', [
            'errors' => $errors,
            'category' => $category
        ]);
    }
}
