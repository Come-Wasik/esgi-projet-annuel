<?php

namespace App\Controller\Dashboard;

use App\Entity\Article_have_tag;
use App\Entity\Articles;
use App\Entity\Categories;
use App\Entity\Tags;
use App\Entity\Users;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class ArticlesController
{
    /**
     * Affiches le pannel d'administration des articles
     *
     * Liste les article et rajoute la possibilité de les éditer, supprimer ou d'en ajouter
     *
     * @return false|string
     * @throws \Exception
     * @method GET
     */
    public function index()
    {
        $dbConnexion = \DB::manager();
        $articles = $dbConnexion->getAll(Articles::class);
        $categories = $dbConnexion->getAll(Categories::class);
        $tags = $dbConnexion->getAll(Tags::class);
        $relBetwArticleAndTag = $dbConnexion->getAll(Article_have_tag::class);
        $users = $dbConnexion->getAll(Users::class);

        return view('dashboard/articles/index', [
            'articles' => $articles,
            'categories' => $categories,
            'tags' => $tags,
            'relBetwArticleAndTag' => $relBetwArticleAndTag,
            'users' => $users,
        ]);
    }

    /**
     * Permet de créer un article
     *
     * Affiche un formulaire et permet à partir de celui-ci d'enregistrer un nouvel article
     *
     * @param Request $request
     * @return false|string la vue permettant de créer un nouvel article.
     * @throws \Exception
     * @method GET / POST
     */
    public function create(Request $request)
    {
        $errors = null;
        $dbConnexion = \DB::manager();

        if ($request->getHttpMethod() == 'POST') {
            $postData = $request->getRequest();

            $errors = validate($postData, 'articles');

            ## Enregistrement des données
            if (!$errors->any()) {
                ## Enregistrement des informations de l'article pour de l'hydratation
                $articleData = $postData;
                unset($articleData['tags_id']);

                ## Attribution de l'id utilisateur
                $articleData['author_id'] = \User::id();

                ## Saisie du status de publication de l'article en fonction de la valeur de la checkbox
                if (key_exists('published', $articleData)) {
                    $articleData['published'] = 1;
                } else {
                    $articleData['published'] = 0;
                }

                $newArticle = new Articles();
                # Appel automatique des setters
                foreach ($articleData as $key => $data) {
                    $setter = 'set' . ucfirst($key);
                    $newArticle->$setter($data);
                }
                $newArticle->forCreation();

                # Enregistrement de l'article
                $dbConnexion->add($newArticle);
                # Récupération de l'article créé (nous aurons alors son id en plus)
                $articleCreated = $dbConnexion->request('SELECT * FROM ' . DB_PREFIX . 'Articles WHERE name = :name', [
                    'name' => $newArticle->getName()
                ], 'class', [
                    'classname' => Articles::class
                ])[0];

                # Création et enregistrement d'une relation entre l'article précédemment créé et chacun de ses tags
                if (key_exists('tags_id', $postData)) {
                    foreach ($postData['tags_id'] as $tagId) {
                        $relationBetweenArticleAndTag = new Article_have_tag();
                        $relationBetweenArticleAndTag->setArticles_id($articleCreated->getId());
                        $relationBetweenArticleAndTag->setTags_id($tagId);
                        # Ajout de la relation en base de données
                        $dbConnexion->add($relationBetweenArticleAndTag);
                    }
                }

                \Flash::set('success', 'The article has been created');
                redirection(getRouteUrl('articles.index'));
            } else {
                $errors = $errors->all();
            }
        }

        $tags = $dbConnexion->getAll(Tags::class);
        $categories = $dbConnexion->getAll(Categories::class);
        $users = $dbConnexion->getAll(Users::class);

        return view('dashboard/articles/create', [
            'errors' => $errors,
            'tags' => $tags,
            'categories' => $categories,
            'users' => $users
        ]);
    }

    /**
     * Permet de supprimer un article
     *
     * L'article donné par id en POST est supprimé à cette addresse
     *
     * @param Request $request Requête Http
     * @throws \Exception
     * @method POST
     */
    public function delete(Request $request)
    {
        $postData = $request->getRequest();
        if (key_exists('id_article', $postData)) {
            $idArticle = (int) $postData['id_article'];
            if ($idArticle > 0) {
                $dbConnexion = \DB::manager();
                if (($dbConnexion->get(Articles::class, $idArticle)) !== null) {
                    $dbConnexion->delete(Articles::class, $idArticle);
                }
            }
        }
        \Flash::set('success', 'The article with id ' . $idArticle . ' has been deleted');
        redirection(getRouteUrl('articles.index'));
    }

    /**
     * Permet de modifier un article
     *
     * Fournit un formulaire de modification d'article et traite la modification
     *
     * @param Request $request
     * @param int $id_article
     * @return false|string la vue permettant d'éditer un nouvel.
     * @throws \Exception
     * @method GET / POST
     */
    public function edit(Request $request, int $id_article)
    {
        $errors = null;
        $dbConnexion = \DB::manager();
        $article = $dbConnexion->get(Articles::class, $id_article);

        if ($request->getHttpMethod() == 'POST') {
            $postData = $request->getRequest();

            $errors = validate($postData, 'articles');

            ## Enregistrement des données
            if (!$errors->any()) {
                ## Enregistrement des informations de l'article pour de l'hydratation
                $articleData = $postData;
                unset($articleData['tags_id']);

                ## Saisie du status de publication de l'article en fonction de la valeur de la checkbox
                if (key_exists('published', $articleData)) {
                    $articleData['published'] = 1;
                } else {
                    $articleData['published'] = 0;
                }

                # Appel automatique des setters
                foreach ($articleData as $key => $data) {
                    $setter = 'set' . ucfirst($key);
                    $article->$setter($data);
                }
                $article->forUpdate();

                ## Gestion des relations entre un article et un ou plusieurs tag(s). Entre autres, ajout et suppression
                if (key_exists('tags_id', $postData)) {
                    $actualRel = [];
                    $relBetwArticleAndTag = $dbConnexion->getAll(Article_have_tag::class);
                    foreach ($relBetwArticleAndTag as $rel) {
                        if ($article->getId() == $rel->getArticles_id()) {
                            $actualRel[] = $rel->getTags_id();
                        }
                    }

                    $relToAdd = array_diff($postData['tags_id'], $actualRel);
                    $relToDelete = array_diff($actualRel, $postData['tags_id']);

                    ## Ajout
                    foreach ($relToAdd as $tagId) {
                        $relationBetweenArticleAndTag = new Article_have_tag();
                        $relationBetweenArticleAndTag->setArticles_id($article->getId());
                        $relationBetweenArticleAndTag->setTags_id($tagId);
                        $dbConnexion->add($relationBetweenArticleAndTag);
                    }

                    ## Suppression
                    foreach ($relToDelete as $relToDel) {
                        foreach ($relBetwArticleAndTag as $existingRel) {
                            if (
                                $article->getId() == $existingRel->getArticles_id()
                                && $existingRel->getTags_id() == $relToDel
                            ) {
                                $dbConnexion->delete($existingRel);
                            }
                        }
                    }
                }

                # Enregistrement de l'article
                $dbConnexion->update($article);
                \Flash::set('success', 'The article with id ' . $article->getId() . ' has been updated');
                redirection(getRouteUrl('articles.index'));
            } else {
                $errors = $errors->all();
            }
        }

        $tags = $dbConnexion->getAll(Tags::class);
        $categories = $dbConnexion->getAll(Categories::class);
        $users = $dbConnexion->getAll(Users::class);
        $relBetwArticleAndTag = $dbConnexion->getAll(Article_have_tag::class);

        return view('dashboard/articles/edit', [
            'errors' => $errors,
            'tags' => $tags,
            'categories' => $categories,
            'users' => $users,
            'article' => $article,
            'relBetwArticleAndTag' => $relBetwArticleAndTag
        ]);
    }
}
