<?php

namespace App\Controller\Dashboard;

use App\Entity\Tags;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class TagsController
{
    /**
     * @Method = GET
     */
    public function index()
    {
        $dbConnexion = \DB::manager();
        $tags = $dbConnexion->getAll(Tags::class);

        return view('dashboard/tags/index', [
            'tags' => $tags
        ]);
    }

    /**
     * @Method = GET / POST
     */
    public function create(Request $request)
    {
        $errors = null;

        if ($request->getHttpMethod() == 'POST') {
            $postData = $request->getRequest();
            $dbConnexion = \DB::manager();

            $errors = validate($postData, 'tags');

            ## Enregistrement des données
            if (!$errors->any()) {
                $newTag = new Tags();
                # Appel automatique des setters
                foreach ($postData as $key => $data) {
                    $setter = 'set' . ucfirst($key);
                    $newTag->$setter($data);
                }
                $newTag->forCreation();

                $dbConnexion->add($newTag);
                \Flash::set('success', 'The tag has been created');
                redirection(getRouteUrl('tags.index'));
            } else {
                $errors = $errors->all();
            }
        }

        return view('dashboard/tags/create', [
            'errors' => $errors
        ]);
    }

    /**
     * @Method = POST
     */
    public function delete(Request $request)
    {
        $postData = $request->getRequest();
        if (key_exists('id_tag', $postData)) {
            $idTag = (int) $postData['id_tag'];
            if ($idTag > 0) { # L'ID d'un tag commence à 1. La valeur 0 Signifierait que $postData['id_user'] était du texte avant transtypage et non un nombre
                $dbConnexion = \DB::manager();
                if ($dbConnexion->get(Tags::class, $idTag) !== null) { # L'utilisateur existe
                    $dbConnexion->delete(Tags::class, $idTag);
                }
            }
        }
        \Flash::set('success', 'The tag with id ' . $postData['id_tag'] . ' has been deleted');
        redirection(getRouteUrl('tags.index'));
    }

    /**
     * @Method = GET / POST
     */
    public function edit(int $id_tag, Request $request)
    {
        $errors = null;
        $dbConnexion = \DB::manager();
        $tag = $dbConnexion->get(Tags::class, $id_tag);

        if ($request->getHttpMethod() == 'POST') {
            $postData = $request->getRequest();

            $errors = validate($postData, 'tags');
            $tagData = $postData;

            ## Enregistrement des données
            if (!$errors->any()) {
                # Appel automatique des setters
                foreach ($tagData as $key => $data) {
                    $setter = 'set' . ucfirst($key);
                    $tag->$setter($data);
                }
                $tag->forUpdate();

                $dbConnexion->update($tag);
                \Flash::set('success', 'The tag with id ' . $tag->getId() . ' has been updated');

                redirection(getRouteUrl('tags.index'));
            } else {
                $errors = $errors->all();
            }
        }

        return view('dashboard/tags/edit', [
            'errors' => $errors,
            'tag' => $tag
        ]);
    }
}
