<?php

namespace App\Controller\Dashboard;

use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class ParameterController
{
    public function edit()
    {
        $config = \Config::get('cms');
        $templates = [];
        $directory = view_path() . '/front/templates';

        foreach (scandir($directory) as $file) {
            if(is_file($directory . '/' . $file)) {
                $templates[] = $file;
            }
        }

        return view('dashboard/parameters/edit', [
            'config' => $config,
            'templates' => $templates
        ]);
    }

    public function update(Request $request)
    {
        $patchData =  $request->getRequest();
        unset($patchData['_method']);
        $errors = validate($patchData, 'config');

        if (!$errors->any()) {
            $config = \Config::get('cms');

            foreach ($patchData as $key => $value) {
                if (key_exists($key, $config)) {
                    $config[$key] = $value;
                }
            }

            $cms = ['cms' => $config];
            $fileContent = '<?php' . "\n" . 'return ' . var_export($cms, true) . ';';
            file_put_contents(data_storage_path() . '/cms.php', $fileContent);
            \Flash::set('success', 'Your data has been updated');
        } else {
            \Flash::set('error', 'Your data cannot be updated. ' . $errors->first()->title . ' : ' . $errors->first()->reason);
        }

        redirection(getRouteUrl('config.edit'));
    }
}
