<?php

namespace App\Controller\Dashboard;

use App\Entity\User_roles;
use App\Entity\Users;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use App\Middleware\Authentification;

class UsersController
{
    /**
     * @Method = GET
     */
    public function index(): string
    {
        $dbConnexion = \DB::manager();
        $users = $dbConnexion->getAll(Users::class);
        $roles = $dbConnexion->getAll(User_roles::class);

        return view('dashboard/users/index', [
            'users' => $users,
            'roles' => $roles
        ]);
    }

    /**
     * @Method = GET / POST
     */
    public function create(Request $request)
    {
        $errors = null; # Message affiché sur le formulaire en cas d'erreur
        $dbConnexion = \DB::manager();

        if ($request->getHttpMethod() == 'POST') {
            $postData = $request->getRequest();

            ### Vérification de l'existance des champs + protection XSS avec htmlentities + rejet d'un champ inconnu
            $errors = validate($postData, 'user_dashboard_add');

            ## Vérification des champs
            # Vérification du mot de passe
            if ($postData['password'] != $postData['passwordConfirm']) {
                $errors->addError('passwordConfirm', 'passwordDoNotMatch');
            }
            unset($postData['passwordConfirm']);

            ## Enregistrement des données
            if (!$errors->any()) {
                ## Saisie du status en fonction de la valeur de la checkbox
                if (key_exists('status', $postData)) {
                    $postData['status'] = 'enabled';
                } else {
                    $postData['status'] = 'disabled';
                }
                $postData['email_verified_at'] = getFormattedTimestamp();

                $auth = new Authentification();
                $auth->tryRegistration($postData);
                \Flash::set('success', 'The user has been created');
                redirection(getRouteUrl('users.index'));
            } else {
                $errors = $errors->all();
            }
        }

        $roles = $dbConnexion->getAll(User_roles::class);

        return view('dashboard/users/create', [
            'roles' => $roles,
            'errors' => $errors
        ]);
    }

    /**
     * @Method = POST
     */
    public function delete(Request $request)
    {
        $postData = $request->getRequest();
        if (key_exists('id_user', $postData)) {
            $idUser = (int) $postData['id_user'];
            if ($idUser > 0) { # L'ID d'un utilisateur commence à 1. La valeur 0 Signifierait que $postData['id_user'] était du texte avant transtypage et non un nombre
                $dbConnexion = \DB::manager();
                if ($dbConnexion->get(Users::class, $idUser) !== null) { # L'utilisateur existe
                    $dbConnexion->delete(Users::class, $idUser);
                }
            }
        }
        \Flash::set('success', 'The user with id ' . $postData['id_user'] . ' has been deleted');
        redirection(getRouteUrl('users.index'));
    }

    /**
     * @Method = GET / POST
     */
    public function edit(int $id_user, Request $request)
    {
        $errors = null; # Message affiché sur le formulaire en cas d'erreur
        $dbConnexion = \DB::manager();
        $user = $dbConnexion->get(Users::class, $id_user);

        if ($request->getHttpMethod() == 'POST') {
            $postData = $request->getRequest();

            ### Vérification de l'existance des champs + protection XSS avec htmlentities + rejet d'un champ inconnu
            $errors = validate($postData, 'user_dashboard_update');

            ## Vérification des champs
            # On choisit ou non d'avoir à modifier le mot de passe
            if (empty($postData['password']) && empty($postData['passwordConfirm'])) {
                unset($postData['password'], $postData['passwordConfirm']);
                # Vérification du mot de passe
            } elseif ($postData['password'] != $postData['passwordConfirm']) {
                $errors->addError('passwordConfirm', 'passwordDoNotMatch');
                unset($postData['passwordConfirm']);
            } else {
                # Même si les mots de passe sont pareils, on a plus besoin de passwordConfirm
                $postData['password'] = password_hash($postData['password'], PASSWORD_BCRYPT);
                unset($postData['passwordConfirm']);
            }

            ## Enregistrement des données
            if (!$errors->any()) {
                ## Saisie du status en fonction de la valeur de la checkbox
                if (key_exists('status', $postData)) {
                    $postData['status'] = 'enabled';
                } else {
                    $postData['status'] = 'disabled';
                }

                # Appel automatique des setters
                foreach ($postData as $key => $data) {
                    $setter = 'set' . ucfirst($key);
                    $user->$setter($data);
                }
                $user->forUpdate();

                $dbConnexion->update($user);
                \Flash::set('success', 'The user with id ' . $user->getId() . ' has been updated');
                redirection(getRouteUrl('users.index'));
            } else {
                $errors = $errors->all();
            }
        }

        $roles = $dbConnexion->getAll(User_roles::class);

        return view('dashboard/users/edit', [
            'user' => $user,
            'roles' => $roles,
            'errors' => $errors
        ]);
    }
}
