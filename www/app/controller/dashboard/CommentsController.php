<?php

namespace App\Controller\Dashboard;

use App\Entity\Articles;
use App\Entity\Comments;
use App\Entity\Users;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class CommentsController
{
    /**
     * Lister / Validation / Suppression
     */
    public function index()
    {
        $dbConnexion = \DB::manager();
        $comments = $dbConnexion->getAll(Comments::class);
        foreach ($comments as &$comment) {
            $article = $dbConnexion->get(Articles::class, $comment->getArticles_id());
            $comment->setArticle($article);
        }

        $users = $dbConnexion->getAll(Users::class);

        return view('dashboard/comments/index', [
            'comments' => $comments,
            'users' => $users
        ]);
    }

    public function validate(Request $request)
    {
        $postData = $request->getRequest();
        $commentId = $postData['id_comment'];
        $dbConnexion = \DB::manager();

        $comment = $dbConnexion->get(Comments::class, $commentId);
        $comment->validate();
        $dbConnexion->save($comment);
        \Flash::set('success', 'The comment with id ' . $commentId . ' has been validated');
        redirection(getRouteUrl('comments.index'));
    }

    public function invalidate(Request $request)
    {
        $postData = $request->getRequest();
        $commentId = $postData['id_comment'];
        $dbConnexion = \DB::manager();

        $comment = $dbConnexion->get(Comments::class, $commentId);
        $comment->invalidate();
        $dbConnexion->save($comment);
        \Flash::set('success', 'The comment with id ' . $commentId . ' has been invalidated');
        redirection(getRouteUrl('comments.index'));
    }

    public function delete(Request $request)
    {
        $postData = $request->getRequest();
        $commentId = $postData['id_comment'];
        $dbConnexion = \DB::manager();

        $comment = $dbConnexion->get(Comments::class, $commentId);
        $dbConnexion->delete($comment);
        \Flash::set('success', 'The comment with id ' . $commentId . ' has been deleted');
        redirection(getRouteUrl('comments.index'));
    }
}
