<?php

namespace App\Controller\Dashboard;

class DashboardController
{
    public function index()
    {
        $dbConnexion = \DB::manager();

        return view('dashboard/index', [
            'articleCount' => $dbConnexion->request('SELECT COUNT(*) as count FROM ' . DB_PREFIX . 'articles')[0]['count'],
            'pageCount' => $dbConnexion->request('SELECT COUNT(*) as count FROM ' . DB_PREFIX . 'pages')[0]['count'],
            'userCount' => $dbConnexion->request('SELECT COUNT(*) as count FROM ' . DB_PREFIX . 'users')[0]['count'],
        ]);
    }
}
