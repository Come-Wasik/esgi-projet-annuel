<?php

namespace App\Middleware;

use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class Firewall
{
    public function analyse(Request $request)
    {
        $uri = rtrim($request->getUri(), '/');

        $rules = [
            'dashboard_home' => [
                'path' => [
                    \Config::get('cms.dashboard_path')
                ],
                'rolesAuthorized' => [
                    'Administrator',
                    'Redactor',
                    'Moderator'
                ],
                'userIdAuthorized' => [
                    // '1' # Root ou Bestmaker
                ],
                'redirectIfNotAuthorized' => getRouteUrl('app.form.connexion')
            ],
            'dashboard_comments' => [
                'path' => [
                    \Config::get('cms.dashboard_path') . '/comments'
                ],
                'rolesAuthorized' => [
                    'Administrator',
                    'Moderator'
                ],
                'userIdAuthorized' => [],
                'redirectIfNotAuthorized' => \Config::get('cms.dashboard_path')
            ],
            'dashboard_admin_only_access' => [
                'path' => [
                    \Config::get('cms.dashboard_path') . '/pages',
                    \Config::get('cms.dashboard_path') . '/pages/*',
                    \Config::get('cms.dashboard_path') . '/users',
                    \Config::get('cms.dashboard_path') . '/users/*',
                    \Config::get('cms.dashboard_path') . '/config',
                ],
                'rolesAuthorized' => [
                    'Administrator',
                ],
                'userIdAuthorized' => [],
                'redirectIfNotAuthorized' => \Config::get('cms.dashboard_path'),
            ],
            'dashboard_articles_tags_categories' => [
                'path' => [
                    \Config::get('cms.dashboard_path') . '/articles',
                    \Config::get('cms.dashboard_path') . '/articles/*',
                    \Config::get('cms.dashboard_path') . '/tags',
                    \Config::get('cms.dashboard_path') . '/tags/*',
                    \Config::get('cms.dashboard_path') . '/categories',
                    \Config::get('cms.dashboard_path') . '/categories/*'
                ],
                'rolesAuthorized' => [
                    'Administrator',
                    'Redactor'
                ],
                'userIdAuthorized' => [],
                'redirectIfNotAuthorized' => \Config::get('cms.dashboard_path'),
            ],
        ];

        ### Action !!!
        foreach ($rules as $rule) {
            foreach ($rule['path'] as $curPath) {
                # Clean uri
                $curPath = rtrim($curPath, '/');

                ## Joker test
                # Test if the position of the joker (*) is at the end of the string
                if (
                    strpos($curPath, '*', -1) !== false
                ) {
                    $curPath = rtrim($curPath, '*');

                    # Position of needle test
                    if (strpos($uri, $curPath) === 0) {
                        if (
                            !\User::isConnected()
                            || !in_array(\User::role(), $rule['rolesAuthorized'])
                            && !in_array(\User::id(), $rule['userIdAuthorized'])
                        ) {
                            redirection($rule['redirectIfNotAuthorized']);
                        }
                    }
                    ## Equality test
                } elseif ($uri == $curPath) {
                    if (
                        !\User::isConnected()
                        || !in_array(\User::role(), $rule['rolesAuthorized'])
                        && !in_array(\User::id(), $rule['userIdAuthorized'])
                    ) {
                        redirection($rule['redirectIfNotAuthorized']);
                    }
                }
            }
        }
    }
}
