<?php

namespace App\Middleware;

use App\Entity\Users;
use DB;
use Flash;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use Framework\Architecture\Web\Validator\ValidatorErrorContainer;
use NewRoute;
use Session;
use Str;
use User;

class Authentification
{
    private $userClass;
    private $userTable;
    private $roleClass;
    private $roleTable;
    private $databaseManager;

    public function __construct()
    {
        $securityConfiguration = \Config::get('security');

        if ($securityConfiguration['moduleEnabled']) {
            ### Variable definition ###
            $this->userClass = $securityConfiguration['userEntity'];
            $this->userTable = $this->getOnlyClassname($this->userClass);
            $this->roleClass = $securityConfiguration['roleEntity'];
            $this->roleTable = $this->getOnlyClassname($this->roleClass);
            $this->databaseManager = \DB::manager();
        }
    }

    public function addRoutes()
    {
        NewRoute::get('app.form.connexion', '/login', function () {
            $errors = [];

            if (\Session::get('form_error')) {
                $errors = \Session::get('form_error');
                Session::delete('form_error');
            }

            return view(\Config::get('security.connexionData.view'), [
                'errors' => $errors
            ]);
        });
        NewRoute::get('app.form.inscription', '/register', function () {
            $errors = [];
            if (\Session::get('form_error')) {
                $errors = \Session::get('form_error');
                Session::delete('form_error');
            }
            return view(\Config::get('security.registrationData.view'), [
                'errors' => $errors
            ]);
        });
        NewRoute::post('app.process.connexion', '/login', function () {
        });
        NewRoute::post('app.process.inscription', '/register', function () {
        });

        NewRoute::get('app.process.deconnexion', \Config::get('security.deconnexionData.route'), function () {
        });

        /**
         * Confirm account
         */

        NewRoute::get('app.process.email-confirmation', '/confirm-email', function (Request $request) {
            $token = $request->getQuery()['token'] ?? null;

            $dbManager = \DB::manager();
            $user = $dbManager->request('SELECT * FROM ' . DB_PREFIX . 'users WHERE email_token = :email_token', ['email_token' => $token], 'class', [
                'classname' => $this->userClass
            ]);

            if ($user !== NULL && !empty($user)) {
                $user = $user[0];
                $user->setEmail_token(null);
                $user->setEmail_verified_at(getFormattedTimestamp());

                $dbManager->save($user);
                \Flash::set('success', 'Your account has been validated. You can log in');
            }

            redirection(getRouteUrl('app.form.connexion'));
        });

        /**
         * Reset password
         */
        NewRoute::get(
            'app.reset-password.send-email.form',
            \Config::get('security.reset-password.send-email.route'),
            function () {
                if (User::isConnected()) {
                    redirection(getRouteUrl('home'));
                }
                $errors = [];
                if (\Session::has('form_error')) {
                    $errors = \Session::get('form_error');
                    Session::delete('form_error');
                }
                return view(\Config::get('security.reset-password.send-email.view'), [
                    'errors' => $errors
                ]);
            }
        );

        NewRoute::post(
            'app.reset-password.send-email.process',
            \Config::get('security.reset-password.send-email.route'),
            function (Request $request) {
                $postData = $request->getRequest();
                $errors = validate($postData, 'reset-password_email');

                # Test errors by validator
                if ($errors->any()) {
                    Session::set('form_error', $errors->all());
                    redirection(getRouteUrl('app.reset-password.send-email.form'));
                }

                $email = Str::xssFilter($postData['email']);
                $token = bin2hex(random_bytes(16));

                $user = DB::manager()
                    ->request(
                        'SELECT * FROM ' . DB_PREFIX . 'users WHERE email = :email',
                        ['email' => $email],
                        'class',
                        ['classname' => Users::class]
                    );

                ## Test if user exist in database
                if (empty($user)) {
                    $errors->addError('email', 'emailNotRegistered', ['email' => $email]);
                    Session::set('form_error', $errors->all());
                    redirection(getRouteUrl('app.reset-password.send-email.form'));
                }
                $user = $user[0];
                $user->setEmail_token($token);
                DB::manager()->update($user);

                \Mailer::sendMail(function ($mail) use ($email, $token) {
                    //Recipients
                    $mail->setFrom('nepasrepondre@cms-blog.com', 'CMS Blog Robot');
                    $mail->addAddress($email, $email);

                    $websiteUrl = rtrim(WEBSITE_URL, '/');
                    $tokenUrl = $websiteUrl . getRouteUrl('app.reset-password.send-password.form') . '?token=' . $token;
                    // Content
                    $mail->isHTML(true);
                    $mail->Subject = 'Changement de mot de passe sur ' . $websiteUrl;
                    $mail->Body    = '<a href="' . $tokenUrl . '">Cliquez ici pour reinitialiser votre mot de passe</a>';
                    $mail->AltBody = 'Copiez-collez ce lien dans votre navigateur pour reinitialiser votre mot de passe : ' . $tokenUrl;
                    return $mail;
                });

                \FLash::set('success', 'Votre demande de réinitialisation a bien été envoyé. Veuillez vérifier votre voite mail "' . $email . '"');

                # Redirection de l'utilisateur vers la page d'inscription "AfterProcessing" #
                redirection(getRouteUrl('app.reset-password.send-email.form'));
            }
        );

        NewRoute::get(
            'app.reset-password.send-password.form',
            \Config::get('security.reset-password.send-password.route'),
            function (Request $request) {
                if (User::isConnected()) {
                    redirection(getRouteUrl('home'));
                }
                $token = $request->getQuery()['token'] ?? null;
                Session::set('email_token', $token);
                $user = DB::manager()->request('SELECT * FROM ' . DB_PREFIX . 'users WHERE email_token = :email_token', ['email_token' => $token], 'class', [
                    'classname' => $this->userClass
                ]);

                $errors = [];
                if (\Session::has('form_error')) {
                    $errors = \Session::get('form_error');
                    Session::delete('form_error');
                }

                ## Test if user exist in database
                if (empty($user)) {
                    redirection(getRouteUrl('app.reset-password.send-email.form'));
                }
                $user = $user[0];

                return view(\Config::get('security.reset-password.send-password.view'), [
                    'errors' => $errors
                ]);
            }
        );

        NewRoute::post(
            'app.reset-password.send-password.process',
            \Config::get('security.reset-password.send-password.route'),
            function (Request $request) {
                ## A token must be stocked in session
                if (!Session::has('email_token')) {
                    redirection(getRouteUrl('app.reset-password.send-email.form'));
                }

                ## Get request data
                $postData = $request->getRequest();
                $errors = validate($postData, 'reset-password_password');

                ## Error testing
                if ($errors->any()) {
                    Session::set('form_error', $errors->all());
                    redirection(getRouteUrl('app.reset-password.send-password.form'));
                }

                if ($postData['password'] != $postData['passwordConfirm']) {
                    $errors->addError('passwordConfirm', 'passwordDoNotMatch');
                    redirection(getRouteUrl('app.reset-password.send-password.form'));
                }

                ## Get token
                $token = Session::get('email_token');
                Session::delete('email_token');

                $user = DB::manager()->request('SELECT * FROM ' . DB_PREFIX . 'users WHERE email_token = :email_token', ['email_token' => $token], 'class', [
                    'classname' => $this->userClass
                ]);
                $user = $user[0];

                $password = password_hash($postData['password'], PASSWORD_BCRYPT);
                $user->setPassword($password);
                $user->setEmail_token('');
                DB::manager()->update($user);

                \FLash::set('success', 'Votre mot de passe a bien été réinitialisé');
                redirection(getRouteUrl('app.form.connexion'));
            }
        );
    }

    public function startSystem(Request $request): void
    {
        if (\Config::get('security.moduleEnabled')) {
            ### Processing ###
            ## Connexion ##
            if ($request->getUri() == \Config::get('security.connexionData.route')) {
                if ($this->userIsConnnected()) {
                    redirection(\Config::get('security.connexionData.redirectionAfterProcessing'));
                } else {
                    if ($request->getHttpMethod() == \Config::get('security.connexionData.httpMethod')) {
                        ## Getting all user input data ##
                        $currentHttpMethod = \Config::get('security.connexionData.httpMethod');
                        if ($currentHttpMethod == 'GET') {
                            $userInput = $request->getQuery();
                        } elseif ($currentHttpMethod == 'POST') {
                            $userInput = $request->getRequest();
                        }

                        # Primary tests (data existing, expected amount of data, good types) #
                        $errors = validate($userInput, 'user_login');
                        if ($errors->any()) {
                            \Session::set('form_error', $errors->all());
                            redirection(\Config::get('security.connexionData.route'));
                        }

                        # Obtaining all data with XSS filtering #
                        $this->tryConnexion([
                            'username' => Str::xssFilter($userInput['email']),
                            'email' => Str::xssFilter($userInput['email']),
                            'password' => Str::xssFilter($userInput['password'])
                        ]);

                        # Suppression du tableau d'erreur
                        \Session::set('form_error', null);

                        # Redirection de l'utilisateur vers la page deconnexion "AfterProcessing" #
                        redirection(\Config::get('security.connexionData.redirectionAfterProcessing'));
                    }
                }
            } elseif ($request->getUri() == \Config::get('security.registrationData.route')) {
                ## Registration ##
                if ($this->userIsConnnected()) {
                    redirection(\Config::get('security.registrationData.redirectionAfterProcessing'));
                } else {
                    if ($request->getHttpMethod() == \Config::get('security.registrationData.httpMethod')) {
                        ## Getting all user input data ##
                        $currentHttpMethod = \Config::get('security.registrationData.httpMethod');
                        if ($currentHttpMethod == 'GET') {
                            $userInput = $request->getQuery();
                        } elseif ($currentHttpMethod == 'POST') {
                            $userInput = $request->getRequest();
                        }

                        # Primary tests (data existing, expected amount of data, good types) #
                        $errors = validate($userInput, 'user_registration');
                        if ($errors->any()) {
                            \Session::set('form_error', $errors->all());
                            redirection(\Config::get('security.registrationData.route'));
                        }

                        ## Vérification supplémentaire des champs
                        # Vérification du mot de passe
                        if ($userInput['password'] != $userInput['passwordConfirm']) {
                            \Session::set('form_error', [
                                'passwordConfirm' => 'Les mots de passe ne correspondent pas'
                            ]);
                            redirection(\Config::get('security.registrationData.route'));
                        }
                        unset($userInput['passwordConfirm']);

                        ## Saisie du rôle
                        $databaseManager = $this->databaseManager;
                        $roleTable = $this->roleTable;
                        $userInput['role_id'] = $databaseManager->request('SELECT id FROM ' . DB_PREFIX . $roleTable . " WHERE name = 'Simple user'")[0]['id'];

                        ## Saisie du status
                        $userInput['status'] = 'enabled';
                        $userInput['email_token'] = bin2hex(random_bytes(16));

                        # Try to register in database #
                        $this->tryRegistration([
                            'username' => Str::xssFilter($userInput['username']),
                            'password' => Str::xssFilter($userInput['password']),
                            'email' => Str::xssFilter($userInput['email']),
                            'firstname' => Str::xssFilter($userInput['firstname']),
                            'lastname' => Str::xssFilter($userInput['lastname']),
                            'role_id' => Str::xssFilter($userInput['role_id']),
                            'status' => Str::xssFilter($userInput['status']),
                            'email_token' => $userInput['email_token']
                        ]);

                        \Mailer::sendMail(function ($mail) use ($userInput) {
                            //Recipients
                            $mail->setFrom('nepasrepondre@cms-blog.com', 'CMS Blog Robot');
                            $mail->addAddress(Str::xssFilter($userInput['email']), Str::xssFilter($userInput['username']));


                            $websiteUrl = rtrim(WEBSITE_URL, '/');
                            $tokenUrl = $websiteUrl . getRouteUrl('app.process.email-confirmation') . '?token=' . $userInput['email_token'];
                            // Content
                            $mail->isHTML(true);
                            $mail->Subject = 'Validation de votre compte sur ' . $websiteUrl;
                            $mail->Body    = '<a href="' . $tokenUrl . '">Cliquez ici pour valider votre compte</a>';
                            $mail->AltBody = 'Copiez-collez ce lien dans votre navigateur pour valider votre compte : ' . $tokenUrl;
                            return $mail;
                        });

                        \FLash::set('success', 'Bravo, votre compte été créé ! Vous devez malgré tout le valider afin de vous connecter. Veuillez vérifier votre boîte mail "' . Str::xssFilter($userInput['email']) . '"');

                        # Suppression du tableau d'erreur
                        \Session::set('form_error', null);

                        # Redirection de l'utilisateur vers la page d'inscription "AfterProcessing" #
                        redirection(\Config::get('security.registrationData.redirectionAfterProcessing'));
                    }
                }
            } elseif ($request->getUri() == \Config::get('security.deconnexionData.route')) {
                ## Deconnexion ##
                if ($this->userIsConnnected()) {
                    $this->deconnexion();
                }
                # Redirection de l'utilisateur vers la page de déconnexion "AfterProcessing" #
                redirection(\Config::get('security.deconnexionData.redirectionAfterProcessing'));
            } else {
                ## Renouvellement de la connexion ##
                if ($this->userIsConnnected()) {
                    $this->tryReconnexion();
                }
            }
        }
    }

    public function userIsConnnected(): bool
    {
        ### Si le token existe (dans la session) ###
        if (\Session::get('session_token')) {
            return true;
        }
        return false;
    }

    private function tryConnexion(array $credentials): void
    {
        ### Requétage de l'utilisateur ###
        $userClass = $this->userClass;
        $userTable = $this->userTable;
        $roleTable = $this->roleTable;
        $databaseManager = $this->databaseManager;

        $password = $credentials['password'];
        unset($credentials['password']);

        $user = $databaseManager->request(
            'SELECT * FROM ' . DB_PREFIX . $userTable . ' WHERE username = :username OR email = :email',
            $credentials,
            'class',
            [
                'classname' => $userClass
            ]
        );

        if (is_array($user) && !empty($user)) {
            $user = $user[array_key_first($user)];
        }

        if (!$user) {
            $error = new ValidatorErrorContainer();
            $error->addError('email', 'userNotExist');
            \Session::set('form_error', $error->all());
            redirection(\Config::get('security.connexionData.route'));
        }

        if ($user->getStatus() != 'enabled') {
            $error = new ValidatorErrorContainer();
            $error->addError('state', 'accountDisabled');
            \Session::set('form_error', $error->all());
            redirection(\Config::get('security.connexionData.route'));
        }

        if (!$user->mailIsVerified()) {
            $error = new ValidatorErrorContainer();
            $error->addError('state', 'emailNotVerified');
            \Session::set('form_error', $error->all());
            redirection(\Config::get('security.connexionData.route'));
        }

        if (!password_verify($password, $user->getPassword())) {
            $error = new ValidatorErrorContainer();
            $error->addError('password', 'passwordIncorrect');
            \Session::set('form_error', $error->all());
            redirection(\Config::get('security.connexionData.route'));
        }

        ### Token generation ###
        $userRole = $databaseManager->request('SELECT name FROM ' . DB_PREFIX . $roleTable . ' WHERE id = :id', ['id' => $user->getRole_id()])[0]['name'];

        $token = $this->generateToken([
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'role' => $userRole,
        ]);

        ### Integrate token in session ###
        \Session::set('session_token', $token);

        ### Register token and connexion date in database ###
        $user->setSession_token(addslashes($token));
        $user->setLast_connexion(getFormattedTimestamp());
        $databaseManager->update($user);
    }

    public function tryRegistration(array $userData): void
    {
        ### Requétage de l'utilisateur ###
        $userClass = $this->userClass;
        $databaseManager = $this->databaseManager;

        ### Chiffrement du mot de passe ###
        $userData['password'] = password_hash($userData['password'], PASSWORD_BCRYPT);

        $newUser = new $userClass();
        # Appel automatique des setters
        foreach ($userData as $key => $data) {
            $setter = 'set' . ucfirst($key);
            $newUser->$setter($data);
        }
        $newUser->forCreation();

        $databaseManager->add($newUser);
    }

    private function tryReconnexion(): void
    {
        $roleTable = $this->roleTable;

        ### On récupère le token ###
        $token = \Session::get('session_token');
        $tokenDecoded = \json_decode($token, 1);

        ### On vérifie la véracité de la signature ###
        $recreatedSign = hash('sha512', \json_encode($tokenDecoded['header']) . '.' . \json_encode($tokenDecoded['payload']));
        if ($tokenDecoded['sign'] == $recreatedSign) {
            ### On vérifie que le token est bien le même que dans l'objet utilisateur ###
            $userClass = $this->userClass;
            $databaseManager = $this->databaseManager;
            $user = $databaseManager->get($userClass, $tokenDecoded['payload']['id']);

            $tokenInDb = stripcslashes($user->getSession_token());

            if (!$user) {
                \Session::set('form_error', [
                    'userNotFound' => 'User either dont\'t exists or is disabled'
                ]);
                redirection(\Config::get('security.connexionData.route'));
            }

            if ($user && $tokenInDb == $token) {
                ### Token generation ###
                $userRole = $databaseManager->request('SELECT name FROM ' . DB_PREFIX . $roleTable . ' WHERE id = :id', ['id' => $user->getRole_id()])[0]['name'];

                $newToken = $this->generateToken([
                    'id' => $user->getId(),
                    'username' => $user->getUsername(),
                    'role' => $userRole,
                ]);

                ### Integrate token in session ###
                \Session::set('session_token', $newToken);

                ### Register token and connexion date in database ###
                $user->setSession_token(addslashes($newToken));
                $user->setLast_connexion(getFormattedTimestamp());
                $databaseManager->update($user);
            } else {
                ### On déconnecte l'utilisateur ###
                \Session::stop();
            }
        } else {
            ### On déconnecte l'utilisateur ###
            \Session::stop();
        }
    }

    private function deconnexion(): void
    {
        ## Stop session ##
        \Session::stop();
    }

    private function generateToken(array $user): string
    {
        ### Création d'un tableau comprenant les informations primordiales ###
        $tokenData = [
            'header' => [
                'typ' => 'JWT',
                'algo' => 'FALSE'
            ],
            'payload' => [
                'id' => $user['id'],
                'username' => $user['username'],
                'role' => $user['role'],
                'horodatage' => getTimestamp(),
                'TTL' => getTimestamp() + (60 * 5),
            ],
        ];

        ### Génération de la signature du token ###
        $tokenData['sign'] = hash('sha512', \json_encode($tokenData['header']) . '.' . \json_encode($tokenData['payload']));

        ### Renvoi du token préparé
        return \json_encode($tokenData);
    }

    // Thanks to codeFareith : https://coderwall.com/p/cpxxxw/php-get-class-name-without-namespace
    private function getOnlyClassname(string $className): string
    {
        return substr($className, strrpos($className, '\\') + 1);
    }
}
