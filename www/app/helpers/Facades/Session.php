<?php

use Framework\Architecture\Web\Component\Session as ComponentSession;

class Session
{
    public static function start(): void
    {
        $sessionManager = new ComponentSession();
        $sessionManager->session_start();
    }

    public static function stop(): void
    {
        $sessionManager = new ComponentSession();
        $sessionManager->session_stop();
    }

    public static function get(string $dataPath)
    {
        $sessionManager = new ComponentSession();
        return $sessionManager->get($dataPath);
    }

    public static function has(string $dataPath)
    {
        $sessionManager = new ComponentSession();
        return $sessionManager->has($dataPath);
    }

    public static function set(string $dataPath, $dataContent): void
    {
        $sessionManager = new ComponentSession();
        $sessionManager->set($dataPath, $dataContent);
    }

    public static function delete(string $dataPath)
    {
        $sessionManager = new ComponentSession();
        return $sessionManager->delete($dataPath);
    }
}
