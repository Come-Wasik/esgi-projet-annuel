<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception as MailException;

class Mailer
{
    public static function sendMail(callable $callback)
    {
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            //Server settings
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;  // Enable verbose debug output
            $mail->isSMTP();                        // Send using SMTP
            $mail->Host       = MAIL_SMTP_SERVER;   // Set the SMTP server to send through
            $mail->SMTPAuth   = true;               // Enable SMTP authentication
            $mail->Username   = MAIL_USERNAME;      // SMTP username
            $mail->Password   = MAIL_PASSWORD;      // SMTP password
            $mail->Port       = MAIL_PORT;          // TCP port to connect to, use 465

            $mail = $callback($mail);

            $mail->send();
        } catch (MailException $e) {
            throw new Exception("Erreur d\'envoi de mail : {$mail->ErrorInfo}", 500);
        }
    }
}
