<?php

class Str
{
    public static function slug(string $content)
    {
        $content = strtolower($content);
        $content = str_replace(' ', '-', $content);
        $content = str_replace('_', '-', $content);
        return $content;
    }

    public static function xssFilter(string $value)
    {
        return htmlentities($value);
    }
}
