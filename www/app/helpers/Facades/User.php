<?php

use App\Middleware\Authentification;

class User
{
    public static function isConnected(): bool
    {
        $authManager = new Authentification();
        return $authManager->userIsConnnected();
    }

    public static function role(): ?string
    {
        $token = \Session::get('session_token');
        $tokenDecoded = \json_decode($token, 1);

        return $tokenDecoded['payload']['role'];
    }

    public static function id(): ?int
    {
        $token = \Session::get('session_token');
        $tokenDecoded = \json_decode($token, 1);

        return $tokenDecoded['payload']['id'];
    }

    public static function name(): ?string
    {
        $token = \Session::get('session_token');
        $tokenDecoded = \json_decode($token, 1);

        ## Requêtage des informations personnelles de l'utilisateur en fonction de son id
        $dbConnexion = \DB::manager();
        $table = substr(Config::get('security.userEntity'), strrpos(Config::get('security.userEntity'), '\\') + 1,);
        $response = $dbConnexion->request('SELECT username, firstname, lastname FROM ' . DB_PREFIX . $table . ' WHERE id = :id', ['id' => $tokenDecoded['payload']['id']])[0];

        ## Gestion de l'affichage en fonction des informations personnelles dde l'utilisateur
        if (
            key_exists('firstname', $response) && !empty($response['firstname'])
            && key_exists('lastname', $response) && !empty($response['lastname'])
        ) {
            $name = $response['firstname'] . ' ' . $response['lastname'];
        } elseif (
            key_exists('username', $response) && !empty($response['username'])
            && key_exists('lastname', $response) && !empty($response['lastname'])
        ) {
            $name = $response['username'] . ' ' . $response['lastname'];
        } else {
            $name = $response['username'];
        }

        return $name;
    }
}
