<?php

use Framework\Architecture\All\Component\Database\DatabaseManager;

class DB
{
    public static function manager(): DatabaseManager
    {
        return new DatabaseManager(
            \Config::get('db_driver'),
            \Config::get('db_host'),
            \Config::get('db_name'),
            \Config::get('db_user'),
            \Config::get('db_password')
        );
    }
}
