<?php

use Framework\Architecture\All\Component\Backpack\Config as BackpackConfig;

class Config
{
    public static function get(string $dataPath, string $delimiter = '.')
    {
        return BackpackConfig::get($dataPath, $delimiter);
    }

    public static function set(string $dataPath, $dataContent, string $delimiter = '.')
    {
        BackpackConfig::set($dataPath, $dataContent, $delimiter);
    }
}
