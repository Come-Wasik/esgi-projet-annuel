<?php

use Framework\Architecture\Web\Component\Flash as ComponentFlash;

class Flash
{
    public static function set(string $type, string $message)
    {
        $flash = new ComponentFlash();
        $flash->assign($type, $message);
    }

    public static function has(string $type)
    {
        $flash = new ComponentFlash();
        return $flash->has($type);
    }

    public static function get(string $type)
    {
        $flash = new ComponentFlash();
        return $flash->get($type);
        
    }
}