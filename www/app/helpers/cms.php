<?php

use App\Entity\Pages;

if (!function_exists('getNavBarContent')) {
    function getNavBarContent()
    {
        $pagesInNavbar = [];
        foreach (DB::manager()->getAll(Pages::class) as $page) {
            if (
                $page->getSlug() != '/'
                && $page->isPublished()
            ) {
                $pagesInNavbar[] = $page;
            }
        }
        return $pagesInNavbar;
    }
}
