<?php

use Framework\Architecture\All\Component\Backpack\Config as ConfigBackpack;
use Framework\Architecture\All\Component\Backpack\Package as PackageBackpack;
use Framework\Architecture\Web\View\ViewManager;
use Framework\Architecture\Web\CoreMaker\Component\Http\Response;

if (!function_exists('response')) {
    function response(...$data)
    {
        return new Response(...$data);
    }
}

if (!function_exists('viewExists')) {
    function viewExists($viewPath)
    {
        return file_exists(ConfigBackpack::get('view_path') . '/' . $viewPath . '.php');
    }
}

if (!function_exists('view')) {
    function view($viewName, $variables = [])
    {
        return (new ViewManager())->show($viewName, $variables);
    }
}

if (!function_exists('redirection')) {
    function redirection($redirectionPath)
    {
        header('Location: ' . rtrim(WEBSITE_URL, '/') . '/' . ltrim($redirectionPath, '/'));
        exit();
    }
}
