<?php

if (!function_exists('get_method_args')) {
    function get_method_args($controller, $method)
    {
        $methodArgs = [];

        $refclass = new ReflectionClass($controller);
        $refMethod = $refclass->getMethod($method);
        $refArgs = $refMethod->getParameters();

        foreach ($refArgs as $refArg) {
            $methodArgs[$refArg->getName()] = [
                'name' => $refArg->getName(),
                'type' => ($refArg->getType() !== null ? $refArg->getType()->__tostring() : null),
            ];
        }
        return $methodArgs;
    }
}

if (!function_exists('get_callback_args')) {
    function get_callback_args($callback)
    {
        $callbackArgs = [];

        $refMethod = new ReflectionFunction($callback);
        $refArgs = $refMethod->getParameters();

        foreach ($refArgs as $refArg) {
            $callbackArgs[$refArg->getName()] = [
                'name' => $refArg->getName(),
                'type' => ($refArg->getType() !== null ? $refArg->getType()->__tostring() : null),
            ];
        }
        return $callbackArgs;
    }
}

if (!function_exists('array_find')) {
    function array_find(array $dataContainer, string $path, string $delimiter = '.')
    {
        $decomposedPath = explode($delimiter, $path);
        $key = array_shift($decomposedPath);
        $newPath = implode($delimiter, $decomposedPath);
        $newDimension = $dataContainer[$key];

        if (count($decomposedPath) >= 1) {
            return array_find($newDimension, $newPath);
        } else {
            return $newDimension;
        }
    }
}

if (!function_exists('array_change')) {
    function array_change(array &$dataContainer, string $path, $newValue, string $delimiter = '.')
    {
        $decomposedPath = explode($delimiter, $path);
        $key = array_shift($decomposedPath);
        $newPath = implode($delimiter, $decomposedPath);
        
        if (count($decomposedPath) >= 1) {
            $newDimension = &$dataContainer[$key];
            array_change($newDimension, $newPath, $newValue);
        } else {
            $dataContainer[$key] = $newValue;
        }
    }
}