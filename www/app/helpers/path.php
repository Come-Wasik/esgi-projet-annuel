<?php

use Framework\Architecture\All\Component\Backpack\Config as ConfigBackpack;

if (!function_exists('controller_path')) {
    function controller_path()
    {
        return ConfigBackpack::get('controller_path');
    }
}

if (!function_exists('route_path')) {
    function route_path()
    {
        return ConfigBackpack::get('route_path');
    }
}

if (!function_exists('view_path')) {
    function view_path()
    {
        return ConfigBackpack::get('view_path');
    }
}

if (!function_exists('validator_path')) {
    function validator_path()
    {
        return ConfigBackpack::get('validator_path');
    }
}

if (!function_exists('app_path')) {
    function app_path()
    {
        return ConfigBackpack::get('app_path');
    }
}

if (!function_exists('entity_path')) {
    function entity_path()
    {
        return ConfigBackpack::get('entity_path');
    }
}

if (!function_exists('config_path')) {
    function config_path()
    {
        return ConfigBackpack::get('config_path');
    }
}

if (!function_exists('data_storage_path')) {
    function data_storage_path()
    {
        return ConfigBackpack::get('data_storage_path');
    }
}

if (!function_exists('component_path')) {
    function component_path()
    {
        return ConfigBackpack::get('component_path');
    }
}

if (!function_exists('test_path')) {
    function test_path()
    {
        return ConfigBackpack::get('test_path');
    }
}

if (!function_exists('storage_path')) {
    function storage_path(string $path = '')
    {
        return ConfigBackpack::get('storage_path') . (!empty($path) ? '/' . $path : '');
    }
}
