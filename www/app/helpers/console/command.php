<?php

use Framework\Architecture\Console\Router\RouteManager;
use Framework\Architecture\Console\Router\Route;

class Command
{
    public static function add(string $regexToRespect, $action)
    {
        $manager = new RouteManager();
        $manager->addCommand(new Route([
            'regex' => $regexToRespect,
            'action' => $action
        ]));
    }

    public static function write(string $content, string $state = 'normal')
    {
        switch ($state) {
            case 'success':
                $color = '[92m';
                break;
            case 'error':
                $color = '[31m';
                break;
            case 'info':
                $color = '[93m';
                break;
            case 'normal':
            default:
                $color = '[39m';
                break;
        }

        echo "\e" . $color . '[' . date('Y-m-d i:m:s') . '] ' . $content . "\n";
    }
}
