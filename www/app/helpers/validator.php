<?php

use Framework\Architecture\Web\Validator\Validator;
use Framework\Architecture\Web\Validator\ValidatorErrorContainer;

function validate(array $requestData, $meanToValidate): ValidatorErrorContainer
{
    $validator = new Validator();
    return $validator->check($requestData, $meanToValidate);
}
