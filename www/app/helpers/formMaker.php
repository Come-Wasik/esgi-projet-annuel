<?php

use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use Packages\FormMaker\Builder\Form;
use Packages\FormMaker\Builder\Component\Field\Input;
use Packages\FormMaker\Builder\Component\Field\OptionList;
use Packages\FormMaker\Builder\Component\Field\OptionElement;
use Packages\FormMaker\Builder\Component\Field\TextArea;
use Packages\FormMaker\Builder\Component\Field\Button;
use Packages\FormMaker\Validator\Validator;

class FormBuilder
{
    public function create(array $formData = null, array $fields = null)
    {
        if ($fields) {
            foreach ($fields as $fieldName => &$fieldData) {
                if (!key_exists('helperType', $fieldData)) {
                    throw new Exception('Helper error : the current element ' . $fieldName . ' has not a "type" key in the array', 500);
                }

                $fieldType = $fieldData['helperType'];
                unset($fieldData['helperType']);

                if ($fieldType == 'input') {
                    $fieldData = $this->input($fieldData);
                } elseif ($fieldType == 'optionList') {
                    $fieldData = $this->optionList($fieldData);
                } elseif ($fieldType == 'textarea') {
                    $fieldData = $this->textArea($fieldData);
                } elseif ($fieldType == 'button') {
                    $fieldData = $this->button($fieldData);
                } else {
                    throw new Exception('Cannot create a ' . $fieldName . ' field element.', 500);
                }
            }
        }

        return new Form($formData, $fields);
    }

    private function input(array $fieldData)
    {
        return new Input($fieldData);
    }

    private function optionList(array $fieldData)
    {
        if (key_exists('data', $fieldData) && is_array($fieldData['data'])) {
            foreach ($fieldData['data'] as &$optionElement) {
                $optionElement = new OptionElement($optionElement);
            }
        }
        return new OptionList($fieldData);
    }

    private function textArea(array $fieldData)
    {
        return new TextArea($fieldData);
    }

    private function button(array $fieldData)
    {
        return new Button($fieldData);
    }
}

if (!function_exists('formValidation')) {
    function formValidation(string $validatorName, Request $request)
    {
        $validator = new Validator();
        return $validator->searchError($validatorName, $request);
    }
}
