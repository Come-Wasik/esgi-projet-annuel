<?php

use App\Middleware\Authentification;
use Framework\Architecture\Web\Routage\RouteManager;
use Framework\Architecture\Web\Routage\Route;

class NewRoute
{
    public static function get($name, $url, $action)
    {
        $manager = new RouteManager();
        $manager->addRoute(new Route([
            'name' => $name,
            'url' => $url,
            'method' => 'GET',
            'action' => $action
        ]));
    }

    public static function post($name, $url, $action)
    {
        $manager = new RouteManager();
        $manager->addRoute(new Route([
            'name' => $name,
            'url' => $url,
            'method' => 'POST',
            'action' => $action
        ]));
    }

    public static function delete($name, $url, $action)
    {
        $manager = new RouteManager();
        $manager->addRoute(new Route([
            'name' => $name,
            'url' => $url,
            'method' => 'DELETE',
            'action' => $action
        ]));
    }

    public static function put($name, $url, $action)
    {
        $manager = new RouteManager();
        $manager->addRoute(new Route([
            'name' => $name,
            'url' => $url,
            'method' => 'PUT',
            'action' => $action
        ]));
    }

    public static function patch($name, $url, $action)
    {
        $manager = new RouteManager();
        $manager->addRoute(new Route([
            'name' => $name,
            'url' => $url,
            'method' => 'PATCH',
            'action' => $action
        ]));
    }

    public static function mixed($name, $url, $action)
    {
        $manager = new RouteManager();
        $manager->addRoute(new Route([
            'name' => $name,
            'url' => $url,
            'method' => 'MIXED',
            'action' => $action
        ]));
    }

    public static function Auth()
    {
        $auth = new Authentification();
        $auth->addRoutes();
    }
}
