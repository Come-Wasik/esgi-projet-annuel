<?php

use Framework\Architecture\Web\CoreMaker\WebsiteMaker;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use Framework\Architecture\Web\Routage\RouteManager;

if (!function_exists('web_request')) {
    function web_request(string $uri, string $httpMethod = 'GET', array $data = null)
    {
        $data['server'] = [
            'REQUEST_URI' => $uri,
            'REQUEST_METHOD' => $httpMethod,
        ];
        $request = new Request($data);

        $consoleMaker = new WebsiteMaker();
        return $consoleMaker->processRequest($request);
    }
}

if (!function_exists('getRouteUrl')) {
    function getRouteUrl(string $name)
    {
        $routeManager = new RouteManager();
        return $routeManager->getUrl($name);
    }
}
