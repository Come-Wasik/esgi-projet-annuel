<?php

/**
 * This function determine the format used for registration in every date + time SQL field
 */
if (!function_exists('getFormattedTimestamp')) {
    function getFormattedTimestamp(): string
    {
        return (new Datetime())->format('Y-m-d H:i:s');
    }
}

/**
 * This function return the actual timestamp, this is an int data
 * @return int timestamp The actual timestamp
 */
if (!function_exists('getTimestamp')) {
    function getTimestamp(): int
    {
        return (new DateTime())->getTimestamp();
    }
}

/**
 * This function format a date given by argument. It can be used in views
 *
 * @param string $datetime A datetime which need to be formatted
 * @return string $formattedDatetime The datetime which is formatted
 */
if (!function_exists('formatDatetime')) {
    function formatDatetime(string $datetime): string
    {
        return (new DateTime($datetime))->format('Y-m-d H:i:s');
    }
}
