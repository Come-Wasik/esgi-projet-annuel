<?php

use Framework\Architecture\Console\CoreMaker\ConsoleMaker;
use Framework\Architecture\Console\CoreMaker\Stream\Command as Request;

define('ARCH', 'console');

### Including makerIni ###
$makeIniPath = dirname(__DIR__).'/maker.ini';
if(!file_exists($makeIniPath)) {
    die('Fatal error : makerIni file should exists');
}
$makerData = parse_ini_file($makeIniPath);

if(!isset($makerData['system_path'])) {
    die('Fatal error : makerIni file should comport a system_path data');
}

### Define system directory ###
$systemPath = dirname(__DIR__).'/'.$makerData['system_path'];

### Include autoloader ###
foreach(glob($systemPath.'/autoloader/*.php') as $autoloader) {
    include $autoloader;
}

### Include bootstrap ###
require $systemPath.'/framework/bootstrap/main.php';

### Get user command ###
$command = new Request();
$command->fillFromGlobals();

### Load command ###
$consoleMaker = new ConsoleMaker();
$consoleMaker->launchCommand($command);
