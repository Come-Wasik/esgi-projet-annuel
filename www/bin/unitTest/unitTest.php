<?php

use Packages\Logger\Logger;
use Framework\Architecture\All\Component\Backpack\Config as ConfigBackpack;
use Framework\Bootstrap\FileLoader\Config;
use Framework\Bootstrap\FileLoader\Helper;
use Framework\Bootstrap\FileLoader\Env;
use Framework\Architecture\Web\Routage\RouteManager as WebRouteManager;
use Framework\Architecture\Console\Router\RouteManager as CommandRouteManager;

### Define constants data for unit tests ###
## Architecture ##
define('ARCH', 'console');
## Environment ##
define('ENV', 'test');

## Define the unit test method to launch ##
$methodToLaunch = 'start';

## A few verification if the class and the method exists
if (!class_exists(UnitTestController::class)) {
    throw new Error('The unit controller doesn\'t exists');
}

if (!method_exists(UnitTestController::class, $methodToLaunch)) {
    throw new Error('The unit controller doesn\'t exists');
}

## Launch ##
$unitController = new UnitTestController();
$unitController->$methodToLaunch();

class UnitTestController
{
    private $systemPath = '';

    public function start()
    {
        try {
            ### Beginning of all test ###
            $this->includeMakerIni();
            $this->includeLogger();
            $this->verifyAutoloader();
            $this->verifyBootstrap();
            $this->verifyConstants();
            $this->verifyConfig();
            $this->verifyhelpers();
            $this->includeErrorManager();
            $this->verifyRoutes();
            echo 'The application works' . "\r\n";
        } catch (Throwable $e) {
            echo "Any error detected\r\n";
        } finally {
            ### Rapport ###
            $logger = new Logger();
            $logger->printLogs();
        }
    }

    private function includeMakerIni()
    {
        ### Defining data for tests ###
        $relativeRootPath = dirname(dirname(__DIR__));
        $makeIniPath = $relativeRootPath . '/maker.ini';
        $importantData = [
            'system_path',
            'config_path',
            'data_storage_path'
        ];
        $importantPath = $importantData;

        ### Tests ! ###
        ## Testing if the make.ini file exists ##
        if (!file_exists($makeIniPath)) {
            die('Fatal error : maker.ini file should exists');
        }

        ### Action ! ###
        ## Get all data from the maker.ini file ##
        $makerData = parse_ini_file($makeIniPath);

        ### Tests ! ###
        ## Testing if all the needed data exists in maker.ini file ##
        foreach ($importantData as $needle) {
            if (!array_key_exists($needle, $makerData)) {
                die('Fatal error : maker.ini file should comport a ' . $needle . ' data');
            }
        }

        ## Testing if all path data actually exists ##
        foreach ($importantPath as $currentPath) {
            $testedPath = $relativeRootPath . '/' . $makerData[$currentPath];
            if (!file_exists($testedPath) || !is_dir($testedPath)) {
                die('System error : In maker.ini, the data ' . $currentPath . ' is incorrect because the path ' . $testedPath . ' doesn\'t exists');
            }
        }
        ### Define system directory ###
        $this->systemPath = $relativeRootPath . '/' . $makerData['system_path'];
    }

    private function includeLogger()
    {
        $systemPath = $this->systemPath;
        $classPath = $systemPath . '/logger/Logger.php';
        require $classPath;
    }

    private function verifyAutoloader()
    {
        ### Defining data for tests ###
        $systemPath = $this->systemPath;
        $autoloaderPath = $systemPath . '/autoloader';

        ### Tests ! ###
        ## Test if autoloader directory exists ##
        if (!file_exists($autoloaderPath) || !is_dir($autoloaderPath)) {
            Logger::setLog('There is no autoloader directory for system classes', Logger::FATAL_ERROR);
        }

        ## Test if autoloader directory is empty
        if (empty(glob($autoloaderPath . '/*.php'))) {
            Logger::setLog('There is no autoloader file', Logger::FATAL_ERROR);
        }

        ### Action ! ###
        ## Include autoloaders ##
        foreach (glob($autoloaderPath . '/*.php') as $autoloader) {
            include $autoloader;
        }
    }

    private function verifyBootstrap()
    {
        ### Defining data for tests ###
        $systemPath = $this->systemPath;
        $bootstrapFilePath = $systemPath . '/framework/bootstrap/main.php';
        $bootstrapFunctionFilePath = $systemPath . '/framework/bootstrap/ressources/functions.php';

        ### Tests ! ###
        ## Test if  ##
        if (!file_exists($bootstrapFilePath)) {
            Logger::setLog('The bootstrap main file is not found.', Logger::FATAL_ERROR);
        }

        if (!file_exists($bootstrapFunctionFilePath)) {
            Logger::setLog('The important bootstrap function file is not found.', Logger::FATAL_ERROR);
        }

        ### Action ! ###
        ## If you ask you of why I don't include the bootstrap/main.php. It's because the program should to test each parts separately
        include $bootstrapFunctionFilePath;
    }

    private function verifyConstants()
    {
        ### Defining data for tests ###
        $envFilePath = base_path() . '/.env';

        $ignoredConstants = [
            'ARCH',
        ];

        ### Tests ! ###
        ## Test if Env Fileloader exists ##
        if (!class_exists(Env::class)) {
            Logger::setLog('The ' . Env::class . ' Fileloader is not found', Logger::FATAL_ERROR);
        }

        ### Action ! ###
        ## Inclusion of Env file loader
        $envFileLoader = new Env();
        $envFileLoader->init();

        ### Tests ! ###
        ## Test if env file exists ##
        if (!file_exists($envFilePath)) {
            Logger::setLog('There is no env file', Logger::WARNING);
        }

        ## Test if user constants are defined ##
        $frameworkConstants = get_defined_constants(true)['user'];

        # We escape ARCH which is defined staticly #
        foreach ($ignoredConstants as $constantToIgnore) {
            if (isset($frameworkConstants[$constantToIgnore])) {
                unset($frameworkConstants[$constantToIgnore]);
            }
        }

        if (empty($frameworkConstants)) {
            Logger::setLog('There is no user constant defined', Logger::NOTICE);
        }
    }

    private function verifyConfig()
    {
        ### Defining data for tests ###
        $configPath = base_path() . '/config';
        $configDataPath = $configPath . '/dataStorage';

        $importantConfigData = [
            'app_path',
            'controller_path',
            'helper_path',
            'route_path',
            'view_path',
            'router',
            'errorManaging'
        ];

        $importantConfigFilePath = [
            'app',
            'cms',
            'db',
            'errorManaging',
            'path',
            'router',
            'security',
            'validator'
        ];

        ### Tests ! ###
        ## Test if /config directory exists ##
        if (!file_exists($configPath) || !is_dir($configPath)) {
            Logger::setLog('There is no config directory', Logger::FATAL_ERROR);
        }

        ## Test if /config/data directory exists ##
        if (!file_exists($configDataPath) || !is_dir($configDataPath)) {
            Logger::setLog('There is no data directory in config directory', Logger::FATAL_ERROR);
        }

        ## Test if there is files in /config/data directory ##
        if (empty(glob($configDataPath . '/*.php'))) {
            Logger::setLog('There is no data file in config directory', Logger::FATAL_ERROR);
        }

        ## Test if important /config/data files are missing ##
        foreach ($importantConfigFilePath as $configFile) {
            if (!file_exists($configDataPath . '/' . $configFile . '.php')) {
                Logger::setLog('The important file ' . $configFile . ' in config directory', Logger::FATAL_ERROR);
                throw new Error();
            }
        }

        if (!class_exists(Config::class)) {
            Logger::setLog('The ' . Config::class . ' Fileloader is not found', Logger::FATAL_ERROR);
        }

        ### Action ! ###
        ## Inclusion of Config file loader
        $configFileLoader = new Config();
        $configFileLoader->init();

        ### Tests ! ###
        ## Test if config backpack exists ##
        if (!class_exists(ConfigBackpack::class)) {
            Logger::setLog('The class ' . ConfigBackpack::class . ' which stock config data doesn\'t exists', Logger::FATAL_ERROR);
        }

        ## Test if config backpack contain any data ##
        if (empty(ConfigBackpack::all())) {
            Logger::setLog('There is no config data stocked in the config backpack', Logger::FATAL_ERROR);
        }

        ## Test if important config data exists ##
        foreach ($importantConfigData as $dataName) {
            if (empty(ConfigBackpack::get($dataName))) {
                Logger::setLog('The important data ' . $dataName . ' doesn\'t exists in the config backpack', Logger::FATAL_ERROR);
                throw new Error();
            }
        }
    }

    private function verifyhelpers()
    {
        ### Defining data for tests ###
        $importantHelpersFunction = [
            'setConfig',
            '\Config::get',
            'app_path',
            'controller_path',
            'route_path',
            'view_path',
            'config_path',
            'validator_path',
            'data_storage_path',
            'viewExists',
            'view',
            'validate',
            'getPackage',
            'getInitializedPackage',
        ];

        $recommandedHelpersFunction = [
            'use_console',
            'use_web',
            'setSession',
            'response',
            'createForm',
            'addFormField'
        ];

        $importantHelpersClasses = [
            'Command' => [
                'add'
            ],
            'NewRoute' => [
                'get',
                'post',
                'delete',
                'put',
                'patch',
                'mixed'
            ],
            'Session' => [
                'get',
                'set'
            ],
            'Config' => [
                'get',
                'set'
            ]
        ];

        $recommandedHelpersClasses = [
        ];

        ### Tests ! ###
        ## Test if Helper Fileloader exists ##
        if (!class_exists(Helper::class)) {
            Logger::setLog('The ' . Helper::class . ' Fileloader is not found', Logger::FATAL_ERROR);
        }

        ### Action ! ###
        ## Inclusion of Env file loader
        $helperFileLoader = new Helper();
        $helperFileLoader->init();

        ### Tests ! ###
        ## Test if helper path exists ##
        if (!file_exists(ConfigBackpack::get('helper_path'))) {
            Logger::setLog('The helper path at path ' . ConfigBackpack::get('helper_path') . ' doesn\'t exists', Logger::FATAL_ERROR);
        }

        ## Test if important helper functions exists ##
        foreach ($importantHelpersFunction as $helper) {
            if (!function_exists($helper)) {
                Logger::setLog('The important helper ' . $helper . ' doesn\'t exists', Logger::FATAL_ERROR);
                throw new Error();
            }
        }

        ## Test if recommanded helper functions exists ##
        foreach ($recommandedHelpersFunction as $helper) {
            if (!function_exists($helper)) {
                Logger::setLog('The recommanded helper ' . $helper . ' doesn\'t exists', Logger::NOTICE);
            }
        }

        ## Test if important helper classes exists ##
        foreach ($importantHelpersClasses as $helperName => $helperContent) {
            if (!class_exists($helperName)) {
                Logger::setLog('The important helper ' . $helperName . ' doesn\'t exists', Logger::FATAL_ERROR);
            }
            if (is_array($helperContent) && !empty($helperContent)) {
                foreach ($helperContent as $method) {
                    if (!method_exists($helperName, $method)) {
                        Logger::setLog('The recommanded method ' . $method . ' in ' . $helperName . ' doesn\'t exists', Logger::FATAL_ERROR);
                        throw new Error();
                    }
                }
            }
        }

        ## Test if recommanded helper classes exists ##
        foreach ($recommandedHelpersClasses as $helperName => $helperContent) {
            if (!class_exists($helperName)) {
                Logger::setLog('The important helper ' . $helperName . ' doesn\'t exists', Logger::NOTICE);
            }
            if (is_array($helperContent) && !empty($helperContent)) {
                foreach ($helperContent as $method) {
                    if (!method_exists($helperName, $method)) {
                        Logger::setLog('The recommanded method ' . $method . ' in ' . $helperName . ' doesn\'t exists', Logger::NOTICE);
                    }
                }
            }
        }
    }

    private function includeErrorManager()
    {
        $systemPath = $this->systemPath;
        $path = $systemPath . '/framework/bootstrap/errormanaging/errorManaging.php';
        require $path;
    }

    private function verifyRoutes()
    {
        ### Defining data for tests ###
        $routePath = route_path();
        $webRouteFilePath = $routePath . '/web.php';
        $consoleRouteFilePath = $routePath . '/console.php';

        ### Tests ! ###
        ## Test if the webRouterManager exist ##
        if (!class_exists(WebRouteManager::class)) {
            Logger::setLog('The class ' . WebRouteManager::class . ' is not found. You\'ll have an error if you use the web Architecture.', Logger::WARNING);
        } else {
            if (!file_exists($webRouteFilePath)) {
                Logger::setLog('The required web route file is not found.', Logger::FATAL_ERROR);
                throw new Error();
            }

            ### Action ! ###
            include $webRouteFilePath;

            ### Tests ! ###
            $routeManager = new WebRouteManager();
            $routes = $routeManager->getRouteList();
            if (empty($routes)) {
                Logger::setLog('No web route found.', Logger::NOTICE);
            }
        }

        ## Test if the RouteManager exist ##
        if (!class_exists(CommandRouteManager::class)) {
            Logger::setLog('The class ' . CommandRouteManager::class . ' is not found. You\'ll have an error if you use the console Architecture.', Logger::WARNING);
        } else {
            if (!file_exists($consoleRouteFilePath)) {
                Logger::setLog('The required console route file is not found.', Logger::FATAL_ERROR);
                throw new Error();
            }

            ### Action ! ###
            include $consoleRouteFilePath;

            ### Tests ! ###
            $routeManager = new CommandRouteManager();
            $commands = $routeManager->getCommandList();
            if (empty($commands)) {
                Logger::setLog('No console command found.', Logger::NOTICE);
            }
        }
    }
}
