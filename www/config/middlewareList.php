<?php

use App\Middleware\Authentification;
use App\Middleware\Firewall;

return [
    'Authentification' => function ($request) {
        $authManager = new Authentification();
        $authManager->startSystem($request);
    },
    'Firewall' => function ($request) {
        $firewall = new Firewall();
        $firewall->analyse($request);
    }
];
