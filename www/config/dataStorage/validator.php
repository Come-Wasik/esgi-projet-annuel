<?php

return [
    'validator' => [
        'messages' => [
            'required' => 'The field {field} is required',
            'characterCount' => [
                'min' => 'The field {field} need a minimum of {min} character',
                'max' => 'The field {field} need a maximum of {max} character',
            ],
            'numberCount' => [
                'min' => 'The field {field} need a minimum number of {min}',
                'max' => 'The field {field} need a maximum number of {max}',
            ],
            'type' => 'The field {field} must to be of type {type}',
            'format' => [
                'email' => 'The field {field} needs to be an email',
                'date' => 'The field {field} needs to be a date'
            ],
            'passwordDoNotMatch' => 'Passwords don\'t match',
            'userNotExist' => 'User not exist',
            'passwordIncorrect' => 'Password is incorrect',
            'accountDisabled' => 'User account disabled',
            'emailNotVerified' => 'Your email is not verified',
            'emailNotRegistered' => 'The email {email} is not registered in our database'
        ]
    ]
];
