<?php

return [
    'app' => [
        'default_timezone' => 'Europe/Paris',

        'middlewareToLaunch' => [
            'Authentification',
            'Firewall'
        ]
    ],
];
