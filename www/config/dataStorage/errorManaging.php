<?php

return [
    'errorManaging' => [
        'errorVisibility' => [
            'prod' => E_ERROR | E_PARSE,
            'dev' => E_ALL,
        ],
    ],
];