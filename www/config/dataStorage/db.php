<?php

return [
    'db_driver' => DB_DRIVER,
    'db_host' => DB_HOST,
    'db_name' => DB_NAME,
    'db_user' => DB_USER,
    'db_password' => DB_PASSWORD,
];