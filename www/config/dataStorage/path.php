<?php

### Importing make.ini data ###
$makeIniPath = base_path() . '/maker.ini';
if (!file_exists($makeIniPath)) {
    die('Fatal error : makerIni file should exists');
}
$makerData = parse_ini_file($makeIniPath);

### Setting data ###
return [
    ### Changed by make.ini ###
    'config_path' => base_path() . '/' . ($makerData['config_path'] ?? 'config'),
    'data_storage_path' => base_path() . '/' . ($makerData['data_storage_path'] ?? 'config/dataStorage'),
    'middleware_list_path' => base_path() . '/' . ($makerData['config_path'] ?? 'config') . '/middlewareList.php',

    ### Changed by user directly here ###
    'app_path' => base_path() . '/app',
    'controller_path' => base_path() . '/app/controller',
    'helper_path' => base_path() . '/app/helpers',
    'route_path' => base_path() . '/routes',
    'view_path' => base_path() . '/ressources/views',
    'entity_path' => base_path() . '/app/entity',
    'validator_path' => base_path() . '/ressources/packages/validators',
    'component_path' => base_path() . '/app/components',
    'test_path' => base_path() . '/ressources/packages/tests',
    'storage_path' => base_path() . '/storage',
];
