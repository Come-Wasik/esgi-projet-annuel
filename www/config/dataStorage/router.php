<?php

return [
    'router' => [
        'filesFor' => [
            'web' => [
                'web',
                'dashboard'
            ],
            'console' => [
                'console'
            ]
        ],
    ]
];
