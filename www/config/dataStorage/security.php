<?php

return [
    'security' => [
        # Enable or disable the module
        'moduleEnabled' => true,

        # Set entities
        'userEntity' => 'App\Entity\Users',
        'roleEntity' => 'App\Entity\User_roles',

        # Set data for each authentification actions
        'connexionData' => [
            'route' => '/login',
            'view' => 'authentification/login',
            'httpMethod' => 'POST',
            'redirectionAfterProcessing' => '/'
        ],
        'registrationData' => [
            'route' => '/register',
            'view' => 'authentification/register',
            'httpMethod' => 'POST',
            'redirectionAfterProcessing' => '/register'
        ],
        'deconnexionData' => [
            'route' => '/deconnexion',
            'redirectionAfterProcessing' => '/',
        ],
        'reset-password' => [
            'send-email' => [
                'route' => '/reset-password/send-email',
                'view' => 'authentification/send-email'
            ],
            'send-password' => [
                'route' => '/reset-password/confirm',
                'view' => 'authentification/reset-password'
            ]
        ]
    ],
];
